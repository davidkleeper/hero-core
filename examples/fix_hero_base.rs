// cargo run --example fix_hero_base
// Utility code for cleaning up builds
use std::fs;

fn main() {
    let mut dirs: Vec<String> = Vec::<String>::new();
    dirs.push(String::from("/Users/davidleeper/Documents/src/rust/hero-core/HeroBase/Arachnos Soldier"));
    dirs.push(String::from("/Users/davidleeper/Documents/src/rust/hero_core/HeroBase/Arachnos Widow"));
    dirs.push(String::from("/Users/davidleeper/Documents/src/rust/hero_core/HeroBase/Blaster"));
    dirs.push(String::from("/Users/davidleeper/Documents/src/rust/hero_core/HeroBase/Brute"));
    dirs.push(String::from("/Users/davidleeper/Documents/src/rust/hero_core/HeroBase/Controller"));
    dirs.push(String::from("/Users/davidleeper/Documents/src/rust/hero_core/HeroBase/Corruptor"));
    dirs.push(String::from("/Users/davidleeper/Documents/src/rust/hero_core/HeroBase/Defender"));
    dirs.push(String::from("/Users/davidleeper/Documents/src/rust/hero_core/HeroBase/Dominator"));
    dirs.push(String::from("/Users/davidleeper/Documents/src/rust/hero_core/HeroBase/Mastermind"));
    dirs.push(String::from("/Users/davidleeper/Documents/src/rust/hero_core/HeroBase/Peacebringer"));
    dirs.push(String::from("/Users/davidleeper/Documents/src/rust/hero_core/HeroBase/Scrapper"));
    dirs.push(String::from("/Users/davidleeper/Documents/src/rust/hero_core/HeroBase/Sentinel"));
    dirs.push(String::from("/Users/davidleeper/Documents/src/rust/hero_core/HeroBase/Stalker"));
    dirs.push(String::from("/Users/davidleeper/Documents/src/rust/hero_core/HeroBase/Tanker"));
    dirs.push(String::from("/Users/davidleeper/Documents/src/rust/hero_core/HeroBase/Warshade"));

    for dir in dirs.iter() {
        let paths = fs::read_dir(dir).unwrap();
        for path in paths {
            let file_name = path.unwrap().path();
            let mut file_contents = std::fs::read_to_string(&file_name).unwrap();

            // Actual work goes here.
            file_contents = str::replace(&file_contents, "SingleO", "Single");
            file_contents = str::replace(&file_contents, "DualO", "Dual");
            file_contents = str::replace(&file_contents, "TrainingO", "Training");
            println!("{:?}", file_contents);
            fs::write(&file_name, file_contents).expect("Unable to write file");
        }
    }
 }