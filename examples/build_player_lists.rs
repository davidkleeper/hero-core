// cargo run --example build_player_lists
// Utility code for find duplicate power effects
extern crate scatterbase;

use std::fs;

use hero_core::functions::registry::get_ancillary_power_sets_from_archetype;
use hero_core::functions::registry::get_archetypes;
use hero_core::functions::registry::get_pool_power_sets_from_archetype;
use hero_core::functions::registry::get_old_power;
use hero_core::functions::registry::get_old_power_set;
use hero_core::functions::registry::get_old_powers_from_power_set;
use hero_core::functions::registry::get_primary_power_sets_from_archetype;
use hero_core::functions::registry::get_secondary_power_sets_from_archetype;
use hero_core::poro::new_effect::NewEffect;
use hero_core::poro::new_power::NewPower;
use hero_core::poro::power::Power;
use hero_core::poro::power_set::PowerSet;

fn main() {
    let mut archetypes = match get_archetypes(true) {
        Ok(at) => at,
        Err(e) => {
            println!("Error loading archetypes. Error: {:?}", e);
            return;
        }
    };

    let mut power_sets = Vec::<PowerSet>::new();
    let mut powers = Vec::<Power>::new();

    for archetype in archetypes.clone() {
        let mut epic_power_sets = get_ancillary_power_sets_from_archetype(&archetype.id).unwrap().unwrap();
        let mut primary_power_sets = get_primary_power_sets_from_archetype(&archetype.id).unwrap().unwrap();
        let mut secondary_power_sets = get_secondary_power_sets_from_archetype(&archetype.id).unwrap().unwrap();
        let mut pool_power_sets = get_pool_power_sets_from_archetype(&archetype.id).unwrap().unwrap();
        let fitness = get_old_power_set("Inherent.Fitness").unwrap().unwrap();
        let inherent = get_old_power_set("Inherent.Inherent").unwrap().unwrap();

        power_sets.append(&mut epic_power_sets);
        power_sets.append(&mut primary_power_sets);
        power_sets.append(&mut secondary_power_sets);
        power_sets.append(&mut pool_power_sets);
        power_sets.push(fitness);
        power_sets.push(inherent);

        for power_set in power_sets.clone() {
            let powers_from_power_set = get_old_powers_from_power_set(&power_set.id).unwrap().unwrap();
            for power in powers_from_power_set {
                let old_power = get_old_power(&power.id).unwrap().unwrap();
                powers.push(old_power);
            }
        }
    }

    archetypes.sort_by(|a, b| a.id.cmp(&b.id));
    archetypes.dedup();
    power_sets.sort_by(|a, b| a.id.cmp(&b.id));
    power_sets.dedup();
    powers.sort_by(|a, b| a.id.cmp(&b.id));
    powers.dedup();

    let mut new_powers = Vec::<NewPower>::new();
    let mut new_effects = Vec::<NewEffect>::new();
    for power in powers {
        let (new_power, mut new_power_effects) = NewPower::convert(&power);
        new_powers.push(new_power);
        new_effects.append(&mut new_power_effects);
    }

    let archetypes_json = serde_json::to_string(&archetypes.clone()).unwrap();
    fs::write("./database/unix/player_archetypes.json", archetypes_json).expect("Unable to write file");
    let power_sets_json = serde_json::to_string(&power_sets).unwrap();
    fs::write("./database/unix/player_power_sets.json", power_sets_json).expect("Unable to write file");
    let new_powers_json = serde_json::to_string(&new_powers).unwrap();
    fs::write("./database/unix/player_powers.json", new_powers_json).expect("Unable to write file");
    let new_effects_json = serde_json::to_string(&new_effects).unwrap();
    fs::write("./database/unix/player_effects.json", new_effects_json).expect("Unable to write file");
}
