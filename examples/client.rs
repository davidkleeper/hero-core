// cargo run --example client
use std::net::{TcpStream};
use std::io::{Read, Write};

fn main() {
    match TcpStream::connect("localhost:3333") {
        Ok(mut stream) => {
            println!("Successfully connected to server on port 3333");

            let msg = b"ARCHETYPE Class_Blaster";

            stream.write(msg).unwrap();
            println!("Sent request, awaiting reply...");

            let mut response = String::new();
            stream.read_to_string(&mut response).unwrap();
            println!("{}", response);
        },
        Err(e) => {
            println!("Failed to connect: {}", e);
        }
    }
    println!("Terminated.");
}