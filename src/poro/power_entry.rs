use crate::poro::power_sub_entry::PowerSubEntry;
use crate::poro::slot::Slot;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct PowerEntry {
    pub level: u16,
    pub power_id: String,
    pub tag: bool,
    pub stat_include: bool,
    pub variable_value: i32,
    pub slots: Vec<Slot>,
    pub sub_powers: Vec<PowerSubEntry>,
}

#[allow(dead_code)]
impl PowerEntry {
    pub fn new() -> PowerEntry {
        let power_entry = PowerEntry {
            level: 0,
            power_id: String::from(""),
            tag: false,
            stat_include: false,
            variable_value: 0,
            slots: Vec::<Slot>::new(),
            sub_powers: Vec::<PowerSubEntry>::new(),
        };
        power_entry
    }
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::prelude::*;

    use crate::scatterbase::functions::cross_platform::path;

    #[test]
    #[allow(unused_must_use)]
    fn test1() {
        let mut file = File::open(&path("./data/power-entry.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: super::PowerEntry = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.level, 0);
        assert_eq!(
            result.power_id,
            "Defender_Buff.Force_Field.Personal_Force_Field"
        );
        assert_eq!(result.tag, false);
        assert_eq!(result.stat_include, false);
        assert_eq!(result.variable_value, 0);
        assert_eq!(result.slots.len(), 3);
        assert_eq!(result.sub_powers.len(), 0);
    }

    #[test]
    #[allow(unused_must_use)]
    fn test2() {
        let mut file = File::open(&path("./data/power-entries.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: Vec<super::PowerEntry> = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.len(), 3);
    }
}
