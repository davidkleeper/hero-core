use crate::enums::buff_mode::BuffMode;
use crate::enums::effect_area::EffectArea;
use crate::enums::enhance::Enhance;
use crate::enums::entity::Entity;
use crate::enums::mode_flags::ModeFlags;
use crate::enums::notify::Notify;
use crate::enums::power_type::PowerType;
use crate::enums::set_type::SetType;
use crate::enums::vector::Vector;
use crate::poro::new_effect::NewEffect;
use crate::poro::power::Power;
use crate::poro::requirement::Requirement;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct NewPower {
    pub absorb_summon_attributes: bool,
    pub absorb_summon_effects: bool,
    pub accuracy: f32,
    pub accuracy_mult: f32,
    pub activate_period: f32,
    pub ai_report: Notify,
    pub allow_front_loading: bool,
    pub always_toggle: bool,
    pub arc: i32,
    pub attack_types: u32,
    pub available: i32,
    pub base_recharge_time: f32,
    pub boost_boostable: bool,
    pub boost_use_player_level: bool,
    pub boosts_allowed: Vec<String>,
    pub buff_mode: BuffMode,
    pub cast_flags: u32,
    pub cast_through_hold: bool,
    pub cast_time: f32,
    pub cast_time_real: f32,
    pub click_buff: bool,
    pub description_long: String,
    pub description_short: String,
    pub display_location: i32,
    pub display_name: String,
    pub do_not_save: bool,
    pub effect_area: EffectArea,
    pub effect_ids: Vec<String>,
    pub end_cost: f32,
    pub enhancement_ids: Option<Vec<String>>,
    pub entities_affected: u32,
    pub entities_auto_hit: u32,
    pub forced_class: String,
    pub full_name: String,
    pub group_membership: Vec<String>,
    pub group_name: String,
    pub has_absorbed_effects: bool,
    pub has_grant_power_effect: bool,
    pub has_power_override_effect: bool,
    pub hidden_power: bool,
    pub id: String,
    pub ignore_buff: Vec<Enhance>,
    pub ignore_enhance: Vec<Enhance>,
    pub ignore_strength: bool,
    pub include_flag: bool,
    pub interrupt_time: f32,
    pub is_epic: bool,
    pub level: u16,
    pub life_time: i32,
    pub life_time_in_game: i32,
    pub location_index: i32,
    pub max_boosts: String,
    pub max_targets: i32,
    pub modes_disallowed: u32,
    pub modes_required: u32,
    pub mutex_auto: bool,
    pub mutex_ignore: bool,
    pub never_auto_update: bool,
    pub never_auto_update_requirements: bool,
    pub num_allowed: i32,
    pub num_charges: i32,
    pub power_name: String,
    pub power_set_id: String,
    pub power_type: PowerType,
    pub radius: f32,
    pub range: f32,
    pub range_secondary: f32,
    pub recharge_time: f32,
    pub requires: Requirement,
    pub set_name: String,
    pub set_types: Vec<SetType>,
    pub show_summon_anyway: bool,
    pub skip_max: bool,
    pub slottable: bool,
    pub sort_override: bool,
    pub sub_is_alt_colour: bool,
    pub sub_power_ids: Vec<String>,
    pub target: u32,
    pub target_los: bool,
    pub target_secondary: u32,
    pub toggle_cost: f32,
    pub usage_time: i32,
    pub variable_enabled: bool,
    pub variable_max: i32,
    pub variable_min: i32,
    pub variable_name: String,
}

#[allow(dead_code)]
impl NewPower {
    pub fn new() -> NewPower {
        let power = NewPower {
            absorb_summon_attributes: false,
            absorb_summon_effects: false,
            accuracy: 0.0f32,
            accuracy_mult: 0.0f32,
            activate_period: 0.0f32,
            ai_report: Notify::Always,
            allow_front_loading: false,
            always_toggle: false,
            arc: 0,
            attack_types: Vector::None,
            available: 0,
            base_recharge_time: 0.0f32,
            boost_boostable: false,
            boost_use_player_level: false,
            boosts_allowed: Vec::<String>::new(),
            buff_mode: BuffMode::Normal,
            cast_flags: 0,
            cast_through_hold: false,
            cast_time: 0.0f32,
            cast_time_real: 0.0f32,
            click_buff: false,
            description_long: String::from(""),
            description_short: String::from(""),
            display_location: 0,
            display_name: String::from(""),
            do_not_save: false,
            effect_area: EffectArea::None,
            effect_ids: Vec::<String>::new(),
            end_cost: 0.0f32,
            enhancement_ids: Some(Vec::<String>::new()),
            entities_affected: Entity::None,
            entities_auto_hit: Entity::None,
            forced_class: String::from(""),
            full_name: String::from(""),
            group_membership: Vec::<String>::new(),
            group_name: String::from(""),
            has_absorbed_effects: false,
            has_grant_power_effect: false,
            has_power_override_effect: false,
            hidden_power: false,
            id: String::from(""),
            ignore_buff: Vec::<Enhance>::new(),
            ignore_enhance: Vec::<Enhance>::new(),
            ignore_strength: false,
            include_flag: false,
            interrupt_time: 0.0f32,
            is_epic: false,
            level: 0,
            life_time: 0,
            life_time_in_game: 0,
            location_index: 0,
            max_boosts: String::from(""),
            max_targets: 0,
            modes_disallowed: ModeFlags::None,
            modes_required: ModeFlags::None,
            mutex_auto: false,
            mutex_ignore: false,
            never_auto_update: false,
            never_auto_update_requirements: false,
            num_allowed: 0,
            num_charges: 0,
            power_name: String::from(""),
            power_set_id: String::from(""),
            power_type: PowerType::Click,
            radius: 0.0f32,
            range: 0.0f32,
            range_secondary: 0.0f32,
            recharge_time: 0.0f32,
            requires: Requirement::new(),
            set_name: String::from(""),
            set_types: Vec::<SetType>::new(),
            show_summon_anyway: false,
            skip_max: false,
            slottable: false,
            sort_override: false,
            sub_is_alt_colour: false,
            sub_power_ids: Vec::<String>::new(),
            target: Entity::None,
            target_los: false,
            target_secondary: Entity::None,
            toggle_cost: 0.0f32,
            usage_time: 0,
            variable_enabled: false,
            variable_max: 0,
            variable_min: 0,
            variable_name: String::from(""),
        };
        power
    }

    pub fn convert(power: &Power) -> (NewPower, Vec<NewEffect>) {
        let (new_effects, new_effect_ids) = NewEffect::batch_convert(&power.effects);
        let new_power = NewPower {
            absorb_summon_attributes: power.absorb_summon_attributes.clone(),
            absorb_summon_effects: power.absorb_summon_effects.clone(),
            accuracy: power.accuracy.clone(),
            accuracy_mult: power.accuracy_mult.clone(),
            activate_period: power.activate_period.clone(),
            ai_report: power.ai_report.clone(),
            allow_front_loading: power.allow_front_loading.clone(),
            always_toggle: power.always_toggle.clone(),
            arc: power.arc.clone(),
            attack_types: power.attack_types.clone(),
            available: power.available.clone(),
            base_recharge_time: power.base_recharge_time.clone(),
            boost_boostable: power.boost_boostable.clone(),
            boost_use_player_level: power.boost_use_player_level.clone(),
            boosts_allowed: power.boosts_allowed.clone(),
            buff_mode: power.buff_mode.clone(),
            cast_flags: power.cast_flags.clone(),
            cast_through_hold: power.cast_through_hold.clone(),
            cast_time: power.cast_time.clone(),
            cast_time_real: power.cast_time_real.clone(),
            click_buff: power.click_buff.clone(),
            description_long: power.description_long.clone(),
            description_short: power.description_short.clone(),
            display_location: power.display_location.clone(),
            display_name: power.display_name.clone(),
            do_not_save: power.do_not_save.clone(),
            effect_area: power.effect_area.clone(),
            effect_ids: new_effect_ids,
            end_cost: power.end_cost.clone(),
            enhancement_ids: power.enhancement_ids.clone(),
            entities_affected: power.entities_affected.clone(),
            entities_auto_hit: power.entities_auto_hit.clone(),
            forced_class: power.forced_class.clone(),
            full_name: power.full_name.clone(),
            group_membership: power.group_membership.clone(),
            group_name: power.group_name.clone(),
            has_absorbed_effects: power.has_absorbed_effects.clone(),
            has_grant_power_effect: power.has_grant_power_effect.clone(),
            has_power_override_effect: power.has_power_override_effect.clone(),
            hidden_power: power.hidden_power.clone(),
            id: power.id.clone(),
            ignore_buff: power.ignore_buff.clone(),
            ignore_enhance: power.ignore_enhance.clone(),
            ignore_strength: power.ignore_strength.clone(),
            include_flag: power.include_flag.clone(),
            interrupt_time: power.interrupt_time.clone(),
            is_epic: power.is_epic.clone(),
            level: power.level.clone(),
            life_time: power.life_time.clone(),
            life_time_in_game: power.life_time_in_game.clone(),
            location_index: power.location_index.clone(),
            max_boosts: power.max_boosts.clone(),
            max_targets: power.max_targets.clone(),
            modes_disallowed: power.modes_disallowed.clone(),
            modes_required: power.modes_required.clone(),
            mutex_auto: power.mutex_auto.clone(),
            mutex_ignore: power.mutex_ignore.clone(),
            never_auto_update: power.never_auto_update.clone(),
            never_auto_update_requirements: power.never_auto_update_requirements.clone(),
            num_allowed: power.num_allowed.clone(),
            num_charges: power.num_charges.clone(),
            power_name: power.power_name.clone(),
            power_set_id: power.power_set_id.clone(),
            power_type: power.power_type.clone(),
            radius: power.radius.clone(),
            range: power.range.clone(),
            range_secondary: power.range_secondary.clone(),
            recharge_time: power.recharge_time.clone(),
            requires: power.requires.clone(),
            set_name: power.set_name.clone(),
            set_types: power.set_types.clone(),
            show_summon_anyway: power.show_summon_anyway.clone(),
            skip_max: power.skip_max.clone(),
            slottable: power.slottable.clone(),
            sort_override: power.sort_override.clone(),
            sub_is_alt_colour: power.sub_is_alt_colour.clone(),
            sub_power_ids: power.sub_power_ids.clone(),
            target: power.target.clone(),
            target_los: power.target_los.clone(),
            target_secondary: power.target_secondary.clone(),
            toggle_cost: power.toggle_cost.clone(),
            usage_time: power.usage_time.clone(),
            variable_enabled: power.variable_enabled.clone(),
            variable_max: power.variable_max.clone(),
            variable_min: power.variable_min.clone(),
            variable_name: power.variable_name.clone(),
        };

        (new_power, new_effects)
    }
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::prelude::*;

    use crate::enums::buff_mode::BuffMode;
    use crate::enums::effect_area::EffectArea;
    use crate::enums::notify::Notify;
    use crate::enums::power_type::PowerType;
    use crate::enums::set_type::SetType;
    use crate::scatterbase::functions::cross_platform::path;

    #[test]
    #[allow(unused_must_use)]
    fn test1() {
        let mut file = File::open(&path("./data/new_power.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: super::NewPower = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.absorb_summon_attributes, false);
        assert_eq!(result.absorb_summon_effects, false);
        assert_eq!(result.accuracy, 1.0f32);
        assert_eq!(result.accuracy_mult, 1.0f32);
        assert_eq!(result.activate_period, 0.5f32);
        assert_eq!(result.ai_report, Notify::Always);
        assert_eq!(result.allow_front_loading, false);
        assert_eq!(result.always_toggle, true);
        assert_eq!(result.arc, 0);
        assert_eq!(result.attack_types, 0);
        assert_eq!(result.available, 0);
        assert_eq!(result.base_recharge_time, 4.0f32);
        assert_eq!(result.boost_boostable, false);
        assert_eq!(result.boost_use_player_level, false);
        assert_eq!(result.boosts_allowed[0], "Buff_Defense_Boost");
        assert_eq!(result.boosts_allowed[1], "EnduranceDiscount_Boost");
        assert_eq!(result.boosts_allowed[2], "Recharge_Boost");
        assert_eq!(result.buff_mode, BuffMode::Normal);
        assert_eq!(result.cast_flags, 0);
        assert_eq!(result.cast_through_hold, false);
        assert_eq!(result.cast_time, 2.02999997f32);
        assert_eq!(result.cast_time_real, 2.02999997f32);
        assert_eq!(result.click_buff, false);
        assert_eq!(result.display_location, 0);
        assert_eq!(result.display_name, "Crystal Armor");
        assert_eq!(result.do_not_save, false);
        assert_eq!(result.effect_area, EffectArea::Character);
        assert_eq!(result.end_cost, 0.129999995f32);
        let enhancement_ids = result.enhancement_ids.unwrap();
        assert_eq!(enhancement_ids[0], "Magic_Defense_DeBuff");
        assert_eq!(enhancement_ids[1], "Magic_Fear");
        assert_eq!(enhancement_ids[2], "Magic_Res_Damage");
        assert_eq!(result.entities_affected, 1);
        assert_eq!(result.entities_auto_hit, 1);
        assert_eq!(result.forced_class, "");
        assert_eq!(result.full_name, "Tanker_Defense.Stone_Armor.Crystal_Armor");
        assert_eq!(result.power_set_id, "Tanker_Defense.Stone_Armor");
        assert_eq!(result.group_membership[0], "Crystal_Armor_Group");
        assert_eq!(result.group_name, "Tanker_Defense");
        assert_eq!(result.has_absorbed_effects, false);
        assert_eq!(result.has_grant_power_effect, false);
        assert_eq!(result.has_power_override_effect, false);
        assert_eq!(result.hidden_power, false);
        assert_eq!(result.ignore_buff.len(), 0);
        assert_eq!(result.ignore_enhance.len(), 0);
        assert_eq!(result.ignore_strength, true);
        assert_eq!(result.include_flag, false);
        assert_eq!(result.interrupt_time, 0.0f32);
        assert_eq!(result.is_epic, false);
        assert_eq!(result.level, 18);
        assert_eq!(result.life_time, 0);
        assert_eq!(result.life_time_in_game, 0);
        assert_eq!(result.location_index, 0);
        assert_eq!(result.max_boosts, "");
        assert_eq!(result.max_targets, 0);
        assert_eq!(result.modes_disallowed, 2);
        assert_eq!(result.modes_required, 0);
        assert_eq!(result.mutex_auto, true);
        assert_eq!(result.mutex_ignore, false);
        assert_eq!(result.never_auto_update, false);
        assert_eq!(result.never_auto_update_requirements, false);
        assert_eq!(result.num_allowed, 1);
        assert_eq!(result.num_charges, 0);
        assert_eq!(result.power_name, "Crystal_Armor");
        assert_eq!(result.power_set_id, "Tanker_Defense.Stone_Armor");
        assert_eq!(result.power_type, PowerType::Toggle);
        assert_eq!(result.radius, 0.0f32);
        assert_eq!(result.range, 0.0f32);
        assert_eq!(result.range_secondary, 0.0f32);
        assert_eq!(result.recharge_time, 4.0f32);
        assert_eq!(result.set_name, "Stone_Armor");
        assert_eq!(result.set_types[0], SetType::Defense);
        assert_eq!(result.show_summon_anyway, false);
        assert_eq!(result.skip_max, false);
        assert_eq!(result.slottable, true);
        assert_eq!(result.sort_override, false);
        assert_eq!(result.sub_is_alt_colour, false);
        assert_eq!(result.target, 1);
        assert_eq!(result.target_los, true);
        assert_eq!(result.target_secondary, 0);
        assert_eq!(result.toggle_cost, 0.25999999f32);
        assert_eq!(result.usage_time, 0);
        assert_eq!(result.variable_enabled, false);
        assert_eq!(result.variable_max, 0);
        assert_eq!(result.variable_min, 0);
        assert_eq!(result.variable_name, "");
    }
}
