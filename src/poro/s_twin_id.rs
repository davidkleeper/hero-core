use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct STwinID {
    pub id: i32,
    pub sub_id: i32,
}

#[allow(dead_code)]
impl STwinID {
    pub fn new() -> STwinID {
        let s_twin_id = STwinID { id: 0, sub_id: 0 };
        s_twin_id
    }
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::prelude::*;

    use crate::scatterbase::functions::cross_platform::path;

    #[test]
    #[allow(unused_must_use)]
    fn test1() {
        let mut file = File::open(&path("./data/s-twin-id.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: super::STwinID = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.id, 14);
        assert_eq!(result.sub_id, -1);
    }
}
