use crate::enums::power_set_type::PowerSetType;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct PowerSet {
    pub archetype_id: String,
    pub description: String,
    pub display_name: String,
    pub full_name: String,
    pub group_name: String,
    pub id: String,
    pub image_name: String,
    pub power_ids: Vec<String>,
    pub set_name: String,
    pub set_type: PowerSetType,
    pub sub_name: String,
}

#[allow(dead_code)]
impl PowerSet {
    pub fn new() -> PowerSet {
        let power_set = PowerSet {
            archetype_id: String::from(""),
            description: String::from(""),
            display_name: String::from(""),
            full_name: String::from(""),
            group_name: String::from(""),
            id: String::from(""),
            image_name: String::from(""),
            power_ids: Vec::<String>::new(),
            set_name: String::from(""),
            set_type: PowerSetType::None,
            sub_name: String::from(""),
        };
        power_set
    }
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::prelude::*;

    use crate::enums::power_set_type::PowerSetType;
    use crate::scatterbase::functions::cross_platform::path;

    #[test]
    #[allow(unused_must_use)]
    fn test1() {
        let mut file = File::open(&path("./data/power-set.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: super::PowerSet = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.archetype_id, "Class_Arachnos_Soldier");
        assert_eq!(result.display_name, "Arachnos Soldier");
        assert_eq!(result.full_name, "Arachnos_Soldiers.Arachnos_Soldier");
        assert_eq!(result.group_name, "Arachnos_Soldiers");
        assert_eq!(result.id, "Arachnos_Soldiers.Arachnos_Soldier");
        assert_eq!(result.image_name, "i12_Pri_Soldier.png");
        assert_eq!(
            result.power_ids[0],
            "Arachnos_Soldiers.Arachnos_Soldier.Single_Shot"
        );
        assert_eq!(
            result.power_ids[1],
            "Arachnos_Soldiers.Arachnos_Soldier.Pummel"
        );
        assert_eq!(
            result.power_ids[2],
            "Arachnos_Soldiers.Arachnos_Soldier.Burst"
        );
        assert_eq!(
            result.power_ids[3],
            "Arachnos_Soldiers.Arachnos_Soldier.WS_Wide_Area_Web_Grenade"
        );
        assert_eq!(
            result.power_ids[4],
            "Arachnos_Soldiers.Arachnos_Soldier.Heavy_Burst"
        );
        assert_eq!(
            result.power_ids[5],
            "Arachnos_Soldiers.Arachnos_Soldier.Bayonet"
        );
        assert_eq!(
            result.power_ids[6],
            "Arachnos_Soldiers.Arachnos_Soldier.Venom_Grenade"
        );
        assert_eq!(
            result.power_ids[7],
            "Arachnos_Soldiers.Arachnos_Soldier.Frag_Grenade"
        );
        assert_eq!(result.set_name, "Arachnos_Soldier");
        assert_eq!(result.set_type, PowerSetType::Primary);
        assert_eq!(result.sub_name, "Arachnos Soldiers");
    }

    #[test]
    #[allow(unused_must_use)]
    fn test2() {
        let mut file = File::open(&path("./data/power-sets.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: Vec<super::PowerSet> = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.len(), 3552);
    }
}
