use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct Requirement {
    pub class_name: Vec<String>,
    pub class_name_not: Vec<String>,
    pub power_id: Vec<Vec<String>>,
    pub power_id_not: Vec<Vec<String>>,
}

#[allow(dead_code)]
impl Requirement {
    pub fn new() -> Requirement {
        let requirement = Requirement {
            class_name: Vec::<String>::new(),
            class_name_not: Vec::<String>::new(),
            power_id: Vec::<Vec<String>>::new(),
            power_id_not: Vec::<Vec<String>>::new(),
        };
        requirement
    }
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::prelude::*;

    use crate::scatterbase::functions::cross_platform::path;

    #[test]
    #[allow(unused_must_use)]
    fn test1() {
        let mut file = File::open(&path("./data/requirement.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: super::Requirement = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.class_name.len(), 0);
        assert_eq!(result.class_name_not.len(), 0);
        assert_eq!(result.power_id.len(), 0);
        assert_eq!(result.power_id_not.len(), 0);
    }
}
