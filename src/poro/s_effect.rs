use crate::enums::buff_debuff::BuffDebuff;
use crate::enums::effect_mode::EffectMode;
use crate::enums::schedule::Schedule;
use crate::poro::effect::Effect;
use crate::poro::s_twin_id::STwinID;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct SEffect {
    pub mode: EffectMode,
    pub buff_debuff: BuffDebuff,
    pub enhance: STwinID,
    pub schedule: Schedule,
    pub multiplier: f32,
    pub fx: Option<Effect>,
}

#[allow(dead_code)]
impl SEffect {
    pub fn new() -> SEffect {
        let s_effect = SEffect {
            mode: EffectMode::Enhancement,
            buff_debuff: BuffDebuff::Any,
            enhance: STwinID::new(),
            schedule: Schedule::None,
            multiplier: 0.0f32,
            fx: Some(Effect::new()),
        };
        s_effect
    }
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::prelude::*;

    use crate::enums::buff_debuff::BuffDebuff;
    use crate::enums::effect_mode::EffectMode;
    use crate::enums::schedule::Schedule;
    use crate::scatterbase::functions::cross_platform::path;

    #[test]
    #[allow(unused_must_use)]
    fn test1() {
        let mut file = File::open(&path("./data/s-effect.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: super::SEffect = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.mode, EffectMode::Enhancement);
        assert_eq!(result.buff_debuff, BuffDebuff::Any);
        assert_eq!(result.enhance.id, 14);
        assert_eq!(result.enhance.sub_id, -1);
        assert_eq!(result.schedule, Schedule::A);
        assert_eq!(result.multiplier, 1.0f32);
        assert_eq!(result.fx, None);
    }

    #[test]
    #[allow(unused_must_use)]
    fn test2() {
        let mut file = File::open(&path("./data/s-effects.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: Vec<super::SEffect> = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.len(), 2);
    }
}
