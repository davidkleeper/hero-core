use crate::enums::class_type::ClassType;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct Archetype {
    pub base_recovery: f32,
    pub base_regen: f32,
    pub base_threat: f32,
    pub cap_damage: f32,
    pub cap_hit_points: f32,
    pub cap_perception: f32,
    pub cap_recharge: f32,
    pub cap_recovery: f32,
    pub cap_regeneration: f32,
    pub cap_resistance: f32,
    pub class_name: String,
    pub class_type: ClassType,
    pub description_long: String,
    pub description_short: String,
    pub display_name: String,
    pub group_epic: String,
    pub group_primary: String,
    pub group_pool: String,
    pub group_secondary: String,
    pub hit_points: f32,
    pub id: String,
    pub origin: Vec<String>,
    pub playable: bool,
    pub powersets_ancillary: Vec<String>,
    pub powersets_primary: Vec<String>,
    pub powersets_secondary: Vec<String>,
}

#[allow(dead_code)]
impl Archetype {
    pub fn new() -> Archetype {
        let archetype = Archetype {
            base_recovery: 0.0f32,
            base_regen: 0.0f32,
            base_threat: 0.0f32,
            cap_damage: 0.0f32,
            cap_hit_points: 0.0f32,
            cap_perception: 0.0f32,
            cap_recharge: 0.0f32,
            cap_recovery: 0.0f32,
            cap_regeneration: 0.0f32,
            cap_resistance: 0.0f32,
            class_name: String::from(""),
            class_type: ClassType::None,
            description_long: String::from(""),
            description_short: String::from(""),
            display_name: String::from(""),
            group_epic: String::from(""),
            group_primary: String::from(""),
            group_pool: String::from(""),
            group_secondary: String::from(""),
            hit_points: 0.0f32,
            id: String::from(""),
            origin: Vec::<String>::new(),
            playable: false,
            powersets_ancillary: Vec::<String>::new(),
            powersets_primary: Vec::<String>::new(),
            powersets_secondary: Vec::<String>::new(),
        };
        archetype
    }
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::prelude::*;

    use crate::enums::class_type::ClassType;
    use crate::scatterbase::functions::cross_platform::path;

    #[test]
    #[allow(unused_must_use)]
    fn test1() {
        let mut file = File::open(&path("./data/archetype.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: super::Archetype = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.base_recovery, 1.0f32);
        assert_eq!(result.base_regen, 0.25f32);
        assert_eq!(result.base_threat, 1.0f32);
        assert_eq!(result.cap_damage, 5.0f32);
        assert_eq!(result.cap_hit_points, 1847.30005f32);
        assert_eq!(result.cap_perception, 1153.0f32);
        assert_eq!(result.cap_recharge, 5.0f32);
        assert_eq!(result.cap_recovery, 5.0f32);
        assert_eq!(result.cap_regeneration, 20.0f32);
        assert_eq!(result.cap_resistance, 0.75f32);
        assert_eq!(result.class_name, "Class_Blaster");
        assert_eq!(result.class_type, ClassType::Hero);
        assert_eq!(
            result.description_short,
            "Superior ranged attacks and tactical support."
        );
        assert_eq!(result.display_name, "Blaster");
        assert_eq!(result.group_epic, "EPIC");
        assert_eq!(result.group_primary, "Blaster_Ranged");
        assert_eq!(result.group_pool, "POOL");
        assert_eq!(result.group_secondary, "Blaster_Support");
        assert_eq!(result.hit_points, 1205.0f32);
        assert_eq!(result.id, "Class_Blaster");
        assert_eq!(result.origin[0], "Magic");
        assert_eq!(result.origin[1], "Mutation");
        assert_eq!(result.origin[2], "Natural");
        assert_eq!(result.origin[3], "Science");
        assert_eq!(result.origin[4], "Technology");
        assert_eq!(result.playable, true);
        assert_eq!(result.powersets_ancillary[0], "Epic.Cold_Mastery");
        assert_eq!(result.powersets_ancillary[1], "Epic.Electrical_Mastery");
        assert_eq!(result.powersets_ancillary[2], "Epic.Flame_Mastery");
        assert_eq!(result.powersets_ancillary[3], "Epic.Force_Mastery");
        assert_eq!(
            result.powersets_ancillary[4],
            "Epic.Mastermind_Leviathan_Mastery"
        );
        assert_eq!(result.powersets_ancillary[5], "Epic.Blaster_Mace_Mastery");
        assert_eq!(result.powersets_ancillary[6], "Epic.Blaster_Mu_Mastery");
        assert_eq!(result.powersets_ancillary[7], "Epic.Munitions_Mastery");
        assert_eq!(
            result.powersets_ancillary[8],
            "Epic.Mastermind_Soul_Mastery"
        );
        assert_eq!(result.powersets_primary[0], "Blaster_Ranged.Archery");
        assert_eq!(result.powersets_primary[1], "Blaster_Ranged.Assault_Rifle");
        assert_eq!(result.powersets_primary[2], "Blaster_Ranged.Beam_Rifle");
        assert_eq!(result.powersets_primary[3], "Blaster_Ranged.Dark_Blast");
        assert_eq!(result.powersets_primary[4], "Blaster_Ranged.Dual_Pistols");
        assert_eq!(
            result.powersets_primary[5],
            "Blaster_Ranged.Electrical_Blast"
        );
        assert_eq!(result.powersets_primary[6], "Blaster_Ranged.Energy_Blast");
        assert_eq!(result.powersets_primary[7], "Blaster_Ranged.Fire_Blast");
        assert_eq!(result.powersets_primary[8], "Blaster_Ranged.Ice_Blast");
        assert_eq!(result.powersets_primary[9], "Blaster_Ranged.Psychic_Blast");
        assert_eq!(
            result.powersets_primary[10],
            "Blaster_Ranged.Radiation_Blast"
        );
        assert_eq!(result.powersets_primary[11], "Blaster_Ranged.Sonic_Attack");
        assert_eq!(result.powersets_primary[12], "Blaster_Ranged.Water_Blast");
        assert_eq!(
            result.powersets_secondary[0],
            "Blaster_Support.Radiation_Manipulation"
        );
        assert_eq!(
            result.powersets_secondary[1],
            "Blaster_Support.Darkness_Manipulation"
        );
        assert_eq!(result.powersets_secondary[2], "Blaster_Support.Gadgets");
        assert_eq!(
            result.powersets_secondary[3],
            "Blaster_Support.Electricity_Manipulation"
        );
        assert_eq!(
            result.powersets_secondary[4],
            "Blaster_Support.Energy_Manipulation"
        );
        assert_eq!(
            result.powersets_secondary[5],
            "Blaster_Support.Fire_Manipulation"
        );
        assert_eq!(
            result.powersets_secondary[6],
            "Blaster_Support.Ice_Manipulation"
        );
        assert_eq!(
            result.powersets_secondary[7],
            "Blaster_Support.Martial_Manipulation"
        );
        assert_eq!(
            result.powersets_secondary[8],
            "Blaster_Support.Mental_Manipulation"
        );
        assert_eq!(
            result.powersets_secondary[9],
            "Blaster_Support.Ninja_Training"
        );
        assert_eq!(
            result.powersets_secondary[10],
            "Blaster_Support.Plant_Manipulation"
        );
        assert_eq!(
            result.powersets_secondary[11],
            "Blaster_Support.Tactical_Arrow"
        );
        assert_eq!(
            result.powersets_secondary[12],
            "Blaster_Support.Temporal_Manipulation"
        );
    }

    #[test]
    #[allow(unused_must_use)]
    fn test2() {
        let mut file = File::open(&path("./data/archetypes.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: Vec<super::Archetype> = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.len(), 61);
    }
}
