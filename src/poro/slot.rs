use crate::enums::enhancement_grade::EnhancementGrade;
use crate::enums::enhancement_relative_level::EnhancementRelativeLevel;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct Slot {
    pub enhancement_id: String,
    pub relative_level: EnhancementRelativeLevel,
    pub grade: EnhancementGrade,
    pub level: i32,
    pub enhancement_level: i32,
}

#[allow(dead_code)]
impl Slot {
    pub fn new() -> Slot {
        let slot = Slot {
            enhancement_id: String::from(""),
            relative_level: EnhancementRelativeLevel::None,
            grade: EnhancementGrade::None,
            level: 0,
            enhancement_level: 0,
        };
        slot
    }
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::prelude::*;

    use crate::enums::enhancement_grade::EnhancementGrade;
    use crate::enums::enhancement_relative_level::EnhancementRelativeLevel;
    use crate::scatterbase::functions::cross_platform::path;

    #[test]
    #[allow(unused_must_use)]
    fn test1() {
        let mut file = File::open(&path("./data/slot.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: super::Slot = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.enhancement_id, "Crafted_Absolute_Amazement_F");
        assert_eq!(result.relative_level, EnhancementRelativeLevel::Even);
        assert_eq!(result.grade, EnhancementGrade::None);
        assert_eq!(result.level, 0);
        assert_eq!(result.enhancement_level, 49);
    }

    #[test]
    #[allow(unused_must_use)]
    fn test2() {
        let mut file = File::open(&path("./data/slots.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: Vec<super::Slot> = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.len(), 3);
    }
}
