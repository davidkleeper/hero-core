use crate::poro::build::Build;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct Toon {
    pub version: String,
    pub name: String,
    pub level: u16,
    pub archetype_id: String,
    pub current_build: usize,
    pub builds: Vec<Build>,
    pub notes: Option<String>,
}

#[allow(dead_code)]
impl Toon {
    pub fn new() -> Toon {
        let mut toon = Toon {
            version: String::from("0.0.0"),
            name: String::from(""),
            level: 1,
            archetype_id: String::from(""),
            current_build: 0,
            builds: Vec::<Build>::new(),
            notes: None,
        };
        toon.builds.push(Build::new());
        toon
    }

    pub fn from_archetype(archetype: &str) -> Toon {
        let mut toon = Toon::new();
        let build = Build::from_archetype(archetype);
        toon.archetype_id = String::from(archetype);
        toon.builds = Vec::<Build>::new();
        toon.builds.push(build);
        toon
    }
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use std::fs;
    use std::fs::File;
    use std::io::prelude::*;

    use crate::scatterbase::functions::cross_platform::path;

    #[test]
    #[allow(unused_must_use)]
    fn test_build_from_json() {
        let mut file = File::open(&path("./data/ai-toon.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: super::Toon = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.name, "Mind Force Field Controller");
        assert_eq!(result.archetype_id, "Class_Controller");
        assert_eq!(result.current_build, 0);
        assert_eq!(result.builds.len(), 1);
    }

    #[test]
    #[allow(unused_must_use)]
    fn test_read_hero_base() {
        // Check that we can read all of HeroBase without a panic.
        let mut dirs: Vec<String> = Vec::<String>::new();
        dirs.push(String::from(path("./HeroBase/Arachnos Soldier")));
        dirs.push(String::from(path("./HeroBase/Arachnos Widow")));
        dirs.push(String::from(path("./HeroBase/Blaster")));
        dirs.push(String::from(path("./HeroBase/Brute")));
        dirs.push(String::from(path("./HeroBase/Controller")));
        dirs.push(String::from(path("./HeroBase/Corruptor")));
        dirs.push(String::from(path("./HeroBase/Defender")));
        dirs.push(String::from(path("./HeroBase/Dominator")));
        dirs.push(String::from(path("./HeroBase/Mastermind")));
        dirs.push(String::from(path("./HeroBase/Peacebringer")));
        dirs.push(String::from(path("./HeroBase/Scrapper")));
        dirs.push(String::from(path("./HeroBase/Sentinel")));
        dirs.push(String::from(path("./HeroBase/Stalker")));
        dirs.push(String::from(path("./HeroBase/Tanker")));
        dirs.push(String::from(path("./HeroBase/Warshade")));

        for dir in dirs.iter() {
            let paths = fs::read_dir(dir).unwrap();
            for path in paths {
                let file_name = path.unwrap().path();
                let file_contents = std::fs::read_to_string(&file_name).unwrap();
                let _result: super::Toon = serde_json::from_str(&file_contents).unwrap();
            }
        }
    }
}
