use serde::{Deserialize, Serialize};
use std::collections::HashMap;

use crate::enums::build_power_sets::BuildPowerSets;
use crate::enums::power_level::PowerLevel;
use crate::functions::power_entry::add_empty_slot;
use crate::poro::power_entry::PowerEntry;

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct Build {
    pub power_sets: HashMap<BuildPowerSets, Option<String>>,
    pub power_entries: HashMap<PowerLevel, Option<PowerEntry>>,
}

#[allow(dead_code)]
impl Build {
    pub fn new() -> Build {
        let mut build = Build {
            power_sets: HashMap::<BuildPowerSets, Option<String>>::new(),
            power_entries: HashMap::<PowerLevel, Option<PowerEntry>>::new(),
        };
        build.power_sets.insert(
            BuildPowerSets::inherent,
            Some(String::from("Inherent.Inherent")),
        );
        build.power_sets.insert(
            BuildPowerSets::fitness,
            Some(String::from("Inherent.Fitness")),
        );
        build.power_sets.insert(BuildPowerSets::primary, None);
        build.power_sets.insert(BuildPowerSets::secondary, None);
        build.power_sets.insert(BuildPowerSets::pool_1, None);
        build.power_sets.insert(BuildPowerSets::pool_2, None);
        build.power_sets.insert(BuildPowerSets::pool_3, None);
        build.power_sets.insert(BuildPowerSets::pool_4, None);
        build.power_sets.insert(BuildPowerSets::epic, None);

        let mut brawl: PowerEntry = PowerEntry::new();
        brawl.level = 1;
        brawl.power_id = String::from("Inherent.Inherent.Brawl");
        add_empty_slot(&mut brawl);
        let mut health: PowerEntry = PowerEntry::new();
        health.level = 1;
        health.power_id = String::from("Inherent.Fitness.Health");
        add_empty_slot(&mut health);
        let mut stamina: PowerEntry = PowerEntry::new();
        stamina.level = 1;
        stamina.power_id = String::from("Inherent.Fitness.Stamina");
        add_empty_slot(&mut stamina);
        let mut prestige_power_slide: PowerEntry = PowerEntry::new();
        prestige_power_slide.level = 1;
        prestige_power_slide.power_id = String::from("Inherent.Inherent.prestige_DVD_Glidep");
        add_empty_slot(&mut prestige_power_slide);
        build.power_entries.insert(PowerLevel::brawl, Some(brawl));
        build.power_entries.insert(PowerLevel::health, Some(health));
        build
            .power_entries
            .insert(PowerLevel::stamina, Some(stamina));
        build.power_entries.insert(PowerLevel::swift, None);
        build.power_entries.insert(PowerLevel::hurdle, None);
        build.power_entries.insert(PowerLevel::sprint, None);
        build.power_entries.insert(PowerLevel::prestige_power_slide, Some(prestige_power_slide));
        build.power_entries.insert(PowerLevel::level_1_primary, None);
        build.power_entries.insert(PowerLevel::level_1_secondary, None);
        build.power_entries.insert(PowerLevel::level_2, None);
        build.power_entries.insert(PowerLevel::level_4, None);
        build.power_entries.insert(PowerLevel::level_6, None);
        build.power_entries.insert(PowerLevel::level_8, None);
        build.power_entries.insert(PowerLevel::level_10, None);
        build.power_entries.insert(PowerLevel::level_12, None);
        build.power_entries.insert(PowerLevel::level_14, None);
        build.power_entries.insert(PowerLevel::level_16, None);
        build.power_entries.insert(PowerLevel::level_18, None);
        build.power_entries.insert(PowerLevel::level_20, None);
        build.power_entries.insert(PowerLevel::level_22, None);
        build.power_entries.insert(PowerLevel::level_24, None);
        build.power_entries.insert(PowerLevel::level_26, None);
        build.power_entries.insert(PowerLevel::level_28, None);
        build.power_entries.insert(PowerLevel::level_30, None);
        build.power_entries.insert(PowerLevel::level_32, None);
        build.power_entries.insert(PowerLevel::level_35, None);
        build.power_entries.insert(PowerLevel::level_38, None);
        build.power_entries.insert(PowerLevel::level_41, None);
        build.power_entries.insert(PowerLevel::level_44, None);
        build.power_entries.insert(PowerLevel::level_47, None);
        build.power_entries.insert(PowerLevel::level_49, None);
        build.power_entries.insert(PowerLevel::level_1_kheldian, None);
        build.power_entries.insert(PowerLevel::level_10_kheldian, None);
        build.power_entries.insert(PowerLevel::nova_1, None);
        build.power_entries.insert(PowerLevel::nova_2, None);
        build.power_entries.insert(PowerLevel::nova_3, None);
        build.power_entries.insert(PowerLevel::nova_4, None);
        build.power_entries.insert(PowerLevel::dwarf_1, None);
        build.power_entries.insert(PowerLevel::dwarf_2, None);
        build.power_entries.insert(PowerLevel::dwarf_3, None);
        build.power_entries.insert(PowerLevel::dwarf_4, None);
        build.power_entries.insert(PowerLevel::dwarf_5, None);
        build.power_entries.insert(PowerLevel::dwarf_6, None);
        build
    }

    pub fn from_archetype(archetype: &str) -> Build {
        let mut build = Build::new();
        if archetype == "Class_Peacebringer" {
            let mut level_1_kheldian: PowerEntry = PowerEntry::new();
            level_1_kheldian.level = 1;
            level_1_kheldian.power_id = String::from("Inherent.Inherent.Energy_Flight");
            add_empty_slot(&mut level_1_kheldian);
            build.power_entries.remove(&PowerLevel::level_1_kheldian);
            build.power_entries.insert(PowerLevel::level_1_kheldian, Some(level_1_kheldian));
        } else if archetype == "Class_Warshade" {
            let mut level_1_kheldian: PowerEntry = PowerEntry::new();
            level_1_kheldian.level = 1;
            level_1_kheldian.power_id = String::from("Inherent.Inherent.Shadow_Step");
            add_empty_slot(&mut level_1_kheldian);
            build.power_entries.remove(&PowerLevel::level_1_kheldian);
            build.power_entries.insert(PowerLevel::level_1_kheldian, Some(level_1_kheldian));
        }
        build
    }
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::prelude::*;

    use crate::enums::build_power_sets::BuildPowerSets;
    use crate::enums::power_level::PowerLevel;
    use crate::scatterbase::functions::cross_platform::path;

    #[test]
    #[allow(unused_must_use)]
    fn test_read_build() {
        let mut file = File::open(&path("./data/ai-build.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: super::Build = serde_json::from_str(&contents).unwrap();

        assert_eq!(result.power_sets.len(), 8);
        assert_eq!(result.power_entries.len(), 27);

        assert_eq!(
            result
                .power_sets
                .get(&BuildPowerSets::fitness)
                .unwrap()
                .as_ref()
                .unwrap(),
            "Inherent.Fitness"
        );
        assert_eq!(
            result
                .power_sets
                .get(&BuildPowerSets::primary)
                .unwrap()
                .as_ref()
                .unwrap(),
            "Controller_Control.Mind_Control"
        );
        assert_eq!(
            result
                .power_sets
                .get(&BuildPowerSets::secondary)
                .unwrap()
                .as_ref()
                .unwrap(),
            "Controller_Buff.Force_Field"
        );
        assert_eq!(
            result
                .power_sets
                .get(&BuildPowerSets::pool_1)
                .unwrap()
                .as_ref()
                .unwrap(),
            "Pool.Speed"
        );
        assert_eq!(
            result
                .power_sets
                .get(&BuildPowerSets::pool_2)
                .unwrap()
                .as_ref()
                .unwrap(),
            "Pool.Invisibility"
        );
        assert_eq!(
            result
                .power_sets
                .get(&BuildPowerSets::pool_3)
                .unwrap()
                .as_ref()
                .unwrap(),
            "Pool.Fighting"
        );
        assert_eq!(
            result
                .power_sets
                .get(&BuildPowerSets::pool_4)
                .unwrap()
                .as_ref()
                .unwrap(),
            "Pool.Force_of_Will"
        );

        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::brawl)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Inherent.Inherent.Brawl"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::health)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Inherent.Fitness.Health"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::stamina)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Inherent.Fitness.Stamina"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_1_primary)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Controller_Control.Mind_Control.Mesmerize"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_1_secondary)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Controller_Buff.Force_Field.Personal_Force_Field"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_2)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Controller_Buff.Force_Field.Deflection_Shield"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_4)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Controller_Control.Mind_Control.Levitate"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_6)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Controller_Control.Mind_Control.Dominate"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_8)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Controller_Control.Mind_Control.Confuse"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_10)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Pool.Invisibility.Stealth"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_12)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Pool.Invisibility.Grant_Invisibility"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_14)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Controller_Buff.Force_Field.Insulation_Shield"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_16)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Pool.Invisibility.Invisibility"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_18)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Controller_Control.Mind_Control.Mass_Hypnosis"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_20)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Controller_Control.Mind_Control.Telekinesis"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_22)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Controller_Buff.Force_Field.Dispersion_Bubble"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_24)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Controller_Control.Mind_Control.Total_Domination"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_26)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Controller_Control.Mind_Control.Terrify"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_28)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Pool.Fighting.Boxing"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_30)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Pool.Fighting.Tough"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_32)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Pool.Fighting.Weave"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_35)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Controller_Control.Mind_Control.Mass_Confusion"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_38)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Controller_Buff.Force_Field.Force_Bubble"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_41)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Controller_Buff.Force_Field.Repulsion_Bomb"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_44)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Controller_Buff.Force_Field.Force_Bolt"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_47)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Controller_Buff.Force_Field.Repulsion_Field"
        );
        assert_eq!(
            result
                .power_entries
                .get(&PowerLevel::level_49)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Controller_Buff.Force_Field.Refraction_Shield"
        );
        assert_eq!(
            result.power_entries.get(&PowerLevel::level_1_kheldian),
            None
        );
        assert_eq!(
            result.power_entries.get(&PowerLevel::level_10_kheldian),
            None
        );
        assert_eq!(result.power_entries.get(&PowerLevel::nova_1), None);
        assert_eq!(result.power_entries.get(&PowerLevel::nova_2), None);
        assert_eq!(result.power_entries.get(&PowerLevel::nova_3), None);
        assert_eq!(result.power_entries.get(&PowerLevel::nova_4), None);
        assert_eq!(result.power_entries.get(&PowerLevel::dwarf_1), None);
        assert_eq!(result.power_entries.get(&PowerLevel::dwarf_2), None);
        assert_eq!(result.power_entries.get(&PowerLevel::dwarf_3), None);
        assert_eq!(result.power_entries.get(&PowerLevel::dwarf_4), None);
        assert_eq!(result.power_entries.get(&PowerLevel::dwarf_5), None);
        assert_eq!(result.power_entries.get(&PowerLevel::dwarf_6), None);
    }
}
