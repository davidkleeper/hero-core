use crate::enums::pvx::PvX;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct BonusItem {
    pub alt_string: Option<String>,
    pub names: Vec<String>,
    pub power_ids: Vec<String>,
    pub pv_mode: PvX,
    pub slotted: i16,
    pub special: i16,
}

#[allow(dead_code)]
impl BonusItem {
    pub fn new() -> BonusItem {
        let bonus_item = BonusItem {
            alt_string: Some(String::from("")),
            names: Vec::<String>::new(),
            power_ids: Vec::<String>::new(),
            pv_mode: PvX::Any,
            slotted: 0,
            special: 0,
        };
        bonus_item
    }
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::prelude::*;

    use crate::enums::pvx::PvX;
    use crate::scatterbase::functions::cross_platform::path;

    #[test]
    #[allow(unused_must_use)]
    fn test1() {
        let mut file = File::open(&path("./data/bonus-item.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: super::BonusItem = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.alt_string, Some(String::from("")));
        assert_eq!(result.names.len(), 1);
        assert_eq!(result.names[0], "Set_Bonus.Set_Bonus.Improved_Recovery_1");
        assert_eq!(result.power_ids.len(), 1);
        assert_eq!(
            result.power_ids[0],
            "Set_Bonus.Set_Bonus.Improved_Recovery_1"
        );
        assert_eq!(result.pv_mode, PvX::Any);
        assert_eq!(result.slotted, 2);
        assert_eq!(result.special, -1);
    }

    #[test]
    #[allow(unused_must_use)]
    fn test2() {
        let mut file = File::open(&path("./data/bonus-items.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: Vec<super::BonusItem> = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.len(), 3);
    }
}
