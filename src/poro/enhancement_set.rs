use crate::enums::set_type::SetType;
use crate::poro::bonus_item::BonusItem;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct EnhancementSet {
    pub bonuses: Option<Vec<BonusItem>>,
    pub bonuses_special: Option<Vec<BonusItem>>,
    pub description: String,
    pub display_name: Option<String>,
    pub enhancement_ids: Option<Vec<String>>,
    pub id: String,
    pub image: String,
    pub image_index: i16,
    pub level_max: i16,
    pub level_min: i16,
    pub set_type: Option<SetType>,
    pub short_name: String,
}

#[allow(dead_code)]
impl EnhancementSet {
    pub fn new() -> EnhancementSet {
        let enhancement_set = EnhancementSet {
            bonuses: Some(Vec::<BonusItem>::new()),
            bonuses_special: Some(Vec::<BonusItem>::new()),
            description: String::from(""),
            display_name: Some(String::from("")),
            enhancement_ids: Some(Vec::<String>::new()),
            id: String::from(""),
            image: String::from(""),
            image_index: 0,
            level_max: 0,
            level_min: 0,
            set_type: Some(SetType::AccDefDeb),
            short_name: String::from(""),
        };
        enhancement_set
    }
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::prelude::*;

    use crate::enums::set_type::SetType;
    use crate::scatterbase::functions::cross_platform::path;

    #[test]
    #[allow(unused_must_use)]
    fn test1() {
        let mut file = File::open(&path("./data/enhancement-set.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: super::EnhancementSet = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.bonuses.unwrap().len(), 3);
        assert_eq!(result.bonuses_special.unwrap().len(), 6);
        assert_eq!(result.description, "");
        assert_eq!(result.display_name, None);
        let enhancement_ids = result.enhancement_ids.unwrap();
        assert_eq!(enhancement_ids.len(), 4);
        assert_eq!(enhancement_ids[0], "Crafted_Cleaving_Blow_A");
        assert_eq!(enhancement_ids[1], "Crafted_Cleaving_Blow_B");
        assert_eq!(enhancement_ids[2], "Crafted_Cleaving_Blow_C");
        assert_eq!(enhancement_ids[3], "Crafted_Cleaving_Blow_D");
        assert_eq!(result.id, "Cleaving_Blow");
        assert_eq!(result.image, "sCleavingBlow.png");
        assert_eq!(result.image_index, 10);
        assert_eq!(result.level_max, 49);
        assert_eq!(result.level_min, 9);
        assert_eq!(result.set_type.unwrap(), SetType::MeleeAoE);
        assert_eq!(result.short_name, "ClvBlo");
    }

    #[test]
    #[allow(unused_must_use)]
    fn test2() {
        let mut file = File::open(&path("./data/enhancements.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: Vec<super::EnhancementSet> = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.len(), 1288);
    }
}
