use serde::{Deserialize, Serialize};

use crate::enums::aspect::Aspect;
use crate::enums::attribute_type::AttributeType;
use crate::enums::buff_mode::BuffMode;
use crate::enums::damage::Damage;
use crate::enums::effect_class::EffectClass;
use crate::enums::effect_type::EffectType;
use crate::enums::mez::Mez;
use crate::enums::override_boolean::OverrideBoolean;
use crate::enums::power_type::PowerType;
use crate::enums::pvx::PvX;
use crate::enums::special_case::SpecialCase;
use crate::enums::stacking::Stacking;
use crate::enums::suppress::Suppress;
use crate::enums::to_who::ToWho;
use crate::poro::effect::Effect;

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct NewEffect {
    pub absorbed_class_id: i32,
    pub absorbed_duration: f32,
    pub absorbed_effect: bool,
    pub absorbed_effect_id: i32,
    pub absorbed_interval: f32,
    pub absorbed_power_id: String,
    pub absorbed_power_type: PowerType,
    pub aspect: Aspect,
    pub attribute_type: AttributeType,
    pub buff_mode: BuffMode,
    pub buffable: bool,
    pub cancel_on_miss: bool,
    pub damage_type: Damage,
    pub delayed_time: f32,
    pub display_percentage: bool,
    pub display_percentage_override: OverrideBoolean,
    pub duration: f32,
    pub effect_class: EffectClass,
    pub effect_id: String,
    pub effect_type: EffectType,
    pub effect_type_modifies: EffectType,
    pub enhancement_long_name: String,
    pub id: String,
    pub inherent_special: bool,
    pub is_enhancement_effect: bool,
    pub mag: f32,
    pub mag_percent: f32,
    pub magnitude: f32,
    pub magnitude_expression: String,
    pub math_duration: f32,
    pub math_mag: f32,
    pub mez_type: Mez,
    pub modifier_table: String,
    pub modifier_table_index: i32,
    pub near_ground: bool,
    pub override_count: i32,
    pub override_value: String,
    pub power_id: String,
    pub probability: f32,
    pub probability_base: f32,
    pub procs_per_minute: f32,
    pub pv_mode: PvX,
    pub requires_to_hit_check: bool,
    pub resistible: bool,
    pub reward: String,
    pub scale: f32,
    pub special: String,
    pub special_case: SpecialCase,
    pub stacking: Stacking,
    pub summon: String,
    pub summon_count: i32,
    pub suppression: u32,
    pub ticks: i32,
    pub to_who: ToWho,
    pub id_class_name: String,
    pub unique_id: i32,
    pub variable_modified: bool,
    pub variable_modified_override: bool,
}

#[allow(dead_code)]
impl NewEffect {
    pub fn new() -> NewEffect {
        let effect = NewEffect {
            absorbed_class_id: 0,
            absorbed_duration: 0.0f32,
            absorbed_effect: false,
            absorbed_effect_id: 0,
            absorbed_interval: 0.0f32,
            absorbed_power_id: String::from(""),
            absorbed_power_type: PowerType::Click,
            aspect: Aspect::Res,
            attribute_type: AttributeType::Magnitude,
            buff_mode: BuffMode::Normal,
            buffable: false,
            cancel_on_miss: false,
            damage_type: Damage::None,
            delayed_time: 0.0f32,
            display_percentage: false,
            display_percentage_override: OverrideBoolean::NoOverride,
            duration: 0.0f32,
            effect_class: EffectClass::Primary,
            effect_id: String::from(""),
            effect_type: EffectType::None,
            effect_type_modifies: EffectType::None,
            enhancement_long_name: String::from(""),
            id: String::from(""),
            inherent_special: false,
            is_enhancement_effect: false,
            mag: 0.0f32,
            mag_percent: 0.0f32,
            magnitude: 0.0f32,
            magnitude_expression: String::from(""),
            math_duration: 0.0f32,
            math_mag: 0.0f32,
            mez_type: Mez::None,
            modifier_table: String::from(""),
            modifier_table_index: 0,
            near_ground: false,
            override_count: 0,
            override_value: String::from(""),
            power_id: String::from(""),
            probability: 0.0f32,
            probability_base: 0.0f32,
            procs_per_minute: 0.0f32,
            pv_mode: PvX::Any,
            requires_to_hit_check: false,
            resistible: false,
            reward: String::from(""),
            scale: 0.0f32,
            special: String::from(""),
            special_case: SpecialCase::None,
            stacking: Stacking::No,
            summon: String::from(""),
            summon_count: 0,
            suppression: Suppress::None,
            ticks: 0,
            to_who: ToWho::Unspecified,
            id_class_name: String::from(""),
            unique_id: 0,
            variable_modified: false,
            variable_modified_override: false,
        };
        effect
    }

    pub fn convert(effect: &Effect, effect_number: u16) -> NewEffect {
        let effect = NewEffect {
            absorbed_class_id: effect.absorbed_class_id.clone(),
            absorbed_duration: effect.absorbed_duration.clone(),
            absorbed_effect: effect.absorbed_effect.clone(),
            absorbed_effect_id: effect.absorbed_effect_id.clone(),
            absorbed_interval: effect.absorbed_interval.clone(),
            absorbed_power_id: effect.absorbed_power_id.clone(),
            absorbed_power_type: effect.absorbed_power_type.clone(),
            aspect: effect.aspect.clone(),
            attribute_type: effect.attribute_type.clone(),
            buff_mode: effect.buff_mode.clone(),
            buffable: effect.buffable.clone(),
            cancel_on_miss: effect.cancel_on_miss.clone(),
            damage_type: effect.damage_type.clone(),
            delayed_time: effect.delayed_time.clone(),
            display_percentage: effect.display_percentage.clone(),
            display_percentage_override: effect.display_percentage_override.clone(),
            duration: effect.duration.clone(),
            effect_class: effect.effect_class.clone(),
            effect_id: effect.effect_id.clone(),
            effect_type: effect.effect_type.clone(),
            effect_type_modifies: effect.effect_type_modifies.clone(),
            enhancement_long_name: effect.enhancement_long_name.clone(),
            id: format!("{}.Effect.{}", effect.power_id, effect_number),
            inherent_special: effect.inherent_special.clone(),
            is_enhancement_effect: effect.is_enhancement_effect.clone(),
            mag: effect.mag.clone(),
            mag_percent: effect.mag_percent.clone(),
            magnitude: effect.magnitude.clone(),
            magnitude_expression: effect.magnitude_expression.clone(),
            math_duration: effect.math_duration.clone(),
            math_mag: effect.math_mag.clone(),
            mez_type: effect.mez_type.clone(),
            modifier_table: effect.modifier_table.clone(),
            modifier_table_index: effect.modifier_table_index.clone(),
            near_ground: effect.near_ground.clone(),
            override_count: effect.override_count.clone(),
            override_value: effect.override_value.clone(),
            power_id: effect.power_id.clone(),
            probability: effect.probability.clone(),
            probability_base: effect.probability_base.clone(),
            procs_per_minute: effect.procs_per_minute.clone(),
            pv_mode: effect.pv_mode.clone(),
            requires_to_hit_check: effect.requires_to_hit_check.clone(),
            resistible: effect.resistible.clone(),
            reward: effect.reward.clone(),
            scale: effect.scale.clone(),
            special: effect.special.clone(),
            special_case: effect.special_case.clone(),
            stacking: effect.stacking.clone(),
            summon: effect.summon.clone(),
            summon_count: effect.summon_count.clone(),
            suppression: effect.suppression.clone(),
            ticks: effect.ticks.clone(),
            to_who: effect.to_who.clone(),
            id_class_name: effect.id_class_name.clone(),
            unique_id: effect.unique_id.clone(),
            variable_modified: effect.variable_modified.clone(),
            variable_modified_override: effect.variable_modified_override.clone(),
        };
        effect
    }

    pub fn batch_convert(effects: &Vec<Effect>) -> (Vec<NewEffect>, Vec<String>) {
        let mut new_effects = Vec::<NewEffect>::new();
        let mut new_effect_ids = Vec::<String>::new();
        let mut count = 0;
        for effect in effects {
            count += 1;
            let new_effect = NewEffect::convert(effect, count);
            new_effects.push(new_effect.clone());
            new_effect_ids.push(new_effect.id.clone());
        }
        (new_effects, new_effect_ids)
    }
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::prelude::*;

    use crate::enums::aspect::Aspect;
    use crate::enums::attribute_type::AttributeType;
    use crate::enums::buff_mode::BuffMode;
    use crate::enums::damage::Damage;
    use crate::enums::effect_class::EffectClass;
    use crate::enums::effect_type::EffectType;
    use crate::enums::mez::Mez;
    use crate::enums::override_boolean::OverrideBoolean;
    use crate::enums::power_type::PowerType;
    use crate::enums::pvx::PvX;
    use crate::enums::special_case::SpecialCase;
    use crate::enums::stacking::Stacking;
    use crate::enums::suppress::Suppress;
    use crate::enums::to_who::ToWho;
    use crate::scatterbase::functions::cross_platform::path;

    #[test]
    #[allow(unused_must_use)]
    fn test1() {
        let mut file = File::open(&path("./data/new_effect.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: super::NewEffect = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.absorbed_class_id, -1);
        assert_eq!(result.absorbed_duration, 0.0f32);
        assert_eq!(result.absorbed_effect, false);
        assert_eq!(result.absorbed_effect_id, -1);
        assert_eq!(result.absorbed_interval, 0.0f32);
        assert_eq!(result.absorbed_power_id, "");
        assert_eq!(result.absorbed_power_type, PowerType::Automatic);
        assert_eq!(result.aspect, Aspect::Cur);
        assert_eq!(result.attribute_type, AttributeType::Duration);
        assert_eq!(result.attribute_type, AttributeType::Duration);
        assert_eq!(result.buffable, false);
        assert_eq!(result.buff_mode, BuffMode::Normal);
        assert_eq!(result.cancel_on_miss, false);
        assert_eq!(result.damage_type, Damage::None);
        assert_eq!(result.delayed_time, 0.0f32);
        assert_eq!(result.display_percentage, false);
        assert_eq!(
            result.display_percentage_override,
            OverrideBoolean::NoOverride
        );
        assert_eq!(result.duration, 0.75f32);
        assert_eq!(result.effect_class, EffectClass::Primary);
        assert_eq!(result.effect_id, "Ones");
        assert_eq!(result.effect_type, EffectType::Mez);
        assert_eq!(result.effect_type_modifies, EffectType::None);
        assert_eq!(result.enhancement_long_name, "");
        assert_eq!(result.id, "ID");
        assert_eq!(result.inherent_special, false);
        assert_eq!(result.is_enhancement_effect, false);
        assert_eq!(result.mag, 100.0f32);
        assert_eq!(result.mag_percent, 100.0f32);
        assert_eq!(result.magnitude, 100.0f32);
        assert_eq!(result.magnitude_expression, "");
        assert_eq!(result.math_duration, 0.0f32);
        assert_eq!(result.math_mag, 0.0f32);
        assert_eq!(result.mez_type, Mez::OnlyAffectsSelf);
        assert_eq!(result.modifier_table, "Melee_Ones");
        assert_eq!(result.modifier_table_index, 41);
        assert_eq!(result.near_ground, false);
        assert_eq!(result.override_value, "");
        assert_eq!(result.override_count, -1);
        assert_eq!(
            result.power_id,
            "Defender_Buff.Force_Field.Personal_Force_Field"
        );
        assert_eq!(result.probability, 1.0f32);
        assert_eq!(result.probability_base, 1.0f32);
        assert_eq!(result.procs_per_minute, 0.0f32);
        assert_eq!(result.pv_mode, PvX::Any);
        assert_eq!(result.requires_to_hit_check, false);
        assert_eq!(result.resistible, false);
        assert_eq!(result.reward, "");
        assert_eq!(result.scale, 0.75f32);
        assert_eq!(result.special_case, SpecialCase::None);
        assert_eq!(result.stacking, Stacking::No);
        assert_eq!(result.summon, "");
        assert_eq!(result.summon_count, -1);
        assert_eq!(result.suppression, Suppress::MissionObjectClick);
        assert_eq!(result.ticks, 0);
        assert_eq!(result.to_who, ToWho::ToSelf);
        assert_eq!(result.id_class_name, "");
        assert_eq!(result.variable_modified, false);
        assert_eq!(result.variable_modified_override, false);
    }
}
