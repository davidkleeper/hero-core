use crate::enums::buff_debuff::BuffDebuff;
use crate::enums::enhancement_mutex::EnhancementMutex;
use crate::enums::enhancement_sub_type::EnhancementSubtype;
use crate::enums::enhancement_type::EnhancementType;
use crate::enums::schedule::Schedule;
use crate::poro::s_effect::SEffect;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct Enhancement {
    pub buff_debuff: BuffDebuff,
    pub class_id: Vec<i16>,
    pub description: String,
    pub effect_chance: f32,
    pub effects: Vec<SEffect>,
    pub enhancement_set_id: String,
    pub id: String,
    pub image: String,
    pub image_index: i16,
    pub level_max: i16,
    pub level_min: i16,
    pub long_name: String,
    pub mutex_id: EnhancementMutex,
    pub name: String,
    pub power_id: String,
    pub probability: f32,
    pub schedule: Schedule,
    pub short_name: String,
    pub sub_type_id: EnhancementSubtype,
    pub superior: bool,
    pub type_id: EnhancementType,
    pub unique: bool,
}

#[allow(dead_code)]
impl Enhancement {
    pub fn new() -> Enhancement {
        let enhancement = Enhancement {
            buff_debuff: BuffDebuff::Any,
            class_id: Vec::<i16>::new(),
            description: String::from(""),
            effect_chance: 0.0f32,
            effects: Vec::<SEffect>::new(),
            enhancement_set_id: String::from(""),
            id: String::from(""),
            image: String::from(""),
            image_index: 0,
            level_max: 0,
            level_min: 0,
            long_name: String::from(""),
            mutex_id: EnhancementMutex::None,
            name: String::from(""),
            power_id: String::from(""),
            probability: 0.0f32,
            schedule: Schedule::None,
            short_name: String::from(""),
            sub_type_id: EnhancementSubtype::None,
            superior: false,
            type_id: EnhancementType::None,
            unique: false,
        };
        enhancement
    }
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::prelude::*;

    use crate::enums::buff_debuff::BuffDebuff;
    use crate::enums::enhancement_mutex::EnhancementMutex;
    use crate::enums::enhancement_sub_type::EnhancementSubtype;
    use crate::enums::enhancement_type::EnhancementType;
    use crate::enums::schedule::Schedule;
    use crate::scatterbase::functions::cross_platform::path;

    #[test]
    #[allow(unused_must_use)]
    fn test1() {
        let mut file = File::open(&path("./data/enhancement.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: super::Enhancement = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.buff_debuff, BuffDebuff::Any);
        assert_eq!(result.class_id.len(), 0);
        assert_eq!(result.description, "Unconfirmed Mag");
        assert_eq!(result.effect_chance, 1.0f32);
        assert_eq!(result.enhancement_set_id, "Absolute_Amazement");
        assert_eq!(result.id, "Crafted_Absolute_Amazement_F");
        assert_eq!(result.image, "sAbsoluteAmazement.png");
        assert_eq!(result.image_index, 591);
        assert_eq!(result.level_max, 49);
        assert_eq!(result.level_min, 49);
        assert_eq!(
            result.long_name,
            "Absolute Amazement: Chance for ToHit Debuff"
        );
        assert_eq!(result.mutex_id, EnhancementMutex::None);
        assert_eq!(result.name, "Chance for ToHit Debuff");
        assert_eq!(
            result.power_id,
            "Boosts.Crafted_Absolute_Amazement_F.Crafted_Absolute_Amazement_F"
        );
        assert_eq!(result.probability, 0.330000013f32);
        assert_eq!(result.schedule, Schedule::A);
        assert_eq!(result.short_name, "ToHitDeb%");
        assert_eq!(result.sub_type_id, EnhancementSubtype::None);
        assert_eq!(result.superior, true);
        assert_eq!(result.type_id, EnhancementType::SetO);
        assert_eq!(result.unique, true);
    }

    #[test]
    #[allow(unused_must_use)]
    fn test2() {
        let mut file = File::open(&path("./data/enhancements.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let result: Vec<super::Enhancement> = serde_json::from_str(&contents).unwrap();
        assert_eq!(result.len(), 1288);
    }
}
