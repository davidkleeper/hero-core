use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct PowerSubEntry {
    pub powerset_id: String,
    pub power_id: String,
    pub stat_include: bool,
}

#[allow(dead_code)]
impl PowerSubEntry {
    pub fn new() -> PowerSubEntry {
        let power_sub_entry = PowerSubEntry {
            powerset_id: String::from(""),
            power_id: String::from(""),
            stat_include: false,
        };
        power_sub_entry
    }
}
