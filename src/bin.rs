#![recursion_limit="256"]
extern crate scatterbase;
extern crate serde;

use hero_core::server;

pub fn main() {
    server::server::server();
}
