use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum PowerSetType {
    None = 0,
    Primary = 1,
    Secondary = 2,
    Ancillary = 3,
    Inherent = 4,
    Pool = 5,
    Accolade = 6,
    Temp = 7,
    Pet = 8,
    SetBonus = 9,
    Boost = 10,
    Incarnate = 11,
}
