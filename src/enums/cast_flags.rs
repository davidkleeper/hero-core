#[allow(dead_code)]
pub struct CastFlags {}
#[allow(dead_code)]
#[allow(non_upper_case_globals)]
impl CastFlags {
    pub const None: u32 = 0;
    pub const NearGround: u32 = 1;
    pub const TargetNearGround: u32 = 2;
    pub const CastableAfterDeath: u32 = 4;
}
