use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum Enhance {
    None = 0,
    Accuracy = 1,
    Damage = 2,
    Defense = 3,
    EnduranceDiscount = 4,
    Endurance = 5,
    SpeedFlying = 6,
    Heal = 7,
    HitPoints = 8,
    Interrupt = 9,
    JumpHeight = 10,
    SpeedJumping = 11,
    Mez = 12,
    Range = 13,
    RechargeTime = 14,
    XRechargeTime = 15,
    Recovery = 16,
    Regeneration = 17,
    Resistance = 18,
    SpeedRunning = 19,
    ToHit = 20,
    Slow = 21,
    Absorb = 22,
}
