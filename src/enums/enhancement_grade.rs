use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum EnhancementGrade {
    None = 0,
    Training = 1,
    Dual = 2,
    Single = 3,
}
