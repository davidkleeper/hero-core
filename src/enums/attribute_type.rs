use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum AttributeType {
    Magnitude = 0,
    Duration = 1,
    Expression = 2,
}
