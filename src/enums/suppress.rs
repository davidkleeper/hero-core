#[allow(dead_code)]
pub struct Suppress {}
#[allow(dead_code)]
#[allow(non_upper_case_globals)]
impl Suppress {
    pub const None: u32 = 0;
    pub const Held: u32 = 1;
    pub const Sleep: u32 = 2;
    pub const Stunned: u32 = 4;
    pub const Immobilized: u32 = 8;
    pub const Terrorized: u32 = 0x00000010;
    pub const Knocked: u32 = 0x00000020;
    pub const Attacked: u32 = 0x00000040;
    pub const HitByFoe: u32 = 0x00000080;
    pub const MissionObjectClick: u32 = 0x00000100;
    pub const ActivateAttackClick: u32 = 0x00000200;
    pub const Damaged: u32 = 0x00000400;
    pub const Phased1: u32 = 0x00000800;
    pub const Confused: u32 = 0x00001000;
}
