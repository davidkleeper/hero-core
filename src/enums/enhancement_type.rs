use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum EnhancementType {
    None = 0,
    Normal = 1,
    InventO = 2,
    SpecialO = 3,
    SetO = 4,
}
