use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum EffectMode {
    Enhancement = 0,
    FX = 1,
    PowerEnhance = 2,
    PowerProc = 3,
}
