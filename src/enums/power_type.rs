use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum PowerType {
    Click = 0,
    Automatic = 1,
    Toggle = 2,
    Boost = 3,
    Inspiration = 4,
    GlobalBoost = 5,
}
