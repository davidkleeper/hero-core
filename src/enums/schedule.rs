use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum Schedule {
    None = -1,
    A = 0,
    B = 1,
    C = 2,
    D = 3,
    Multiple = 4,
}
