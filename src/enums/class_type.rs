use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum ClassType {
    None = 0,
    Hero = 1,
    HeroEpic = 2,
    Villain = 3,
    VillainEpic = 4,
    Henchman = 5,
    Pet = 6,
}
