#[allow(dead_code)]
pub struct Entity {}

#[allow(dead_code)]
#[allow(non_upper_case_globals)]
impl Entity {
    pub const None: u32 = 0x00;
    pub const Caster: u32 = 1;
    pub const Player: u32 = 2;
    pub const DeadPlayer: u32 = 4;
    pub const Teammate: u32 = 8;
    pub const DeadTeammate: u32 = 0x00000010;
    pub const DeadOrAliveTeammate: u32 = 0x00000020;
    pub const Villain: u32 = 0x00000040;
    pub const DeadVillain: u32 = 0x00000080;
    pub const NPC: u32 = 0x00000100;
    pub const Friend: u32 = 0x00000200;
    pub const DeadFriend: u32 = 0x00000400;
    pub const Foe: u32 = 0x00000800;
    pub const Location: u32 = 0x00002000;
    pub const Teleport: u32 = 0x00004000;
    pub const Any: u32 = 0x00008000;
    pub const MyPet: u32 = 0x00010000;
    pub const DeadFoe: u32 = 0x00020000;
    pub const FoeRezzingFoe: u32 = 0x00040000;
    pub const Leaguemate: u32 = 0x00080000;
    pub const DeadLeaguemate: u32 = 0x00100000;
    pub const AnyLeaguemate: u32 = 0x00200000;
    pub const DeadMyCreation: u32 = 0x00400000;
    pub const DeadMyPet: u32 = 0x00800000;
    pub const DeadOrAliveFoe: u32 = 0x01000000;
    pub const DeadOrAliveLeaguemate: u32 = 0x02000000;
    pub const DeadPlayerFriend: u32 = 0x04000000;
    pub const MyOwner: u32 = 0x08000000;
}
