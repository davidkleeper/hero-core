#[allow(dead_code)]
pub struct Vector {}
#[allow(dead_code)]
#[allow(non_upper_case_globals)]
impl Vector {
    pub const None: u32 = 0;
    pub const MeleeAttack: u32 = 1;
    pub const RangedAttack: u32 = 2;
    pub const AOEAttack: u32 = 4;
    pub const SmashingAttack: u32 = 8;
    pub const LethalAttack: u32 = 0x00000010;
    pub const ColdAttack: u32 = 0x00000020;
    pub const FireAttack: u32 = 0x00000040;
    pub const EnergyAttack: u32 = 0x00000080;
    pub const NegativeEnergyAttack: u32 = 0x00000100;
    pub const PsionicAttack: u32 = 0x00000200;
}
