use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum EffectArea {
    None = 0,
    Character = 1,
    Sphere = 2,
    Cone = 3,
    Location = 4,
    Volume = 5,
    Map = 6,
    Room = 7,
    Touch = 8,
}
