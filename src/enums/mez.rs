use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum Mez {
    None = 0,
    Confused = 1,
    Held = 2,
    Immobilized = 3,
    Knockback = 4,
    Knockup = 5,
    OnlyAffectsSelf = 6,
    Placate = 7,
    Repel = 8,
    Sleep = 9,
    Stunned = 10,
    Taunt = 11,
    Terrorized = 12,
    Untouchable = 13,
    Teleport = 14,
    ToggleDrop = 15,
    Afraid = 16,
    Avoid = 17,
    CombatPhase = 18,
    Intangible = 19,
}
