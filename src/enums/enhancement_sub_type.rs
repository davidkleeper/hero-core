use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum EnhancementSubtype {
    None = 0,
    Hamidon = 1,
    Hydra = 2,
    Titan = 3,
}
