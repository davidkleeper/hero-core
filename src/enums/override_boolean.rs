use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum OverrideBoolean {
    NoOverride = 0,
    TrueOverride = 1,
    FalseOverride = 2,
}
