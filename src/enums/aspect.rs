use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum Aspect {
    Res = 0,
    Max = 1,
    Abs = 2,
    Str = 3,
    Cur = 4,
}
