use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum ToWho {
    Unspecified = 0,
    Target = 1,
    ToSelf = 2,
    All = 3,
}
