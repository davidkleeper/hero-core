#[allow(dead_code)]
pub struct ModeFlags {}
#[allow(dead_code)]
#[allow(non_upper_case_globals)]
impl ModeFlags {
    pub const None: u32 = 0;
    pub const Arena: u32 = 1;
    pub const DisableAll: u32 = 2;
    pub const DisableEnhancements: u32 = 4;
    pub const DisableEpic: u32 = 8;
    pub const DisableInspirations: u32 = 0x00000010;
    pub const DisableMarketTP: u32 = 0x00000020;
    pub const DisablePool: u32 = 0x00000040;
    pub const DisableRezInsp: u32 = 0x00000080;
    pub const DisableTeleport: u32 = 0x00000100;
    pub const DisableTemp: u32 = 0x00000200;
    pub const DisableToggle: u32 = 0x00000400;
    pub const DisableTravel: u32 = 0x00000800;
    pub const Domination: u32 = 0x00001000;
    pub const PeacebringerBlasterMode: u32 = 0x00002000;
    pub const PeacebringerLightformMode: u32 = 0x00004000;
    pub const PeacebringerTankerMode: u32 = 0x00008000;
    pub const RaidAttackerMode: u32 = 0x00010000;
    pub const ShivanMode: u32 = 0x00020000;
    pub const Unknown18: u32 = 0x00040000;
    pub const WarshadeBlasterMode: u32 = 0x00080000;
    pub const WarshadeTankerMode: u32 = 0x00100000;
}
