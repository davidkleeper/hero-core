use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum EnhancementMutex {
    None = 0,
    Stealth = 1,
    ArchetypeA = 2,
    ArchetypeB = 3,
    ArchetypeC = 4,
    ArchetypeD = 5,
    ArchetypeE = 6,
    ArchetypeF = 7,
}
