use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum EnhancementRelativeLevel {
    None = 0,
    MinusThree = 1,
    MinusTwo = 2,
    MinusOne = 3,
    Even = 4,
    PlusOne = 5,
    PlusTwo = 6,
    PlusThree = 7,
    PlusFour = 8,
    PlusFive = 9,
}
