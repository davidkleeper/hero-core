use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum Damage {
    None = 0,
    Smashing = 1,
    Lethal = 2,
    Fire = 3,
    Cold = 4,
    Energy = 5,
    Negative = 6,
    Toxic = 7,
    Psionic = 8,
    Special = 9,
    Melee = 10,
    Ranged = 11,
    AoE = 12,
    Unique1 = 13,
    Unique2 = 14,
    Unique3 = 15,
}
