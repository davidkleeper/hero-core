use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[allow(non_camel_case_types)]
#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum BuildPowerSets {
    fitness,
    primary,
    secondary,
    pool_1,
    pool_2,
    pool_3,
    pool_4,
    epic,
    inherent
}
