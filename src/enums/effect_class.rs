use serde::{Deserialize, Serialize};
use strum_macros::{EnumString, Display};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Hash, EnumString, Display)]
pub enum EffectClass {
    Primary = 0,
    Secondary = 1,
    Tertiary = 2,
    Special = 3,
    Ignored = 4,
    DisplayOnly = 5,
}
