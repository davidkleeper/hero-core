use chrono::{DateTime, Utc};
use std::env;
use std::io::prelude::*;
use std::net::{Shutdown, TcpListener, TcpStream};
use std::str;

use crate::functions::command_processor::process;
use crate::server::thread_pool::ThreadPool;

pub fn server() {
    let port = match env::var("PORT") {
        Ok(val) => format!("0.0.0.0:{}", val),
        Err(_e) => String::from("0.0.0.0:1337"),
    };
    let listener = TcpListener::bind(port).unwrap();
    let pool = ThreadPool::new(4);

    loop {
        for stream in listener.incoming().take(2) {
            let stream = stream.unwrap();

            pool.execute(|| {
                match handle_connection(stream) {
                    Ok(_r) => {
                        println!("Processed request successfully.");
                        ()
                    },
                    Err(e) => {
                        println!("{:?}", e);
                        ()
                    }
                }
            });
        }
    }
    // println!("Shutting down.");
}

fn get_headers_and_body(lines: std::str::Lines) -> Result<(Vec::<String>, String), std::io::Error>{
    let mut headers = Vec::<String>::new();
    let mut body = String::from("");
    let mut headers_done = false;
    
    for line in lines {
        if line == "" {
            headers_done = true;
            continue;
        }
        if !headers_done {
            headers.push(String::from(line));
        } else {
            body.push_str(line);
        }
    }
    Ok(( headers, body ))
}

pub fn get_parts(request: &str) -> Option<(&str, &str, Vec::<String>, String)>{
    let mut lines = request.lines();
    let first_line = match lines.next() {
        Some(line) => line,
        None => return None
    };
    println!("Received request {}", first_line );
    let mut words_iter = first_line.split_whitespace();
    let first_word = words_iter.next();
    if "OPTIONS" == first_word.unwrap().to_uppercase() {
        return Some(("OPTIONS", "", Vec::<String>::new(), String::from("")));
    }
    let path = match words_iter.next() {
        Some(p) => p,
        None => return None    
    };
    let mut path_iter = path.split('/');
    path_iter.next();
    let command = match path_iter.next() {
        Some(c) => c,
        None => return None    
    };
    let arg = match path_iter.next() {
        Some(i) => i,
        None => return None    
    };
    let (headers, body) = get_headers_and_body(lines).unwrap();
    Some((command, arg, headers, body))
}

fn handle_connection(mut stream: TcpStream) -> Result<bool, std::io::Error>{
    let mut buffer = [0; 100000];
    let byte_count = stream.read(&mut buffer).unwrap();
    let request: &str = str::from_utf8(&buffer[0..byte_count]).unwrap();
    let (command, arg, headers, body) = match get_parts(request){
        Some(arg) => arg,
        None => return Ok(false)
    };

    println!("command {}, arg {}, header length {}, body length {}", command, arg, headers.len(), body.len());
    if "stop" == command {
        return Ok(false)
    }
    if "OPTIONS" == command {
        println!("Sending OPTIONS response");
        send_options_response(&mut stream)?;
        return Ok(true)
    }
    let result = process(&command, &arg, headers, body)?;
    send(&mut stream, &result)?;
    Ok(true)
}

fn send(stream: &mut TcpStream, respone: &str)  -> Result<bool, std::io::Error>{
    let now: DateTime<Utc> = Utc::now();
    let date_header = now.format("Date: %a %b %d %Y %H:%M:%S GMT\r\n");
    let headers = format!(
        "{}{}{}{}{}{}",
        "HTTP/1.1 200 OK\r\n",
        "Access-Control-Allow-Origin: *\r\n",
        "Access-Control-Allow-Methods: *\r\n",
        "Access-Control-Allow-Headers: *\r\n",
        date_header,
        "\r\n"
    );
    stream.write(headers.as_bytes())?;
    println!("response size {}", respone.len());
    stream.write(respone.as_bytes())?;
    stream.flush()?;
    stream.shutdown(Shutdown::Both)?;
    Ok(true)
}

fn send_options_response(stream: &mut TcpStream)  -> Result<bool, std::io::Error>{
    let now: DateTime<Utc> = Utc::now();
    let date_header = now.format("Date: %a %b %d %Y %H:%M:%S GMT\r\n");
    let headers = format!(
        "{}{}{}{}{}{}",
        "HTTP/1.1 204 No Content\r\n",
        "Access-Control-Allow-Origin: *\r\n",
        "Access-Control-Allow-Methods: *\r\n",
        "Access-Control-Allow-Headers: *\r\n",
        date_header,
        "\r\n"
    );
    println!("OPTION response headers");
    println!("{}", headers);
    stream.write(headers.as_bytes())?;
    stream.flush()?;
    stream.shutdown(Shutdown::Both)?;
    Ok(true)
}


