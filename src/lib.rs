#![recursion_limit="256"]
#[macro_use] extern crate lazy_static;
extern crate scatterbase;
extern crate serde;
extern crate wasm_bindgen;
use wasm_bindgen::prelude::*;
use std::ffi::{CStr, CString};
use std::os::raw::c_char;
extern crate strum;

#[cfg(target_os = "android")]
mod android;

pub mod enums;
pub mod functions;
pub mod poro;
pub mod server;

#[no_mangle]
pub unsafe extern "C" fn core_command(command: *const c_char) -> *mut c_char {
    let c_str = CStr::from_ptr(command);
    let command = match c_str.to_str() {
        Ok(s) => s,
        Err(_) => "command",
    };

    let result = format!("{}", process_command(command));
    CString::new(result)
        .unwrap()
        .into_raw()
}

#[no_mangle]
pub unsafe extern "C" fn core_command_release(command: *mut c_char) {
    if command.is_null() {
        return;
    }
    CString::from_raw(command);
}

#[wasm_bindgen]
pub fn core_command_web(command: String) -> String {
    log(&format!("Command: {}:", command));
    process_command(&command)
}

fn process_command(command: &str) -> String {
    if "Command" == command {
        return String::from(command)
    }
    let (command, arg) = match functions::command_processor::get_command_and_arg(command){
        Some(arg) => arg,
        None => return String::from(format!("Error parsing command. {}", command))
    };

    let result = functions::command_processor::process(command, arg, Vec::<String>::new(), String::from("")).unwrap_or_else(|err| {
        return String::from(format!("Error in rust-core {} {} {}", command, arg, err))
    });
    result
}


#[wasm_bindgen]
extern "C" {
    // Use `js_namespace` here to bind `console.log(..)` instead of just
    // `log(..)`
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    #[test]
    fn test_process_command() {
        let result = super::process_command("command");
        println!("{}", result)
    }
}
