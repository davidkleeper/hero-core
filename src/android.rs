#![cfg(target_os = "android")]
#![allow(non_snake_case)]

use std::ffi::{CString, CStr};
use jni::JNIEnv;
use jni::objects::{JObject, JString};
use jni::sys::{jstring};

// Remember the JNI naming conventions.
#[no_mangle]
pub extern "system" fn Java_hero_com_heroandroid_MainActivity_core_1command(
    env: JNIEnv, _: JObject, j_command: JString
) -> jstring{
    unsafe {
        let command = CString::from(
            CStr::from_ptr(env.get_string(j_command).unwrap().as_ptr())
        );
        let output = env.new_string(String::from(command.to_str().unwrap()) + " " + &"Result".to_owned()).unwrap();
        output.into_inner()
    }

}

