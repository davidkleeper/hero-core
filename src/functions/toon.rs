use crate::enums::power_level::PowerLevel;
use crate::functions::archetype;
use crate::functions::build;
use crate::functions::power_level::convert_power_level;
use crate::functions::registry::get_archetype;
use crate::functions::registry::get_archetype_from_power;
use crate::poro::build::Build;
use crate::poro::power_entry::PowerEntry;
use crate::poro::toon::Toon;

pub fn validate(toon: &Toon) -> Result<bool, std::io::Error> {
    let build = &toon.builds[toon.current_build];
    if !build::validate(&build)? {
        return Err(std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            format!("Build does not pass validation."),
        ));
    }
    let archetype = match get_archetype(&toon.archetype_id)? {
        Some(a) => a,
        None => {
            return Err(std::io::Error::new(
                std::io::ErrorKind::NotFound,
                format!("Archetype {} not found.", toon.archetype_id),
            ))
        }
    };

    for power_level in build.power_entries.keys() {
        match build.power_entries.get(power_level) {
            Some(e) => match e {
                Some(p) => {
                    if !archetype::meets_power_requirements(&archetype, &p.power_id) {
                        return Err(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            format!(
                                "Archetype {} does not meet the requirements of power {}.",
                                toon.archetype_id, p.power_id
                            ),
                        ));
                    }
                    match get_archetype_from_power(&p.power_id)? {
                        Some(a) => {
                            if a.id != toon.archetype_id {
                                return Err(std::io::Error::new(
                                    std::io::ErrorKind::InvalidData,
                                    format!(
                                        "Invalid power {} for toon's archetype {}.",
                                        p.power_id, toon.archetype_id
                                    ),
                                ));
                            }
                        }
                        None => (),
                    };
                }
                None => (),
            },
            None => (),
        }
    }
    Ok(true)
}

pub fn set_power_entry(toon: &mut Toon, power_entry: &PowerEntry, power_level: &PowerLevel) -> Result<Toon, std::io::Error>{
    let build: &mut Build = &mut toon.builds[toon.current_build];
    let level = convert_power_level(&power_level);

    if level > toon.level
        {toon.level = level}
    if toon.level >= 2 {
        if !build::has_power(&build, "Inherent.Inherent.Sprint") {
            let mut sprint: PowerEntry = PowerEntry::new();
            sprint.level = 2;
            sprint.power_id = String::from("Inherent.Inherent.Sprint");
            build.power_entries.insert(PowerLevel::sprint, Some(sprint));
        }
        if !build::has_power(&build, "Inherent.Fitness.Hurdle") {
            let mut hurdle: PowerEntry = PowerEntry::new();
            hurdle.level = 2;
            hurdle.power_id = String::from("Inherent.Fitness.Hurdle");
            build.power_entries.insert(PowerLevel::hurdle, Some(hurdle));
        }
        if !build::has_power(&build, "Inherent.Fitness.Swift") {
            let mut swift: PowerEntry = PowerEntry::new();
            swift.level = 2;
            swift.power_id = String::from("Inherent.Fitness.Swift");
            build.power_entries.insert(PowerLevel::swift, Some(swift));
        }
    }
    
    let new_build = build::set_power_entry(build, power_entry, power_level)?;
    toon.builds[toon.current_build] = new_build;
    Ok(toon.clone())
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use crate::enums::build_power_sets::BuildPowerSets;
    use crate::enums::power_level::PowerLevel;
    use crate::poro::power_entry::PowerEntry;
    use crate::poro::toon::Toon;

    #[test]
    #[allow(unused_must_use)]
    fn test_validate() {
        let mut toon = Toon::new();
        let mut power_entry = PowerEntry::new();
        toon.archetype_id = String::from("Class_Peacebringer");

        power_entry.power_id = String::from("junk");
        toon.builds[toon.current_build]
            .power_entries
            .insert(PowerLevel::level_4, Some(power_entry.clone()));
        let mut _x = match super::validate(&toon) {
            Ok(_p) => {
                assert_eq!(0, 1);
            }
            Err(_e) => {
                assert_eq!(1, 1);
            }
        };

        power_entry.power_id = String::from("Pool.Speed.Hasten");
        toon.builds[toon.current_build]
            .power_entries
            .insert(PowerLevel::level_4, Some(power_entry.clone()));
        _x = match super::validate(&toon) {
            Ok(_p) => {
                assert_eq!(0, 1);
            }
            Err(_e) => {
                assert_eq!(1, 1);
            }
        };
        toon.builds[toon.current_build]
            .power_sets
            .insert(BuildPowerSets::pool_1, Some(String::from("Pool.Speed")));
        toon.builds[toon.current_build]
            .power_entries
            .insert(PowerLevel::level_4, Some(power_entry.clone()));
        _x = match super::validate(&toon) {
            Ok(_p) => {
                assert_eq!(1, 1);
            }
            Err(_e) => {
                assert_eq!(0, 1);
            }
        };

        power_entry.power_id = String::from("Pool.Speed.Whirlwind");
        toon.builds[toon.current_build]
            .power_entries
            .insert(PowerLevel::level_20, Some(power_entry.clone()));
        _x = match super::validate(&toon) {
            Ok(_p) => {
                assert_eq!(0, 1);
            }
            Err(_e) => {
                assert_eq!(1, 1);
            }
        };
        power_entry.power_id = String::from("Pool.Speed.Super_Speed");
        toon.builds[toon.current_build]
            .power_entries
            .insert(PowerLevel::level_14, Some(power_entry.clone()));
        power_entry.power_id = String::from("Pool.Speed.Whirlwind");
        toon.builds[toon.current_build]
            .power_entries
            .insert(PowerLevel::level_20, Some(power_entry.clone()));
        _x = match super::validate(&toon) {
            Ok(_p) => {
                assert_eq!(1, 1);
            }
            Err(_e) => {
                assert_eq!(0, 1);
            }
        };

        power_entry.power_id = String::from("Pool.Teleportation.Recall_Friend");
        toon.builds[toon.current_build].power_sets.insert(
            BuildPowerSets::pool_2,
            Some(String::from("Pool.Teleportation")),
        );
        toon.builds[toon.current_build]
            .power_entries
            .insert(PowerLevel::level_6, Some(power_entry.clone()));
        _x = match super::validate(&toon) {
            Ok(_p) => {
                assert_eq!(0, 1);
            }
            Err(_e) => {
                assert_eq!(1, 1);
            }
        };

        toon.archetype_id = String::from("Class_Blaster");
        _x = match super::validate(&toon) {
            Ok(_p) => {
                assert_eq!(1, 1);
            }
            Err(_e) => {
                assert_eq!(0, 1);
            }
        };

        power_entry.power_id = String::from("Pool.Speed.Whirlwind");
        toon.builds[toon.current_build]
            .power_entries
            .insert(PowerLevel::level_22, Some(power_entry.clone()));
        _x = match super::validate(&toon) {
            Ok(_p) => {
                assert_eq!(0, 1);
            }
            Err(_e) => {
                assert_eq!(1, 1);
            }
        };
    }
}
