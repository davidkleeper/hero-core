use std::collections::HashMap;
use std::sync::Mutex;

use crate::enums::class_type::ClassType;
use crate::enums::power_set_type::PowerSetType;
use crate::poro::archetype::Archetype;
use crate::poro::enhancement::Enhancement;
use crate::poro::enhancement_set::EnhancementSet;
use crate::poro::new_power::NewPower;
use crate::poro::power::Power;
use crate::poro::power_set::PowerSet;
use crate::scatterbase::functions::cross_platform::path;
use crate::scatterbase::functions::registry::get_index;
use crate::scatterbase::functions::registry::get_value;
use crate::scatterbase::functions::registry::register_from_disk_to_disk;
use crate::scatterbase::functions::registry::register_from_disk_to_memory;
use crate::scatterbase::poro::registry::Registry;

lazy_static! {
    static ref GLOBAL_REGISTRY: Registry = build_registry().unwrap();
    static ref POWER_CACHE: Mutex<HashMap<String, NewPower>> =
        Mutex::new(HashMap::<String, NewPower>::new());
}

pub fn build_registry() -> Result<Registry, std::io::Error> {
    let mut clone = register_from_disk_to_memory(
        &Registry::new(),
        "archetypes",
        &path("./database/$PLATFORM$/player_archetypes.json"),
        &path("./database/$PLATFORM$/player_archetypes.index"),
    )?;
    clone = register_from_disk_to_memory(
        &clone,
        "enhancement-sets",
        &path("./database/$PLATFORM$/enhancement-sets.json"),
        &path("./database/$PLATFORM$/enhancement-sets.index"),
    )?;
    clone = register_from_disk_to_memory(
        &clone,
        "enhancements",
        &path("./database/$PLATFORM$/enhancements.json"),
        &path("./database/$PLATFORM$/enhancements.index"),
    )?;
    clone = register_from_disk_to_memory(
        &clone,
        "power-sets",
        &path("./database/$PLATFORM$/player_power_sets.json"),
        &path("./database/$PLATFORM$/player_power_sets.index"),
    )?;
    clone = register_from_disk_to_memory(
        &clone,
        "powers",
        &path("./database/$PLATFORM$/player_powers.json"),
        &path("./database/$PLATFORM$/player_powers.index"),
    )?;
    clone = register_from_disk_to_memory(
        &clone,
        "powers",
        &path("./database/$PLATFORM$/player_effects.json"),
        &path("./database/$PLATFORM$/player_effects.index"),
    )?;
    clone = register_from_disk_to_memory(
        &clone,
        "old-power-sets",
        &path("./database/$PLATFORM$/power-sets.json"),
        &path("./database/$PLATFORM$/power-sets.index"),
    )?;
    clone = register_from_disk_to_disk(
        &clone,
        "old-powers",
        &path("./database/$PLATFORM$/powers.json"),
        &path("./database/$PLATFORM$/powers.index"),
    )?;
    Ok(clone)
}

pub fn get_ancillary_power_sets_from_archetype(
    archetype_id: &str,
) -> Result<Option<Vec<PowerSet>>, std::io::Error> {
    match get_value(get_registry(), "archetypes", archetype_id)? {
        Some(a) => {
            let mut power_sets = Vec::<PowerSet>::new();
            let archetype: Archetype = serde_json::from_str(a.as_str())?;
            for power_set_id in archetype.powersets_ancillary.iter() {
                match get_power_set(power_set_id)? {
                    Some(ps) => {
                        power_sets.push(ps);
                        ()
                    }
                    None => (),
                }
            }
            return Ok(Some(power_sets));
        }
        None => return Ok(None),
    }
}

pub fn get_archetype(archetype_id: &str) -> Result<Option<Archetype>, std::io::Error> {
    match get_value(get_registry(), "archetypes", archetype_id)? {
        Some(v) => {
            let archetype: Archetype = serde_json::from_str(v.as_str())?;
            return Ok(Some(archetype));
        }
        None => return Ok(None),
    }
}

pub fn get_archetypes(player_only: bool) -> Result<Vec<Archetype>, std::io::Error> {
    match get_index(get_registry(), "archetypes")? {
        Some(i) => {
            let mut archetypes = Vec::<Archetype>::new();
            for (archetype_id, _start, _end) in i.index.iter() {
                match get_archetype(&archetype_id)? {
                    Some(a) => {
                        if player_only {
                            if a.class_type != ClassType::Hero
                            && a.class_type != ClassType::HeroEpic
                            && a.class_type != ClassType::Villain
                            && a.class_type != ClassType::VillainEpic
                            {
                                continue;
                            }
                        }
                        archetypes.push(a);
                    }
                    None => continue,
                }
            }
            return Ok(archetypes);
        }
        None => return Ok(Vec::<Archetype>::new()),
    }
}

pub fn get_archetype_from_power(power_id: &str) -> Result<Option<Archetype>, std::io::Error> {
    let power = match get_power(power_id)? {
        Some(p) => p,
        None => return Ok(None),
    };
    get_archetype_from_power_set(power.power_set_id.as_str())
}

pub fn get_archetype_from_power_set(
    power_set_id: &str,
) -> Result<Option<Archetype>, std::io::Error> {
    let power_set = match get_power_set(power_set_id)? {
        Some(p) => p,
        None => return Ok(None),
    };
    get_archetype(power_set.archetype_id.as_str())
}

pub fn get_archetype_ids() -> Result<Option<Vec<String>>, std::io::Error> {
    get_ids("archetypes")
}

pub fn get_enhancement(enhancement_id: &str) -> Result<Option<Enhancement>, std::io::Error> {
    match get_value(get_registry(), "enhancements", enhancement_id)? {
        Some(v) => {
            let enhancement: Enhancement = serde_json::from_str(v.as_str())?;
            return Ok(Some(enhancement));
        }
        None => return Ok(None),
    }
}

pub fn get_enhancements_from_enhancement_set(
    enhancement_set_id: &str,
) -> Result<Option<Vec<Enhancement>>, std::io::Error> {
    match get_value(get_registry(), "enhancement-sets", enhancement_set_id)? {
        Some(es) => {
            let mut enhancements = Vec::<Enhancement>::new();
            let enhancement_set: EnhancementSet = serde_json::from_str(es.as_str())?;
            match enhancement_set.enhancement_ids {
                Some(v) => {
                    for enhancement_id in v.iter() {
                        match get_enhancement(enhancement_id)? {
                            Some(e) => {
                                enhancements.push(e);
                                ()
                            }
                            None => (),
                        }
                    }
                }
                None => return Ok(None),
            }
            return Ok(Some(enhancements));
        }
        None => return Ok(None),
    }
}

pub fn get_enhancement_ids() -> Result<Option<Vec<String>>, std::io::Error> {
    get_ids("enhancements")
}

pub fn get_enhancement_set(
    enhancement_set_id: &str,
) -> Result<Option<EnhancementSet>, std::io::Error> {
    match get_value(get_registry(), "enhancement-sets", enhancement_set_id)? {
        Some(v) => {
            let enhancement_set: EnhancementSet = serde_json::from_str(v.as_str())?;
            return Ok(Some(enhancement_set));
        }
        None => return Ok(None),
    }
}

pub fn get_enhancement_set_ids() -> Result<Option<Vec<String>>, std::io::Error> {
    get_ids("enhancement-sets")
}

fn get_ids(index_key: &str) -> Result<Option<Vec<String>>, std::io::Error> {
    let index = match get_index(get_registry(), index_key)? {
        Some(i) => i,
        None => return Ok(None),
    };
    let mut keys = Vec::<String>::new();
    for entry in index.index.iter() {
        keys.push(entry.0.clone());
    }
    Ok(Some(keys))
}

pub fn get_old_power(power_id: &str) -> Result<Option<Power>, std::io::Error> {
    match get_value(get_registry(), "old-powers", power_id)? {
        Some(v) => {
            let power: Power = serde_json::from_str(v.as_str())?;
            return Ok(Some(power));
        }
        None => return Ok(None),
    };
}

pub fn get_old_power_set(power_set_id: &str) -> Result<Option<PowerSet>, std::io::Error> {
    match get_value(get_registry(), "old-power-sets", power_set_id)? {
        Some(v) => {
            let power_set: PowerSet = serde_json::from_str(v.as_str())?;
            return Ok(Some(power_set));
        }
        None => return Ok(None),
    }
}

pub fn get_old_powers_from_power_set(power_set_id: &str) -> Result<Option<Vec<Power>>, std::io::Error> {
    match get_value(get_registry(), "old-power-sets", power_set_id)? {
        Some(ps) => {
            let mut powers = Vec::<Power>::new();
            let power_set: PowerSet = serde_json::from_str(ps.as_str())?;
            for power_id in power_set.power_ids.iter() {
                match get_old_power(power_id)? {
                    Some(p) => {
                        powers.push(p);
                        ()
                    }
                    None => (),
                }
            }
            return Ok(Some(powers));
        }
        None => return Ok(None),
    }
}

pub fn get_pool_power_sets_from_archetype(
    archetype_id: &str,
) -> Result<Option<Vec<PowerSet>>, std::io::Error> {
    match get_index(get_registry(), "power-sets")? {
        Some(i) => {
            let mut power_sets = Vec::<PowerSet>::new();
            for (power_set_id, _start, _end) in i.index.iter() {
                if !power_set_id.starts_with("Pool.") {
                    continue;
                }
                if "Pool.Leadership_beta" == power_set_id {
                    continue;
                }
                if "Class_Warshade" == archetype_id || "Class_Peacebringer" == archetype_id {
                    if "Pool.Flight" == power_set_id || "Pool.Teleportation" == power_set_id {
                        continue;
                    }
                }
                match get_power_set(power_set_id)? {
                    Some(ps) => {
                        if ps.set_type == PowerSetType::Pool {
                            power_sets.push(ps);
                        }
                        ()
                    }
                    None => (),
                }
            }
            return Ok(Some(power_sets));
        }
        None => return Ok(None),
    }
}

pub fn get_power(power_id: &str) -> Result<Option<NewPower>, std::io::Error> {
    let cache = get_power_cache().lock().unwrap();
    match cache.get(power_id) {
        Some(a) => return Ok(Some(a.clone())),
        None => match get_value(get_registry(), "powers", power_id)? {
            Some(v) => {
                let power: NewPower = serde_json::from_str(v.as_str())?;
                return Ok(Some(power));
            }
            None => return Ok(None),
        },
    }
}

pub fn get_power_ids() -> Result<Option<Vec<String>>, std::io::Error> {
    get_ids("powers")
}

pub fn get_powers_from_power_set(power_set_id: &str) -> Result<Option<Vec<NewPower>>, std::io::Error> {
    match get_value(get_registry(), "power-sets", power_set_id)? {
        Some(ps) => {
            let mut powers = Vec::<NewPower>::new();
            let power_set: PowerSet = serde_json::from_str(ps.as_str())?;
            for power_id in power_set.power_ids.iter() {
                match get_power(power_id)? {
                    Some(p) => {
                        powers.push(p);
                        ()
                    }
                    None => (),
                }
            }
            return Ok(Some(powers));
        }
        None => return Ok(None),
    }
}

fn get_power_cache() -> &'static Mutex<HashMap<String, NewPower>> {
    &POWER_CACHE
}

pub fn get_power_set(power_set_id: &str) -> Result<Option<PowerSet>, std::io::Error> {
    match get_value(get_registry(), "power-sets", power_set_id)? {
        Some(v) => {
            let power_set: PowerSet = serde_json::from_str(v.as_str())?;
            return Ok(Some(power_set));
        }
        None => return Ok(None),
    }
}

pub fn get_power_set_from_power(power_id: &str) -> Result<Option<PowerSet>, std::io::Error> {
    let power = match get_power(power_id)? {
        Some(p) => p,
        None => return Ok(None),
    };
    get_power_set(power.power_set_id.as_str())
}

pub fn get_power_set_ids() -> Result<Option<Vec<String>>, std::io::Error> {
    get_ids("power-sets")
}

pub fn get_primary_power_sets_from_archetype(
    archetype_id: &str,
) -> Result<Option<Vec<PowerSet>>, std::io::Error> {
    match get_value(get_registry(), "archetypes", archetype_id)? {
        Some(a) => {
            let mut power_sets = Vec::<PowerSet>::new();
            let archetype: Archetype = serde_json::from_str(a.as_str())?;
            for power_set_id in archetype.powersets_primary.iter() {
                match get_power_set(power_set_id)? {
                    Some(ps) => {
                        power_sets.push(ps);
                        ()
                    }
                    None => (),
                }
            }
            return Ok(Some(power_sets));
        }
        None => return Ok(None),
    }
}

pub fn get_registry() -> &'static Registry {
    &GLOBAL_REGISTRY
}

pub fn get_secondary_power_sets_from_archetype(
    archetype_id: &str,
) -> Result<Option<Vec<PowerSet>>, std::io::Error> {
    match get_value(get_registry(), "archetypes", archetype_id)? {
        Some(a) => {
            let mut power_sets = Vec::<PowerSet>::new();
            let archetype: Archetype = serde_json::from_str(a.as_str())?;
            for power_set_id in archetype.powersets_secondary.iter() {
                match get_power_set(power_set_id)? {
                    Some(ps) => {
                        power_sets.push(ps);
                        ()
                    }
                    None => (),
                }
            }
            return Ok(Some(power_sets));
        }
        None => return Ok(None),
    }
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use crate::poro::archetype::Archetype;
    use crate::poro::enhancement::Enhancement;
    use crate::poro::enhancement_set::EnhancementSet;
    use crate::poro::new_power::NewPower;
    use crate::poro::power_set::PowerSet;
    use crate::scatterbase::functions::registry::has_disk_key;
    use crate::scatterbase::functions::registry::has_memory_key;

    #[test]
    #[allow(unused_must_use)]
    fn test_build_registry() {
        let registry = super::build_registry().unwrap();
        assert_eq!(has_memory_key(&registry, "archetypes"), true);
        assert_eq!(has_memory_key(&registry, "enhancement-sets"), true);
        assert_eq!(has_memory_key(&registry, "enhancements"), true);
        assert_eq!(has_memory_key(&registry, "power-sets"), true);
        assert_eq!(has_memory_key(&registry, "powers"), true);
        assert_eq!(has_disk_key(&registry, "old-powers"), true);
        assert_eq!(has_memory_key(&registry, "junk"), false);
        assert_eq!(has_disk_key(&registry, "junk"), false);
    }

    #[test]
    fn test_get_archetype() {
        let blaster: Archetype = super::get_archetype("Class_Blaster").unwrap().unwrap();
        assert_eq!(blaster.id, "Class_Blaster");

        assert_eq!(super::get_archetype("junk").unwrap(), None);
    }

    #[test]
    fn test_get_archetypes() {
        let mut archetypes: Vec<Archetype> = super::get_archetypes(false).unwrap();
        assert_eq!(archetypes.len(), 15);

        archetypes = super::get_archetypes(true).unwrap();
        assert_eq!(archetypes.len(), 15);
    }

    #[test]
    fn test_get_archetype_ids() {
        let ids = super::get_archetype_ids().unwrap().unwrap();
        let mut iter = ids.iter();
        assert_eq!(iter.next().unwrap(), "Class_Arachnos_Soldier");
    }

    #[test]
    fn test_get_enhancement_set() {
        let absolute_amazement: EnhancementSet = super::get_enhancement_set("Absolute_Amazement")
            .unwrap()
            .unwrap();
        assert_eq!(absolute_amazement.id, "Absolute_Amazement");

        assert_eq!(super::get_enhancement_set("junk").unwrap(), None);
    }

    #[test]
    fn test_get_enhancement_set_ids() {
        let ids = super::get_enhancement_set_ids().unwrap().unwrap();
        let mut iter = ids.iter();
        assert_eq!(iter.next().unwrap(), "Absolute_Amazement");
    }

    #[test]
    fn test_get_enhancements_from_enhancement_set() {
        let enhancements = super::get_enhancements_from_enhancement_set("Absolute_Amazement")
            .unwrap()
            .unwrap();
        assert_eq!(enhancements.len(), 6);
        assert_eq!(enhancements[0].id, "Crafted_Absolute_Amazement_A");
        assert_eq!(enhancements[1].id, "Crafted_Absolute_Amazement_B");
        assert_eq!(enhancements[2].id, "Crafted_Absolute_Amazement_C");
        assert_eq!(enhancements[3].id, "Crafted_Absolute_Amazement_D");
        assert_eq!(enhancements[4].id, "Crafted_Absolute_Amazement_E");
        assert_eq!(enhancements[5].id, "Crafted_Absolute_Amazement_F");
    }

    #[test]
    fn test_get_enhancement() {
        let ascendency: Enhancement =
            super::get_enhancement("Attuned_Ascendency_of_the_Dominator_A")
                .unwrap()
                .unwrap();
        assert_eq!(ascendency.id, "Attuned_Ascendency_of_the_Dominator_A");
        assert_eq!(super::get_enhancement("junk").unwrap(), None);
    }

    #[test]
    fn test_get_enhancement_ids() {
        let ids = super::get_enhancement_ids().unwrap().unwrap();
        let mut iter = ids.iter();
        assert_eq!(
            iter.next().unwrap(),
            "Attuned_Ascendency_of_the_Dominator_A"
        );
    }

    #[test]
    fn test_get_power_set() {
        let arachnos_soldiers: PowerSet =
            super::get_power_set("Arachnos_Soldiers.Arachnos_Soldier")
                .unwrap()
                .unwrap();
        assert_eq!(arachnos_soldiers.id, "Arachnos_Soldiers.Arachnos_Soldier");

        assert_eq!(super::get_power_set("junk").unwrap(), None);
    }

    #[test]
    fn test_get_power_set_ids() {
        let ids = super::get_power_set_ids().unwrap().unwrap();
        let mut iter = ids.iter();
        assert_eq!(iter.next().unwrap(), "Arachnos_Soldiers.Arachnos_Soldier");
    }

    #[test]
    fn test_get_primary_power_sets_from_archetype() {
        let power_sets = super::get_primary_power_sets_from_archetype("Class_Arachnos_Soldier")
            .unwrap()
            .unwrap();
        assert_eq!(power_sets.len(), 3);
        assert_eq!(power_sets[0].id, "Arachnos_Soldiers.Arachnos_Soldier");
        assert_eq!(power_sets[1].id, "Arachnos_Soldiers.Bane_Spider_Soldier");
        assert_eq!(power_sets[2].id, "Arachnos_Soldiers.Crab_Spider_Soldier");
    }

    #[test]
    fn test_get_secondary_power_sets_from_archetype() {
        let power_sets = super::get_secondary_power_sets_from_archetype("Class_Arachnos_Soldier")
            .unwrap()
            .unwrap();

        assert_eq!(power_sets.len(), 3);
        assert_eq!(power_sets[0].id, "Training_Gadgets.Bane_Spider_Training");
        assert_eq!(power_sets[1].id, "Training_Gadgets.Crab_Spider_Training");
        assert_eq!(power_sets[2].id, "Training_Gadgets.Training_and_Gadgets");
    }

    #[test]
    fn test_get_ancillary_power_sets_from_archetype() {
        let power_sets = super::get_ancillary_power_sets_from_archetype("Class_Blaster")
            .unwrap()
            .unwrap();

        assert_eq!(power_sets.len(), 9);
        assert_eq!(power_sets[0].id, "Epic.Cold_Mastery");
        assert_eq!(power_sets[1].id, "Epic.Electrical_Mastery");
        assert_eq!(power_sets[2].id, "Epic.Flame_Mastery");
        assert_eq!(power_sets[3].id, "Epic.Force_Mastery");
        assert_eq!(power_sets[4].id, "Epic.Mastermind_Leviathan_Mastery");
        assert_eq!(power_sets[5].id, "Epic.Blaster_Mace_Mastery");
        assert_eq!(power_sets[6].id, "Epic.Blaster_Mu_Mastery");
        assert_eq!(power_sets[7].id, "Epic.Munitions_Mastery");
        assert_eq!(power_sets[8].id, "Epic.Mastermind_Soul_Mastery");
    }

    #[test]
    fn test_get_pool_power_sets_from_archetype() {
        let mut power_sets = super::get_pool_power_sets_from_archetype("Class_Blaster")
            .unwrap()
            .unwrap();

        assert_eq!(power_sets.len(), 12);
        assert_eq!(power_sets[0].id, "Pool.Experimentation");
        assert_eq!(power_sets[1].id, "Pool.Fighting");
        assert_eq!(power_sets[2].id, "Pool.Flight");
        assert_eq!(power_sets[3].id, "Pool.Force_of_Will");
        assert_eq!(power_sets[4].id, "Pool.Invisibility");
        assert_eq!(power_sets[5].id, "Pool.Leadership");
        assert_eq!(power_sets[6].id, "Pool.Leaping");
        assert_eq!(power_sets[7].id, "Pool.Manipulation");
        assert_eq!(power_sets[8].id, "Pool.Medicine");
        assert_eq!(power_sets[9].id, "Pool.Sorcery");
        assert_eq!(power_sets[10].id, "Pool.Speed");
        assert_eq!(power_sets[11].id, "Pool.Teleportation");

        power_sets = super::get_pool_power_sets_from_archetype("Class_Warshade")
            .unwrap()
            .unwrap();

        assert_eq!(power_sets.len(), 10);
        assert_eq!(power_sets[0].id, "Pool.Experimentation");
        assert_eq!(power_sets[1].id, "Pool.Fighting");
        assert_eq!(power_sets[2].id, "Pool.Force_of_Will");
        assert_eq!(power_sets[3].id, "Pool.Invisibility");
        assert_eq!(power_sets[4].id, "Pool.Leadership");
        assert_eq!(power_sets[5].id, "Pool.Leaping");
        assert_eq!(power_sets[6].id, "Pool.Manipulation");
        assert_eq!(power_sets[7].id, "Pool.Medicine");
        assert_eq!(power_sets[8].id, "Pool.Sorcery");
        assert_eq!(power_sets[9].id, "Pool.Speed");
    }

    #[test]
    fn test_get_archetype_from_power_set() {
        let arachnos_soldiers: PowerSet =
            super::get_power_set("Arachnos_Soldiers.Arachnos_Soldier")
                .unwrap()
                .unwrap();
        let archetype = super::get_archetype_from_power_set(arachnos_soldiers.id.as_str())
            .unwrap()
            .unwrap();
        assert_eq!(archetype.id, "Class_Arachnos_Soldier");

        let speed: PowerSet = super::get_power_set("Pool.Speed").unwrap().unwrap();
        let archetype2 = super::get_archetype_from_power_set(speed.id.as_str()).unwrap();
        assert_eq!(archetype2, None);
    }

    #[test]
    fn test_get_power() {
        let bayonet: NewPower = super::get_power("Arachnos_Soldiers.Arachnos_Soldier.Bayonet")
            .unwrap()
            .unwrap();
        assert_eq!(bayonet.id, "Arachnos_Soldiers.Arachnos_Soldier.Bayonet");
        assert_eq!(super::get_power("junk").unwrap(), None);
    }

    #[test]
    fn test_get_power_ids() {
        let ids = super::get_power_ids().unwrap().unwrap();
        let mut iter = ids.iter();
        assert_eq!(
            iter.next().unwrap(),
            "Arachnos_Soldiers.Arachnos_Soldier.Bayonet"
        );
    }

    #[test]
    fn test_get_powers_from_power_set() {
        let powers = super::get_powers_from_power_set("Arachnos_Soldiers.Arachnos_Soldier")
            .unwrap()
            .unwrap();
        assert_eq!(powers.len(), 8);
        assert_eq!(
            powers[0].id,
            "Arachnos_Soldiers.Arachnos_Soldier.Single_Shot"
        );
        assert_eq!(powers[1].id, "Arachnos_Soldiers.Arachnos_Soldier.Pummel");
        assert_eq!(powers[2].id, "Arachnos_Soldiers.Arachnos_Soldier.Burst");
        assert_eq!(
            powers[3].id,
            "Arachnos_Soldiers.Arachnos_Soldier.WS_Wide_Area_Web_Grenade"
        );
        assert_eq!(
            powers[4].id,
            "Arachnos_Soldiers.Arachnos_Soldier.Heavy_Burst"
        );
        assert_eq!(powers[5].id, "Arachnos_Soldiers.Arachnos_Soldier.Bayonet");
        assert_eq!(
            powers[6].id,
            "Arachnos_Soldiers.Arachnos_Soldier.Venom_Grenade"
        );
        assert_eq!(
            powers[7].id,
            "Arachnos_Soldiers.Arachnos_Soldier.Frag_Grenade"
        );
    }

    #[test]
    fn test_get_power_set_from_power() {
        let bayonet: NewPower = super::get_power("Arachnos_Soldiers.Arachnos_Soldier.Bayonet")
            .unwrap()
            .unwrap();
        let power_set = super::get_power_set_from_power(bayonet.id.as_str())
            .unwrap()
            .unwrap();
        assert_eq!(power_set.id, "Arachnos_Soldiers.Arachnos_Soldier");
    }

    #[test]
    fn test_get_archetype_from_power() {
        let bayonet: NewPower = super::get_power("Arachnos_Soldiers.Arachnos_Soldier.Bayonet")
            .unwrap()
            .unwrap();
        let archetype = super::get_archetype_from_power(bayonet.id.as_str())
            .unwrap()
            .unwrap();
        assert_eq!(archetype.id, "Class_Arachnos_Soldier");

        let hasten: NewPower = super::get_power("Pool.Speed.Hasten").unwrap().unwrap();
        let archetype2 = super::get_archetype_from_power(hasten.id.as_str()).unwrap();
        assert_eq!(archetype2, None);
    }
}
