use crate::poro::power_entry::PowerEntry;
use crate::poro::slot::Slot;

pub fn add_empty_slot(power_entry: &mut PowerEntry) -> bool {
    if power_entry.slots.len() == 6 
        { return false; }
    let slot = Slot::new();
    power_entry.slots.push(slot);
    true
}


// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use crate::enums::enhancement_grade::EnhancementGrade;
    use crate::enums::enhancement_relative_level::EnhancementRelativeLevel;
        use crate::poro::power_entry::PowerEntry;

    #[test]
    #[allow(unused_must_use)]
    fn test_add_empty_slot() {
        let mut brawl: PowerEntry = PowerEntry::new();
        brawl.level = 1;
        brawl.power_id = String::from("Inherent.Inherent.Brawl");
        assert_eq!(brawl.slots.len(), 0);
        super::add_empty_slot(&mut brawl);
        assert_eq!(brawl.slots.len(), 1);
        assert_eq!(brawl.slots[0].enhancement_id, String::from(""));
        assert_eq!(brawl.slots[0].relative_level, EnhancementRelativeLevel::None);
        assert_eq!(brawl.slots[0].grade, EnhancementGrade::None);
        assert_eq!(brawl.slots[0].level, 0);
        assert_eq!(brawl.slots[0].enhancement_level, 0);
    }
} 
