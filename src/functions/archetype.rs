use crate::functions::registry::get_power_set_from_power;
use crate::poro::archetype::Archetype;

pub fn meets_power_requirements(archetype: &Archetype, power_id: &str) -> bool {
    if archetype.id != "Class_Warshade" && archetype.id != "Class_Peacebringer" {
        return true;
    }
    let power_set = match get_power_set_from_power(power_id) {
        Ok(o) => match o {
            Some(p) => p,
            None => return false,
        },
        Err(_e) => return false,
    };
    power_set.id != "Pool.Flight" && power_set.id != "Pool.Teleportation"
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use crate::functions::registry::get_archetype;
    use crate::poro::power_entry::PowerEntry;
    use crate::poro::toon::Toon;

    #[test]
    #[allow(unused_must_use)]
    fn test_meets_power_requirements() {
        let mut toon = Toon::new();
        let mut power_entry = PowerEntry::new();
        toon.archetype_id = String::from("Class_Peacebringer");
        let mut archetype = match get_archetype(&toon.archetype_id).unwrap() {
            Some(a) => a,
            None => panic!(),
        };

        power_entry.power_id = String::from("Pool.Teleportation.Recall_Friend");
        let mut x = super::meets_power_requirements(&archetype, &power_entry.power_id);
        assert_eq!(x, false);
        power_entry.power_id = String::from("Pool.Flight.Fly");
        x = super::meets_power_requirements(&archetype, &power_entry.power_id);
        assert_eq!(x, false);
        power_entry.power_id = String::from("Pool.Speed.Hasten");
        x = super::meets_power_requirements(&archetype, &power_entry.power_id);
        assert_eq!(x, true);

        toon.archetype_id = String::from("Class_Warshade");
        archetype = match get_archetype(&toon.archetype_id).unwrap() {
            Some(a) => a,
            None => panic!(),
        };

        power_entry.power_id = String::from("Pool.Teleportation.Recall_Friend");
        x = super::meets_power_requirements(&archetype, &power_entry.power_id);
        assert_eq!(x, false);
        power_entry.power_id = String::from("Pool.Flight.Fly");
        x = super::meets_power_requirements(&archetype, &power_entry.power_id);
        assert_eq!(x, false);
        power_entry.power_id = String::from("Pool.Speed.Hasten");
        x = super::meets_power_requirements(&archetype, &power_entry.power_id);
        assert_eq!(x, true);
    }
}
