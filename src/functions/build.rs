use crate::enums::build_power_sets::BuildPowerSets;
use crate::enums::power_level::PowerLevel;
use crate::enums::power_set_type::PowerSetType;
use crate::functions::power_level::convert_power_level;
use crate::functions::registry::get_power;
use crate::functions::registry::get_power_set;
use crate::functions::registry::get_power_set_from_power;
use crate::poro::build::Build;
use crate::poro::new_power::NewPower;
use crate::poro::power_entry::PowerEntry;
use crate::poro::power_set::PowerSet;

pub fn has_power(build: &Build, power_id: &str) -> bool {
    for (_power_level, power_entry) in build.power_entries.iter() {
        match &power_entry {
            Some(p) => {
                if p.power_id == power_id {
                    return true;
                }
            }
            None => (),
        };
    }
    false
}

pub fn has_all_powers(build: &Build, power_ids: &Vec::<String>) -> bool {
    for power_id in power_ids {
        if !has_power(build, &power_id) 
            { return false }
    }
    true
}

pub fn has_power_set(build: &Build, power_set_id: &str) -> bool {
    for (_build_power_set_type, power_set) in build.power_sets.iter() {
        match &power_set {
            Some(i) => {
                if i == power_set_id {
                    return true;
                }
            }
            None => (),
        };
    }
    false
}

pub fn validate(build: &Build) -> Result<bool, std::io::Error> {
    if !validate_power_sets(build)? {
        return Err(std::io::Error::new(
            std::io::ErrorKind::NotFound,
            format!("Power sets did not pass validation."),
        ));
    }
    if !validate_power_entries(build)? {
        return Err(std::io::Error::new(
            std::io::ErrorKind::NotFound,
            format!("Power entries did not pass validation."),
        ));
    }
    Ok(true)
}

pub fn validate_power_sets(build: &Build) -> Result<bool, std::io::Error> {
    for (_power_set_type, power_set) in build.power_sets.iter() {
        if None == *power_set {
            continue;
        }
        match power_set {
            Some(power_set_id) => match get_power_set(power_set_id)? {
                Some(_p) => (),
                None => {
                    return Err(std::io::Error::new(
                        std::io::ErrorKind::NotFound,
                        format!("Power set {} not found.", power_set_id),
                    ));
                }
            },
            None => (),
        }
    }
    Ok(true)
}

pub fn validate_power_entries(build: &Build) -> Result<bool, std::io::Error> {
    let mut entries = Vec::<&Option<PowerEntry>>::new();

    for (power_level, optional_power_entry) in build.power_entries.iter() {
        if None != *optional_power_entry {
            if entries.contains(&optional_power_entry) {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::InvalidInput,
                    format!(
                        "Power entry {:?} appears in the build more than once.",
                        optional_power_entry
                    ),
                ));
            }
            entries.push(&optional_power_entry);
        }
        match build.power_entries.get(power_level).unwrap() {
            Some(p) => {
                if !validate_power_entry(build, &p, &power_level)? {
                    return Err(std::io::Error::new(
                        std::io::ErrorKind::InvalidInput,
                        format!("Power entry {} did not pass validation.", &p.power_id),
                    ));
                }
            }
            None => (),
        }
    }
    Ok(true)
}

pub fn validate_power_entry(
    build: &Build,
    power_entry: &PowerEntry,
    power_level: &PowerLevel,
) -> Result<bool, std::io::Error> {
    let power = match get_power(&power_entry.power_id)? {
        Some(p) => p,
        None => {
            return Err(std::io::Error::new(
                std::io::ErrorKind::NotFound,
                format!("Power {} not found.", power_entry.power_id),
            ))
        }
    };

    if !has_power_set(build, &power.power_set_id) {
        return Err(std::io::Error::new(
            std::io::ErrorKind::NotFound,
            format!(
                "Build does not have power set {} required by power {}.",
                power.power_set_id, power.id
            ),
        ));
    }

    let level = convert_power_level(power_level);
    if level < power.level {
        return Err(std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            format!(
                "Power {} is too high level. Requires {} or less, but found {}.",
                power_entry.power_id, level, power.level
            ),
        ));
    }

    if !meets_power_requirements(build, &power) {
        return Err(std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            format!(
                "Build does not meet the requirements of power {}.",
                power_entry.power_id
            ),
        ));
    }
    Ok(true)
}

pub fn meets_power_requirements(build: &Build, power: &NewPower) -> bool {
    let mut meets = true;
    for or_list in power.requires.power_id.iter() {
        let mut and_meets = true;
        for and_power_id in or_list {
            if "" == and_power_id {
                continue;
            }
            if !has_power(build, and_power_id) {
                and_meets = false;
                break;
            }
        }
        meets = and_meets;
        if meets {
            break;
        }
    }
    meets
}

pub fn get_power_set_from_build(build: &Build, power_set: &BuildPowerSets) -> String{
    let power_set = match build.power_sets.get(power_set) {
        Some(o) => match o {
            Some(ps) => ps,
            None => return String::from("")
        },
        None => return String::from("")
    };
    
    String::from(power_set)
}

pub fn get_primary(build: &Build) -> String{
    get_power_set_from_build(&build, &BuildPowerSets::primary)
}

pub fn get_secondary(build: &Build) -> String{
    get_power_set_from_build(&build, &BuildPowerSets::secondary)
}

pub fn get_epic(build: &Build) -> String{
    get_power_set_from_build(&build, &BuildPowerSets::epic)
}

pub fn get_pool_1(build: &Build) -> String{
    get_power_set_from_build(&build, &BuildPowerSets::pool_1)
}

pub fn get_pool_2(build: &Build) -> String{
    get_power_set_from_build(&build, &BuildPowerSets::pool_2)
}

pub fn get_pool_3(build: &Build) -> String{
    get_power_set_from_build(&build, &BuildPowerSets::pool_3)
}

pub fn get_pool_4(build: &Build) -> String{
    get_power_set_from_build(&build, &BuildPowerSets::pool_4)
}

pub fn get_inherent(build: &Build) -> String{
    get_power_set_from_build(&build, &BuildPowerSets::inherent)
}

pub fn get_fitness(build: &Build) -> String{
    get_power_set_from_build(&build, &BuildPowerSets::fitness)
}

pub fn has_pool(build: &Build, power_set_id: &String) -> bool {
    let build_pool_1 = get_pool_1(&build);
    let build_pool_2 = get_pool_2(&build);
    let build_pool_3 = get_pool_3(&build);
    let build_pool_4 = get_pool_4(&build);
    let has_pool = &build_pool_1 == power_set_id || &build_pool_2 == power_set_id || &build_pool_3 == power_set_id || &build_pool_4 == power_set_id;
    has_pool
}

pub fn has_free_pool(build: &Build) -> bool {
    let has_free_pool = has_pool(&build, &String::from(""));
    has_free_pool
}

pub fn can_set_power_entry(build: &Build, power_entry: &PowerEntry) -> bool {
    let power_set: PowerSet = match get_power_set_from_power(&power_entry.power_id) {
        Ok(s) => match s {
            Some(ps) => ps,
            None => {
                return false
            }
        },
        Err(_e) => return false
    };
    if power_set.set_type == PowerSetType::Primary {
        let build_primary = get_primary(&build);
        if build_primary != power_set.id {
            return false
        }
    } else if power_set.set_type == PowerSetType::Secondary {
        let build_secondary = get_secondary(&build);
        if build_secondary != power_set.id {
            return false
        }
    } else if power_set.set_type == PowerSetType::Ancillary {
        let build_epic = get_epic(&build);
        if build_epic != power_set.id && build_epic != ""{
            return false
        }
    } else if power_set.set_type == PowerSetType::Pool {
        let build_pool_1 = get_pool_1(&build);
        let build_pool_2 = get_pool_2(&build);
        let build_pool_3 = get_pool_3(&build);
        let build_pool_4 = get_pool_4(&build);
        if build_pool_1 != power_set.id && build_pool_1 != ""
        && build_pool_2 != power_set.id && build_pool_2 != ""
        && build_pool_3 != power_set.id && build_pool_3 != ""
        && build_pool_4 != power_set.id && build_pool_4 != "" {
            return false
        }

        if !has_pool(&build, &power_set.id) && !has_free_pool(&build) {
            return false
        }
    }
    let power = match get_power(&power_entry.power_id){
        Ok(s) => match s {
            Some(p) => p,
            None => {
                return false
            }
        },
        Err(_e) => return false
    };
    if power.level > power_entry.level
        { return false; }

    return true
}

pub fn set_power_entry(build: &mut Build, power_entry: &PowerEntry, power_level: &PowerLevel) -> Result<Build, std::io::Error>{
    let mut power_entry_clone = power_entry.clone();
    let level = convert_power_level(&power_level);
    power_entry_clone.level = level;

    if !can_set_power_entry(&build, &power_entry_clone) {
        return Err(std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            format!("Could not add power set."),
        ))
    }

    let power_set: PowerSet = match get_power_set_from_power(&power_entry_clone.power_id)? {
        Some(ps) => ps,
        None => {
            return Err(std::io::Error::new(
                std::io::ErrorKind::InvalidData,
                format!("Could not find power set for power {}.", power_entry_clone.power_id),
            ))
        }
    };
    if power_set.set_type == PowerSetType::Pool && !has_pool(&build, &power_set.id) {
        let build_pool_1 = get_pool_1(&build);
        let build_pool_2 = get_pool_2(&build);
        let build_pool_3 = get_pool_3(&build);
        let build_pool_4 = get_pool_4(&build);
        if build_pool_1 == "" {
            build.power_sets.insert(BuildPowerSets::pool_1, Some(power_set.id));
        } else if build_pool_2 == "" {
            build.power_sets.insert(BuildPowerSets::pool_2, Some(power_set.id));
        } else if build_pool_3 == "" {
            build.power_sets.insert(BuildPowerSets::pool_3, Some(power_set.id));
        } else if build_pool_4 == "" {
            build.power_sets.insert(BuildPowerSets::pool_4, Some(power_set.id));
        }
    } else if power_set.set_type == PowerSetType::Ancillary && "" == get_epic(&build) {
        build.power_sets.insert(BuildPowerSets::epic, Some(power_set.id));
    }

    build.power_entries.insert(power_level.clone(), Some(power_entry_clone));
    Ok(build.clone())
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::prelude::*;

    use crate::enums::power_level::PowerLevel;
    use crate::functions::registry::get_power;
    use crate::poro::power_entry::PowerEntry;
    use crate::poro::toon::Toon;
    use crate::scatterbase::functions::cross_platform::path;

    #[test]
    #[allow(unused_must_use)]
    fn test_has_power() {
        let mut file = File::open(&path("./data/ai-toon.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let toon: Toon = serde_json::from_str(&contents).unwrap();
        let build = &toon.builds[toon.current_build];

        assert_eq!(super::has_power(&build, "Inherent.Fitness.Stamina"), true);
        assert_eq!(
            super::has_power(&build, "Inherent.Inherent.Alt_Coyote_Travel_Power"),
            false
        );
        assert_eq!(super::has_power(&build, "junk"), false);
    }

    #[test]
    #[allow(unused_must_use)]
    fn test_has_power_set() {
        let mut file = File::open(&path("./data/ai-toon.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let toon: Toon = serde_json::from_str(&contents).unwrap();
        let build = &toon.builds[toon.current_build];

        assert_eq!(super::has_power_set(&build, "Inherent.Fitness"), true);
        assert_eq!(
            super::has_power(&build, "Arachnos_Soldiers.Bane_Spider_Soldier"),
            false
        );
        assert_eq!(super::has_power(&build, "junk"), false);
    }

    #[test]
    #[allow(unused_must_use)]
    fn test_meets_power_requirements() {
        let mut file = File::open(&path("./data/ai-toon.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let toon: Toon = serde_json::from_str(&contents).unwrap();
        let mut build = toon.builds[toon.current_build].clone();

        assert_eq!(
            super::meets_power_requirements(
                &build,
                &get_power("Pool.Speed.Hasten").unwrap().unwrap()
            ),
            true
        );
        assert_eq!(
            super::meets_power_requirements(
                &build,
                &get_power("Pool.Speed.Whirlwind").unwrap().unwrap()
            ),
            false
        );
        if !super::has_power(&build, "Pool.Speed.Hasten") {
            let mut power_entry = PowerEntry::new();
            power_entry.power_id = String::from("Pool.Speed.Hasten");
            build
                .power_entries
                .insert(PowerLevel::level_4, Some(power_entry.clone()));
        }
        if !super::has_power(&build, "Pool.Speed.Super_Speed") {
            let mut power_entry = PowerEntry::new();
            power_entry.power_id = String::from("Pool.Speed.Super_Speed");
            build
                .power_entries
                .insert(PowerLevel::level_14, Some(power_entry.clone()));
        }
        assert_eq!(
            super::meets_power_requirements(
                &build,
                &get_power("Pool.Speed.Whirlwind").unwrap().unwrap()
            ),
            true
        );

        assert_eq!(
            super::meets_power_requirements(
                &build,
                &get_power("Pool.Leadership.Defense").unwrap().unwrap()
            ),
            true
        );
        assert_eq!(
            super::meets_power_requirements(
                &build,
                &get_power("Pool.Leadership.Tactics").unwrap().unwrap()
            ),
            false
        );
        if !super::has_power(&build, "Pool.Leadership.Defense") {
            let mut power_entry = PowerEntry::new();
            power_entry.power_id = String::from("Pool.Leadership.Defense");
            build
                .power_entries
                .insert(PowerLevel::level_6, Some(power_entry.clone()));
        }
        if !super::has_power(&build, "Pool.Leadership.Assault") {
            let mut power_entry = PowerEntry::new();
            power_entry.power_id = String::from("Pool.Leadership.Assault");
            build
                .power_entries
                .insert(PowerLevel::level_16, Some(power_entry.clone()));
        }
        assert_eq!(
            super::meets_power_requirements(
                &build,
                &get_power("Pool.Leadership.Tactics").unwrap().unwrap()
            ),
            true
        );
    }

    #[test]
    #[allow(unused_must_use)]
    fn test_get_primary() {
        let mut file = File::open(&path("./data/ai-toon.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let toon: Toon = serde_json::from_str(&contents).unwrap();
        let build = &toon.builds[toon.current_build];

        let result = super::get_primary(&build);
        assert_eq!(result, "Controller_Control.Mind_Control")
    }

    #[test]
    #[allow(unused_must_use)]
    fn test_get_secondary() {
        let mut file = File::open(&path("./data/ai-toon.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let toon: Toon = serde_json::from_str(&contents).unwrap();
        let build = &toon.builds[toon.current_build];

        let result = super::get_secondary(&build);
        assert_eq!(result, "Controller_Buff.Force_Field")
    }
}
