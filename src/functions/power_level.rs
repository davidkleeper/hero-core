use crate::enums::power_level::PowerLevel;

pub fn convert_power_level(power_level: &PowerLevel) -> u16 {
    match power_level {
        PowerLevel::level_1_primary => return 1,
        PowerLevel::level_1_secondary => return 1,
        PowerLevel::level_2 => return 2,
        PowerLevel::level_4 => return 4,
        PowerLevel::level_6 => return 6,
        PowerLevel::level_8 => return 8,
        PowerLevel::level_10 => return 10,
        PowerLevel::level_12 => return 12,
        PowerLevel::level_14 => return 14,
        PowerLevel::level_16 => return 16,
        PowerLevel::level_18 => return 18,
        PowerLevel::level_20 => return 20,
        PowerLevel::level_22 => return 22,
        PowerLevel::level_24 => return 24,
        PowerLevel::level_26 => return 26,
        PowerLevel::level_28 => return 28,
        PowerLevel::level_30 => return 30,
        PowerLevel::level_32 => return 32,
        PowerLevel::level_35 => return 35,
        PowerLevel::level_38 => return 38,
        PowerLevel::level_41 => return 41,
        PowerLevel::level_44 => return 44,
        PowerLevel::level_47 => return 47,
        PowerLevel::level_49 => return 49,
        PowerLevel::level_1_kheldian => return 1,
        PowerLevel::level_10_kheldian => return 10,
        PowerLevel::health => return 1,
        PowerLevel::stamina => return 1,
        PowerLevel::swift => return 2,
        PowerLevel::hurdle => return 2,
        PowerLevel::sprint => return 2,
        PowerLevel::prestige_power_slide => return 1,
        PowerLevel::brawl => return 1,
        PowerLevel::nova_1 => return 6,
        PowerLevel::nova_2 => return 6,
        PowerLevel::nova_3 => return 6,
        PowerLevel::nova_4 => return 6,
        PowerLevel::dwarf_1 => return 20,
        PowerLevel::dwarf_2 => return 20,
        PowerLevel::dwarf_3 => return 20,
        PowerLevel::dwarf_4 => return 20,
        PowerLevel::dwarf_5 => return 20,
        PowerLevel::dwarf_6 => return 20,
    }
}

pub fn get_power_levels(include_kheldian_powers: bool) -> Vec::<PowerLevel> {
    let mut power_levels = Vec::<PowerLevel>::new();
    power_levels.push(PowerLevel::level_1_primary);
    power_levels.push(PowerLevel::level_1_secondary);
    power_levels.push(PowerLevel::level_2);
    power_levels.push(PowerLevel::level_4);
    power_levels.push(PowerLevel::level_6);
    power_levels.push(PowerLevel::level_8);
    power_levels.push(PowerLevel::level_10);
    power_levels.push(PowerLevel::level_12);
    power_levels.push(PowerLevel::level_14);
    power_levels.push(PowerLevel::level_16);
    power_levels.push(PowerLevel::level_18);
    power_levels.push(PowerLevel::level_20);
    power_levels.push(PowerLevel::level_22);
    power_levels.push(PowerLevel::level_24);
    power_levels.push(PowerLevel::level_26);
    power_levels.push(PowerLevel::level_28);
    power_levels.push(PowerLevel::level_30);
    power_levels.push(PowerLevel::level_32);
    power_levels.push(PowerLevel::level_35);
    power_levels.push(PowerLevel::level_38);
    power_levels.push(PowerLevel::level_41);
    power_levels.push(PowerLevel::level_44);
    power_levels.push(PowerLevel::level_47);
    power_levels.push(PowerLevel::level_49);
    power_levels.push(PowerLevel::health);
    power_levels.push(PowerLevel::stamina);
    power_levels.push(PowerLevel::swift);
    power_levels.push(PowerLevel::hurdle);
    power_levels.push(PowerLevel::sprint);
    power_levels.push(PowerLevel::prestige_power_slide);
    power_levels.push(PowerLevel::brawl);
    if include_kheldian_powers {
        power_levels.push(PowerLevel::level_1_kheldian);
        power_levels.push(PowerLevel::level_10_kheldian);
        power_levels.push(PowerLevel::nova_1);
        power_levels.push(PowerLevel::nova_2);
        power_levels.push(PowerLevel::nova_3);
        power_levels.push(PowerLevel::nova_4);
        power_levels.push(PowerLevel::dwarf_1);
        power_levels.push(PowerLevel::dwarf_2);
        power_levels.push(PowerLevel::dwarf_3);
        power_levels.push(PowerLevel::dwarf_4);
        power_levels.push(PowerLevel::dwarf_5);
        power_levels.push(PowerLevel::dwarf_6);    
    }
    power_levels
}
