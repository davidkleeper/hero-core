use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::str;

use crate::enums::build_power_sets::BuildPowerSets;
use crate::enums::power_level::PowerLevel;
use crate::enums::power_set_type::PowerSetType;
use crate::functions::build::get_epic;
use crate::functions::build::get_pool_1;
use crate::functions::build::get_pool_2;
use crate::functions::build::get_pool_3;
use crate::functions::build::get_pool_4;
use crate::functions::build::has_all_powers;
use crate::functions::build::has_power;
use crate::functions::build::meets_power_requirements;
use crate::functions::power_level::convert_power_level;
use crate::functions::power_level::get_power_levels;
use crate::functions::registry::get_ancillary_power_sets_from_archetype;
use crate::functions::registry::get_archetype;
use crate::functions::registry::get_archetypes;
use crate::functions::registry::get_enhancement;
use crate::functions::registry::get_enhancement_set;
use crate::functions::registry::get_enhancements_from_enhancement_set;
use crate::functions::registry::get_pool_power_sets_from_archetype;
use crate::functions::registry::get_power;
use crate::functions::registry::get_power_set;
use crate::functions::registry::get_power_set_from_power;
use crate::functions::registry::get_powers_from_power_set;
use crate::functions::registry::get_primary_power_sets_from_archetype;
use crate::functions::registry::get_registry;
use crate::functions::registry::get_secondary_power_sets_from_archetype;
use crate::functions::toon::set_power_entry;
use crate::poro::build::Build;
use crate::poro::new_power::NewPower;
use crate::poro::power_entry::PowerEntry;
use crate::poro::power_set::PowerSet;
use crate::poro::toon::Toon;
use crate::scatterbase::functions::registry::get_data;
use crate::scatterbase::functions::registry::get_index;

pub fn get_command_and_arg(request: &str) -> Option<(&str, &str)> {
    let mut words_iter = request.split("!@#");
    let command = match words_iter.next() {
        Some(c) => c,
        None => return None,
    };
    let arg = match words_iter.next() {
        Some(i) => i,
        None => return None,
    };
    Some((command, arg))
}

pub fn process(
    command: &str,
    arg: &str,
    _headers: Vec<String>,
    body: String,
) -> Result<String, std::io::Error> {
    println!("command: {}", command);
    if command == "ancillary_power_sets_for_archetype" {
        return ancillary_power_sets_for_archetype(&arg);
    } else if command == "archetype" {
        return archetype(&arg);
    } else if command == "archetypes" {
        return archetypes();
    } else if command == "archetypes_index" {
        return archetypes_index();
    } else if command == "enhancement" {
        return enhancement(&arg);
    } else if command == "enhancements" {
        return enhancements();
    } else if command == "enhancements_index" {
        return enhancements_index();
    } else if command == "enhancement_set" {
        return enhancement_set(&arg);
    } else if command == "enhancement_sets" {
        return enhancement_sets();
    } else if command == "enhancement_sets_index" {
        return enhancement_sets_index();
    } else if command == "enhancements_for_set" {
        return enhancements_for_set(&arg);
    } else if command == "get_power_entries" {
        return get_power_entries(&body);
    } else if command == "power" {
        return power(&arg);
    } else if command == "powers" {
        return powers();
    } else if command == "powers_index" {
        return powers_index();
    } else if command == "ping" {
        return ping();
    } else if command == "power_set" {
        return power_set(&arg);
    } else if command == "power_set_for_power" {
        return power_set_for_power(&arg);
    } else if command == "power_sets" {
        return power_sets();
    } else if command == "power_sets_index" {
        return power_sets_index();
    } else if command == "powers_for_power_set" {
        return powers_for_power_set(&arg);
    } else if command == "pool_power_sets_for_archetype" {
        return pool_power_sets_for_archetype(&arg);
    } else if command == "primary_power_sets_for_archetype" {
        return primary_power_sets_for_archetype(&arg);
    } else if command == "secondary_power_sets_for_archetype" {
        return secondary_power_sets_for_archetype(&arg);
    } else if command == "toon_get_allowed_power_sets" {
        return toon_get_allowed_power_sets(&body);
    } else if command == "toon_get_allowed_powers" {
        return toon_get_allowed_powers(&body);
    } else if command == "toon_new" {
        return toon_new(&arg);
    } else if command == "toon_set_primary" {
        return toon_set_primary(&arg);
    } else if command == "toon_set_secondary" {
        return toon_set_secondary(&arg);
    } else if command == "toon_set_power_entry" {
        return toon_set_power_entry(&body);
    }
    Err(std::io::Error::new(
        std::io::ErrorKind::NotFound,
        "Requested command not supported.",
    ))
}

fn ancillary_power_sets_for_archetype(arg: &str) -> Result<String, std::io::Error> {
    let power_sets = match get_ancillary_power_sets_from_archetype(&arg)? {
        Some(a) => a,
        None => return Ok(String::from("")),
    };
    let power_json = serde_json::to_string(&power_sets)?;
    Ok(power_json)
}

fn archetype(arg: &str) -> Result<String, std::io::Error> {
    let archetype = match get_archetype(&arg)? {
        Some(a) => a,
        None => return Ok(String::from("")),
    };
    let archetype_json = serde_json::to_string(&archetype)?;
    Ok(archetype_json)
}

fn archetypes() -> Result<String, std::io::Error> {
    let archetypes = get_archetypes(true)?;
    let archetypes_json = serde_json::to_string(&archetypes)?;
    Ok(archetypes_json)
}

fn archetypes_index() -> Result<String, std::io::Error> {
    let archetypes_index = get_index(get_registry(), "archetypes")?;
    let archetypes_index_json = serde_json::to_string(&archetypes_index)?;
    Ok(archetypes_index_json)
}

fn enhancement(arg: &str) -> Result<String, std::io::Error> {
    let enhancement = match get_enhancement(&arg)? {
        Some(a) => a,
        None => return Ok(String::from("")),
    };
    let enhancement_json = serde_json::to_string(&enhancement)?;
    Ok(enhancement_json)
}

fn enhancements() -> Result<String, std::io::Error> {
    let enhancements = get_data(get_registry(), "enhancements")?;
    let enhancements_json = serde_json::to_string(&enhancements)?;
    Ok(enhancements_json)
}

fn enhancements_index() -> Result<String, std::io::Error> {
    let enhancements_index = get_index(get_registry(), "enhancements")?;
    let enhancements_index_json = serde_json::to_string(&enhancements_index)?;
    Ok(enhancements_index_json)
}

fn enhancement_sets() -> Result<String, std::io::Error> {
    let enhancement_sets = get_data(get_registry(), "enhancement-sets")?;
    let enhancement_sets_json = serde_json::to_string(&enhancement_sets)?;
    Ok(enhancement_sets_json)
}

fn enhancement_sets_index() -> Result<String, std::io::Error> {
    let enhancement_sets_index = get_index(get_registry(), "enhancement-sets")?;
    let enhancement_sets_index_json = serde_json::to_string(&enhancement_sets_index)?;
    Ok(enhancement_sets_index_json)
}

fn enhancements_for_set(arg: &str) -> Result<String, std::io::Error> {
    let power = match get_enhancements_from_enhancement_set(&arg)? {
        Some(a) => a,
        None => return Ok(String::from("")),
    };
    let power_json = serde_json::to_string(&power)?;
    Ok(power_json)
}

fn enhancement_set(arg: &str) -> Result<String, std::io::Error> {
    let enhancement_set = match get_enhancement_set(&arg)? {
        Some(a) => a,
        None => return Ok(String::from("")),
    };
    let enhancement_set_json = serde_json::to_string(&enhancement_set)?;
    Ok(enhancement_set_json)
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
struct PowerEntryWithMetadata {
    pub power_entry: Option<PowerEntry>,
    pub power_level: PowerLevel,
    pub level: u16,
    pub power_set_type: PowerSetType,
}

#[allow(dead_code)]
impl PowerEntryWithMetadata {
    pub fn new() -> PowerEntryWithMetadata {
        let power_entry_with_metadata = PowerEntryWithMetadata {
            power_entry: None,
            power_level: PowerLevel::brawl,
            level: 0,
            power_set_type: PowerSetType::None,
        };
        power_entry_with_metadata
    }
}

fn build_power_set_entry_with_metadata(
    build: &Build,
    power_level: &PowerLevel,
    level: u16,
) -> Result<Option<PowerEntryWithMetadata>, std::io::Error> {
    let mut power_entry_with_metadata = PowerEntryWithMetadata::new();
    power_entry_with_metadata.power_level = power_level.clone();
    power_entry_with_metadata.level = level;
    let power_entry_option = match build.power_entries.get(&power_level) {
        Some(o) => o,
        None => return Ok(None),
    };
    power_entry_with_metadata.power_entry = power_entry_option.clone();
    let power_entry = match power_entry_option {
        Some(o) => o,
        None => return Ok(Some(power_entry_with_metadata)),
    };
    let power_string = match power(&power_entry.power_id) {
        Ok(p) => p,
        Err(_e) => {
            let error_text = format!("Power not found: {}.", power_entry.power_id);
            return Err(std::io::Error::new(
                std::io::ErrorKind::InvalidData,
                error_text,
            ));
        }
    };
    let power: NewPower = serde_json::from_str(&power_string)?;
    let power_set_string = match power_set(&power.power_set_id) {
        Ok(ps) => ps,
        Err(_e) => {
            let error_text = format!("Power Set not found: {}.", power.power_set_id);
            return Err(std::io::Error::new(
                std::io::ErrorKind::InvalidData,
                error_text,
            ));
        }
    };
    let power_set: PowerSet = serde_json::from_str(&power_set_string)?;
    power_entry_with_metadata.power_set_type = power_set.set_type;
    Ok(Some(power_entry_with_metadata))
}

fn get_power_entries(data: &str) -> Result<String, std::io::Error> {
    let toon: Toon = serde_json::from_str(&data)?;
    let build = &toon.builds[toon.current_build];
    let mut power_entries = HashMap::<String, PowerEntryWithMetadata>::new();
    let is_kheldian = "Class_Warshade" == toon.archetype_id || "Class_Peacebringer" == toon.archetype_id;
    let power_levels = get_power_levels(is_kheldian);

    for power_level in power_levels {
        let level = convert_power_level(&power_level);
        let power_set_entry_with_metadata_result = build_power_set_entry_with_metadata(&build, &power_level, level);
        match power_set_entry_with_metadata_result {
            Ok(opsewm) => match opsewm {
                Some(psewm) => {
                    power_entries.insert(power_level.to_string(), psewm);
                }
                None => continue,
            },
            Err(e) => return Err(e),
        }
    }

    let power_entries_json = serde_json::to_string(&power_entries)?;
    Ok(power_entries_json)
}

fn ping() -> Result<String, std::io::Error> {
    Ok(String::from("Hero Core v0.1.12"))
}

fn pool_power_sets_for_archetype(arg: &str) -> Result<String, std::io::Error> {
    let power_sets = match get_pool_power_sets_from_archetype(&arg)? {
        Some(a) => a,
        None => return Ok(String::from("")),
    };
    let power_json = serde_json::to_string(&power_sets)?;
    Ok(power_json)
}

fn power(arg: &str) -> Result<String, std::io::Error> {
    let power = match get_power(&arg)? {
        Some(a) => a,
        None => return Ok(String::from("")),
    };
    let power_json = serde_json::to_string(&power)?;
    Ok(power_json)
}

fn powers_for_power_set(arg: &str) -> Result<String, std::io::Error> {
    let power = match get_powers_from_power_set(&arg)? {
        Some(a) => a,
        None => return Ok(String::from("")),
    };
    let power_json = serde_json::to_string(&power)?;
    Ok(power_json)
}

fn power_set(arg: &str) -> Result<String, std::io::Error> {
    let power_set = match get_power_set(&arg)? {
        Some(a) => a,
        None => return Ok(String::from("")),
    };
    let power_set_json = serde_json::to_string(&power_set)?;
    Ok(power_set_json)
}

fn power_set_for_power(arg: &str) -> Result<String, std::io::Error> {
    let power_set = match get_power_set_from_power(&arg)? {
        Some(a) => a,
        None => return Ok(String::from("")),
    };
    let power_set_json = serde_json::to_string(&power_set)?;
    Ok(power_set_json)
}

fn power_sets() -> Result<String, std::io::Error> {
    let power_sets = get_data(get_registry(), "power-sets")?;
    let power_sets_json = serde_json::to_string(&power_sets)?;
    Ok(power_sets_json)
}

fn power_sets_index() -> Result<String, std::io::Error> {
    let power_sets_index = get_index(get_registry(), "power-sets")?;
    let power_sets_index_json = serde_json::to_string(&power_sets_index)?;
    Ok(power_sets_index_json)
}

fn powers() -> Result<String, std::io::Error> {
    let powers = get_data(get_registry(), "powers")?;
    let powers_json = serde_json::to_string(&powers)?;
    Ok(powers_json)
}

fn powers_index() -> Result<String, std::io::Error> {
    let powers_index = get_index(get_registry(), "powers")?;
    let powers_index_json = serde_json::to_string(&powers_index)?;
    Ok(powers_index_json)
}

fn primary_power_sets_for_archetype(arg: &str) -> Result<String, std::io::Error> {
    let power_sets = match get_primary_power_sets_from_archetype(&arg)? {
        Some(a) => a,
        None => return Ok(String::from("")),
    };
    let power_json = serde_json::to_string(&power_sets)?;
    Ok(power_json)
}

fn secondary_power_sets_for_archetype(arg: &str) -> Result<String, std::io::Error> {
    let power_sets = match get_secondary_power_sets_from_archetype(&arg)? {
        Some(a) => a,
        None => return Ok(String::from("")),
    };
    let power_json = serde_json::to_string(&power_sets)?;
    Ok(power_json)
}

fn toon_get_allowed_power_sets(body: &str) -> Result<String, std::io::Error> {
    let data: Vec<String> = serde_json::from_str(&body)?;
    if 2 != data.len() {
        return Err(std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            "Toon and power level must be set.",
        ));
    }
    let toon: Toon = serde_json::from_str(&data[0])?;
    let power_level_enum: PowerLevel = serde_json::from_str(&data[1])?;
    let power_level = convert_power_level(&power_level_enum);
    let build = &toon.builds[toon.current_build];
    if None == build.power_sets.get(&BuildPowerSets::primary)
    || None == build.power_sets.get(&BuildPowerSets::secondary)
    {
        return Err(std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            "Primary and secondary must be set.",
        ));
    }
    if None == build.power_sets.get(&BuildPowerSets::primary).unwrap().as_ref()
    || None == build.power_sets.get(&BuildPowerSets::secondary).unwrap().as_ref()
    {
        return Err(std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            "Primary and secondary must be set.",
        ));
    }

    let mut power_sets = Vec::<PowerSet>::new();
    let primary_id = build.power_sets.get(&BuildPowerSets::primary).unwrap().as_ref().unwrap();
    let primary: PowerSet = get_power_set(&primary_id)?.unwrap();
    let secondary_id = build.power_sets.get(&BuildPowerSets::secondary).unwrap().as_ref().unwrap();
    let secondary: PowerSet = get_power_set(&secondary_id)?.unwrap();
    if power_level_enum != PowerLevel::level_1_secondary 
    && !has_all_powers(&build, &primary.power_ids) {
        power_sets.push(primary);
    }
    if power_level_enum != PowerLevel::level_1_primary 
    && !has_all_powers(&build, &secondary.power_ids) {
        power_sets.push(secondary);
    }

    if power_level_enum != PowerLevel::level_1_primary
    && power_level_enum != PowerLevel::level_1_secondary
    && power_level_enum != PowerLevel::level_2 {
        let pool_1_id = get_pool_1(&build);
        let pool_2_id = get_pool_2(&build);
        let pool_3_id = get_pool_3(&build);
        let pool_4_id = get_pool_4(&build);
        if "" == pool_1_id
        || "" == pool_2_id
        || "" == pool_3_id
        || "" == pool_4_id
        {
            let pools: Vec<PowerSet> = get_pool_power_sets_from_archetype(&toon.archetype_id)?.unwrap();
            for power_set in pools {
                if !has_all_powers(&build, &power_set.power_ids) {
                    power_sets.push(power_set);
                }
            }
        } else {
            let pool_1 = get_power_set(&pool_1_id)?.unwrap();
            let pool_2 = get_power_set(&pool_2_id)?.unwrap();
            let pool_3 = get_power_set(&pool_3_id)?.unwrap();
            let pool_4 = get_power_set(&pool_4_id)?.unwrap();
            if !has_all_powers(&build, &pool_1.power_ids) {
                power_sets.push(pool_1);
            }
            if !has_all_powers(&build, &pool_2.power_ids) {
                power_sets.push(pool_2);
            }
            if !has_all_powers(&build, &pool_3.power_ids) {
                power_sets.push(pool_3);
            }
            if !has_all_powers(&build, &pool_4.power_ids) {
                power_sets.push(pool_4);
            }
        }
    }

    if power_level >= 35 {
        if "" == get_epic(build) {
            let epic: Vec<PowerSet> = get_ancillary_power_sets_from_archetype(&toon.archetype_id)?.unwrap();
            for power_set in epic {
                if !has_all_powers(&build, &power_set.power_ids) {
                    power_sets.push(power_set);
                }
            }
        } else {
            let epic_id = get_epic(build);
            let epic: PowerSet = get_power_set(&epic_id)?.unwrap();
            if !has_all_powers(&build, &epic.power_ids) {
                power_sets.push(epic);
            }
        }
    }

    let mut power_sets_meet_requirements = Vec::<PowerSet>::new();
    for power_set in power_sets {
        let powers: Vec<NewPower> = get_powers_from_power_set(&power_set.id)?.unwrap();
        let mut powers_meet_requirements = Vec::<NewPower>::new();

        if power_level_enum == PowerLevel::level_1_primary
        && power_set.set_type != PowerSetType::Primary {
            continue;
        }
        if power_level_enum == PowerLevel::level_1_secondary
        && power_set.set_type != PowerSetType::Secondary {
            continue;
        }
        for power in powers {
            if power.level > power_level {
               continue;
            }
            if !meets_power_requirements(&build, &power) {
                continue;
            }
            powers_meet_requirements.push(power);
        }
        if 0 == powers_meet_requirements.len() {
            continue;
        }
        power_sets_meet_requirements.push(power_set);
    }

    let result: String = serde_json::to_string(&power_sets_meet_requirements)?;
    Ok(result)
}

fn toon_get_allowed_powers(body: &str) -> Result<String, std::io::Error> {
    let data: Vec<String> = serde_json::from_str(&body)?;
    if 3 != data.len() {
        return Err(std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            "Toon, power level, and power set id must be set.",
        ));
    }
    let toon: Toon = serde_json::from_str(&data[0])?;
    let power_level = match data[1].parse::<u16>() {
        Ok(pl) => pl,
        Err(_e) => {
            return Err(std::io::Error::new(
                std::io::ErrorKind::InvalidData,
                "Error parsing power level.",
            ))
        }
    };
    let power_set_id = &data[2];
    let build = &toon.builds[toon.current_build];
    let powers_json = powers_for_power_set(&power_set_id)?;
    let powers: Vec<NewPower> = serde_json::from_str(&powers_json)?;
    let mut powers_meet_requirements = Vec::<NewPower>::new();

    for power in powers {
        if power.level > power_level {
            continue;
        }
        if has_power(&build, &power.id) {
            continue;
        }
        if !meets_power_requirements(&build, &power) {
            continue;
        }
        powers_meet_requirements.push(power);
    }

    let result: String = serde_json::to_string(&powers_meet_requirements)?;
    Ok(result)
}

fn toon_new(arg: &str) -> Result<String, std::io::Error> {
    let new_toon = Toon::from_archetype(arg);
    let new_toon_json = serde_json::to_string(&new_toon)?;
    Ok(new_toon_json)
}

fn toon_set_power_entry(data: &str) -> Result<String, std::io::Error> {
    let data_array: Vec<String> = serde_json::from_str(data)?;
    let mut toon: Toon = serde_json::from_str(&data_array[0])?;
    let power_entry: PowerEntry = serde_json::from_str(&data_array[1])?;
    let power_level: PowerLevel = match serde_json::from_str(&data_array[2]) {
        Ok(pl) => pl,
        Err(_e) => {
            return Err(std::io::Error::new(
                std::io::ErrorKind::InvalidData,
                format!("Invalid power level: {}.", data_array[2]),
            ))
        }
    };

    let toon = set_power_entry(&mut toon, &power_entry, &power_level)?;
    let toon_json = serde_json::to_string(&toon)?;
    return Ok(toon_json);
}

fn toon_set_power_set(data: &str, power_set: &BuildPowerSets) -> Result<String, std::io::Error> {
    let data_array: Vec<String> = serde_json::from_str(data)?;
    let mut toon: Toon = serde_json::from_str(&data_array[0])?;
    let power_set_id = &data_array[1];

    toon.builds[toon.current_build]
        .power_sets
        .insert(power_set.clone(), Some(String::from(power_set_id)));
    let toon_json = serde_json::to_string(&toon)?;
    Ok(toon_json)
}

fn toon_set_primary(data: &str) -> Result<String, std::io::Error> {
    toon_set_power_set(data, &BuildPowerSets::primary)
}

fn toon_set_secondary(data: &str) -> Result<String, std::io::Error> {
    toon_set_power_set(data, &BuildPowerSets::secondary)
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use std::collections::HashMap;
    use std::fs::File;
    use std::io::prelude::*;

    use crate::enums::build_power_sets::BuildPowerSets;
    use crate::enums::power_level::PowerLevel;
    use crate::functions::build::get_pool_1;
    use crate::functions::build::get_pool_2;
    use crate::functions::build::get_pool_3;
    use crate::functions::build::get_pool_4;
    use crate::poro::build::Build;
    use crate::poro::new_power::NewPower;
    use crate::poro::power_entry::PowerEntry;
    use crate::poro::power_set::PowerSet;
    use crate::poro::toon::Toon;
    use crate::scatterbase::functions::cross_platform::path;

    fn build_blaster() -> (Toon, String) {
        let mut toon_json = super::toon_new("Class_Blaster").unwrap();
        let mut toon: Toon = serde_json::from_str(&toon_json).unwrap();
        let build: &mut Build = &mut toon.builds[toon.current_build];
        build.power_sets.insert(
            BuildPowerSets::primary,
            Some(String::from("Blaster_Ranged.Radiation_Blast")),
        );
        build.power_sets.insert(
            BuildPowerSets::secondary,
            Some(String::from("Blaster_Support.Tactical_Arrow")),
        );
        toon_json = serde_json::to_string(&toon).unwrap();
        (toon.clone(), toon_json.clone())
    }

    #[test]
    #[allow(unused_must_use)]
    fn test_set_primary() {
        let mut file = File::open(&path("./data/ai-toon.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);

        let mut data = Vec::<String>::new();
        data.push(contents);
        data.push(String::from("Controller_Control.Illusion_Control"));
        let data_json = serde_json::to_string(&data).unwrap();
        let result = super::toon_set_primary(&data_json).unwrap();
        let toon: Toon = serde_json::from_str(&result).unwrap();
        assert_eq!(
            toon.builds[toon.current_build]
                .power_sets
                .get(&BuildPowerSets::primary)
                .unwrap()
                .as_ref()
                .unwrap(),
            "Controller_Control.Illusion_Control"
        )
    }

    #[test]
    #[allow(unused_must_use)]
    fn test_set_secondary() {
        let mut file = File::open(&path("./data/ai-toon.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);

        let mut data = Vec::<String>::new();
        data.push(contents);
        data.push(String::from("Controller_Buff.Electrical_Affinity"));
        let data_json = serde_json::to_string(&data).unwrap();
        let result = super::toon_set_secondary(&data_json).unwrap();
        let toon: Toon = serde_json::from_str(&result).unwrap();
        assert_eq!(
            toon.builds[toon.current_build]
                .power_sets
                .get(&BuildPowerSets::secondary)
                .unwrap()
                .as_ref()
                .unwrap(),
            "Controller_Buff.Electrical_Affinity"
        );
    }

    #[test]
    #[allow(unused_must_use)]
    fn test_set_power_entry() {
        let mut file = File::open(&path("./data/ai-toon.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);

        let mut power_level = serde_json::to_string(&PowerLevel::level_49).unwrap();
        let mut power_entry = PowerEntry::new();
        let mut data_array = Vec::<String>::new();
        power_entry.level = 0;
        power_entry.power_id = String::from("Pool.Invisibility.Phase_Shift");
        let mut power_entry_json = serde_json::to_string(&power_entry).unwrap();
        data_array.push(contents);
        data_array.push(power_entry_json.clone());
        data_array.push(power_level.clone());
        let mut data = serde_json::to_string(&data_array).unwrap();
        let result = super::toon_set_power_entry(&data).unwrap();
        let mut toon: Toon = serde_json::from_str(&result).unwrap();
        assert_eq!(
            toon.builds[toon.current_build]
                .power_entries
                .get(&PowerLevel::level_49)
                .unwrap()
                .as_ref()
                .unwrap()
                .power_id,
            "Pool.Invisibility.Phase_Shift"
        );

        // Test add power pool
        let toon_tuple = build_blaster();
        toon = toon_tuple.0;
        let mut toon_json = toon_tuple.1;
        power_entry = PowerEntry::new();
        data_array = Vec::<String>::new();
        power_entry.level = 0;
        power_entry.power_id = String::from("Pool.Invisibility.Phase_Shift");
        power_entry_json = serde_json::to_string(&power_entry).unwrap();
        data_array.push(toon_json);
        data_array.push(power_entry_json);
        data_array.push(power_level);
        data = serde_json::to_string(&data_array).unwrap();
        toon_json = super::toon_set_power_entry(&data).unwrap();
        toon = serde_json::from_str(&toon_json).unwrap();
        let mut build = &toon.builds[toon.current_build];
        let pool_1 = get_pool_1(&build);
        assert_eq!(pool_1, "Pool.Invisibility");

        data_array = Vec::<String>::new();
        power_level = serde_json::to_string(&PowerLevel::level_47).unwrap();
        power_entry = PowerEntry::new();
        power_entry.level = 0;
        power_entry.power_id = String::from("Pool.Flight.Air_Superiority");
        power_entry_json = serde_json::to_string(&power_entry).unwrap();
        data_array.push(toon_json);
        data_array.push(power_entry_json);
        data_array.push(power_level);
        data = serde_json::to_string(&data_array).unwrap();
        toon_json = super::toon_set_power_entry(&data).unwrap();
        toon = serde_json::from_str(&toon_json).unwrap();
        build = &toon.builds[toon.current_build];
        let pool_2 = get_pool_2(&build);
        assert_eq!(pool_2, "Pool.Flight");

        data_array = Vec::<String>::new();
        power_level = serde_json::to_string(&PowerLevel::level_44).unwrap();
        power_entry = PowerEntry::new();
        power_entry.level = 0;
        power_entry.power_id = String::from("Pool.Leadership.Assault");
        power_entry_json = serde_json::to_string(&power_entry).unwrap();
        data_array.push(toon_json);
        data_array.push(power_entry_json);
        data_array.push(power_level);
        data = serde_json::to_string(&data_array).unwrap();
        toon_json = super::toon_set_power_entry(&data).unwrap();
        toon = serde_json::from_str(&toon_json).unwrap();
        build = &toon.builds[toon.current_build];
        let pool_3 = get_pool_3(&build);
        assert_eq!(pool_3, "Pool.Leadership");

        data_array = Vec::<String>::new();
        power_level = serde_json::to_string(&PowerLevel::level_41).unwrap();
        power_entry = PowerEntry::new();
        power_entry.level = 0;
        power_entry.power_id = String::from("Pool.Sorcery.Arcane_Bolt");
        power_entry_json = serde_json::to_string(&power_entry).unwrap();
        data_array.push(toon_json);
        data_array.push(power_entry_json);
        data_array.push(power_level);
        data = serde_json::to_string(&data_array).unwrap();
        toon_json = super::toon_set_power_entry(&data).unwrap();
        toon = serde_json::from_str(&toon_json).unwrap();
        build = &toon.builds[toon.current_build];
        let pool_4 = get_pool_4(&build);
        assert_eq!(pool_4, "Pool.Sorcery");

        // Test no more than 4 power pools
        data_array = Vec::<String>::new();
        power_level = serde_json::to_string(&PowerLevel::level_38).unwrap();
        power_entry = PowerEntry::new();
        power_entry.level = 0;
        power_entry.power_id = String::from("Pool.Teleportation.Recall_Friend");
        power_entry_json = serde_json::to_string(&power_entry).unwrap();
        data_array.push(toon_json.clone());
        data_array.push(power_entry_json);
        data_array.push(power_level);
        data = serde_json::to_string(&data_array).unwrap();
        match super::toon_set_power_entry(&data) {
            Ok(_o) => assert_eq!(1, 0),
            Err(_e) => assert_eq!(1, 1),
        }

        // Test add level 1 power
        data_array = Vec::<String>::new();
        power_level = serde_json::to_string(&PowerLevel::level_1_primary).unwrap();
        power_entry = PowerEntry::new();
        power_entry.level = 0;
        power_entry.power_id = String::from("Blaster_Ranged.Radiation_Blast.Neutrino_Bolt");
        power_entry_json = serde_json::to_string(&power_entry).unwrap();
        data_array.push(toon_json);
        data_array.push(power_entry_json);
        data_array.push(power_level);
        data = serde_json::to_string(&data_array).unwrap();
        toon_json = super::toon_set_power_entry(&data).unwrap();
        toon = serde_json::from_str(&toon_json).unwrap();
        build = &toon.builds[toon.current_build];
        match build.power_entries.get(&PowerLevel::level_1_primary) {
            Some(ope) => match ope {
                Some(pe) => assert_eq!(pe.power_id, "Blaster_Ranged.Radiation_Blast.Neutrino_Bolt"),
                None => assert_eq!(0, 1),
            },
            None => assert_eq!(0, 1),
        };

        data_array = Vec::<String>::new();
        power_level = serde_json::to_string(&PowerLevel::level_1_secondary).unwrap();
        power_entry = PowerEntry::new();
        power_entry.level = 0;
        power_entry.power_id = String::from("Blaster_Support.Tactical_Arrow.Electrified_Net_Arrow");
        power_entry_json = serde_json::to_string(&power_entry).unwrap();
        data_array.push(toon_json);
        data_array.push(power_entry_json);
        data_array.push(power_level);
        data = serde_json::to_string(&data_array).unwrap();
        toon_json = super::toon_set_power_entry(&data).unwrap();
        toon = serde_json::from_str(&toon_json).unwrap();
        build = &toon.builds[toon.current_build];
        match build.power_entries.get(&PowerLevel::level_1_secondary) {
            Some(ope) => match ope {
                Some(pe) => assert_eq!(
                    pe.power_id,
                    "Blaster_Support.Tactical_Arrow.Electrified_Net_Arrow"
                ),
                None => assert_eq!(0, 1),
            },
            None => assert_eq!(0, 1),
        };

        // Test add level 2 power
        data_array = Vec::<String>::new();
        power_level = serde_json::to_string(&PowerLevel::level_2).unwrap();
        power_entry = PowerEntry::new();
        power_entry.level = 0;
        power_entry.power_id = String::from("Blaster_Ranged.Radiation_Blast.X-Ray_Beam");
        power_entry_json = serde_json::to_string(&power_entry).unwrap();
        data_array.push(toon_json);
        data_array.push(power_entry_json);
        data_array.push(power_level);
        data = serde_json::to_string(&data_array).unwrap();
        toon_json = super::toon_set_power_entry(&data).unwrap();
        toon = serde_json::from_str(&toon_json).unwrap();
        build = &toon.builds[toon.current_build];
        match build.power_entries.get(&PowerLevel::level_2) {
            Some(ope) => match ope {
                Some(pe) => assert_eq!(pe.power_id, "Blaster_Ranged.Radiation_Blast.X-Ray_Beam"),
                None => assert_eq!(0, 1),
            },
            None => assert_eq!(0, 1),
        };

        // Test add level 4 power
        data_array = Vec::<String>::new();
        power_level = serde_json::to_string(&PowerLevel::level_4).unwrap();
        power_entry = PowerEntry::new();
        power_entry.level = 0;
        power_entry.power_id = String::from("Blaster_Ranged.Radiation_Blast.Irradiate");
        power_entry_json = serde_json::to_string(&power_entry).unwrap();
        data_array.push(toon_json);
        data_array.push(power_entry_json);
        data_array.push(power_level);
        data = serde_json::to_string(&data_array).unwrap();
        toon_json = super::toon_set_power_entry(&data).unwrap();
        toon = serde_json::from_str(&toon_json).unwrap();
        build = &toon.builds[toon.current_build];
        match build.power_entries.get(&PowerLevel::level_4) {
            Some(ope) => match ope {
                Some(pe) => assert_eq!(pe.power_id, "Blaster_Ranged.Radiation_Blast.Irradiate"),
                None => assert_eq!(0, 1),
            },
            None => assert_eq!(0, 1),
        };

        // Test add level 35 epic power
    }

    #[test]
    #[allow(unused_must_use)]
    fn test_toon_get_allowed_power_sets() {
        let mut file = File::open(&path("./data/ai-toon.json")).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);
        let mut body = Vec::<String>::new();
        let mut power_level = PowerLevel::level_49;
        let mut power_level_json = serde_json::to_string(&power_level).unwrap();
        body.push(contents.clone());
        body.push(power_level_json);
        let mut body_json = serde_json::to_string(&body).unwrap();
        let mut result = super::toon_get_allowed_power_sets(&body_json).unwrap();
        let mut power_set_list: Vec<PowerSet> = serde_json::from_str(&result).unwrap();
        assert_eq!(power_set_list.len(), 5);

        body = Vec::<String>::new();
        power_level = PowerLevel::level_1_primary;
        power_level_json = serde_json::to_string(&power_level).unwrap();
        body.push(contents.clone());
        body.push(power_level_json);
        body_json = serde_json::to_string(&body).unwrap();
        result = super::toon_get_allowed_power_sets(&body_json).unwrap();
        power_set_list = serde_json::from_str(&result).unwrap();
        assert_eq!(power_set_list.len(), 0);

        let toon_json = super::toon_new("Class_Blaster").unwrap();
        let mut toon: Toon = serde_json::from_str(&toon_json).unwrap();
        let build: &mut Build = &mut toon.builds[toon.current_build];
        build.power_sets.insert(
            BuildPowerSets::primary,
            Some(String::from("Blaster_Ranged.Radiation_Blast")),
        );
        build.power_sets.insert(
            BuildPowerSets::secondary,
            Some(String::from("Blaster_Support.Tactical_Arrow")),
        );
        body = Vec::<String>::new();
        power_level = PowerLevel::level_1_primary;
        power_level_json = serde_json::to_string(&power_level).unwrap();
        body.push(serde_json::to_string(&toon).unwrap());
        body.push(power_level_json);
        body_json = serde_json::to_string(&body).unwrap();
        result = super::toon_get_allowed_power_sets(&body_json).unwrap();
        power_set_list = serde_json::from_str(&result).unwrap();
        assert_eq!(power_set_list.len(), 1);

        body = Vec::<String>::new();
        power_level = PowerLevel::level_4;
        power_level_json = serde_json::to_string(&power_level).unwrap();
        body.push(serde_json::to_string(&toon).unwrap());
        body.push(power_level_json);
        body_json = serde_json::to_string(&body).unwrap();
        result = super::toon_get_allowed_power_sets(&body_json).unwrap();
        power_set_list = serde_json::from_str(&result).unwrap();
        assert_eq!(power_set_list.len(), 14);
    }

    #[test]
    #[allow(unused_must_use)]
    fn test_get_power_entries() {
        let mut file = File::open(&path(
            "./HeroBase/brute/Brawl FA Soul bjln Brute - Knight Ruby Lambda c2e4 - [i25].hero",
        ))
        .unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents);

        let mut result = super::get_power_entries(&contents.as_str()).unwrap();
        let mut power_entries: HashMap<String, super::PowerEntryWithMetadata> = serde_json::from_str(&result).unwrap();

        assert_eq!(power_entries.len(), 31);

        contents = super::toon_new("Class_Blaster").unwrap();
        result = super::get_power_entries(&contents.as_str()).unwrap();
        power_entries = serde_json::from_str(&result).unwrap();

        assert_eq!(power_entries.len(), 31);
    }

    #[test]
    #[allow(unused_must_use)]
    fn test_toon_get_allowed_powers() {
        let toon = super::toon_new("Class_Blaster").unwrap();
        let mut power_level: i16 = 1;
        let mut power_set_id = "Blaster_Ranged.Beam_Rifle";
        let mut body = Vec::<String>::new();

        body.push(toon.clone());
        body.push(format!("{}", power_level));
        body.push(String::from(power_set_id));
        let mut body_json = serde_json::to_string(&body).unwrap();
        let mut result_json = super::toon_get_allowed_powers(&body_json).unwrap();
        let mut result: Vec<NewPower> = serde_json::from_str(&result_json).unwrap();
        assert_eq!(result.len(), 2);

        power_set_id = "Pool.Speed";
        power_level = 1;
        body = Vec::<String>::new();
        body.push(toon.clone());
        body.push(format!("{}", power_level));
        body.push(String::from(power_set_id));
        body_json = serde_json::to_string(&body).unwrap();
        result_json = super::toon_get_allowed_powers(&body_json).unwrap();
        result = serde_json::from_str(&result_json).unwrap();
        assert_eq!(result.len(), 0);

        power_set_id = "Pool.Speed";
        power_level = 14;
        body = Vec::<String>::new();
        body.push(toon.clone());
        body.push(format!("{}", power_level));
        body.push(String::from(power_set_id));
        body_json = serde_json::to_string(&body).unwrap();
        result_json = super::toon_get_allowed_powers(&body_json).unwrap();
        result = serde_json::from_str(&result_json).unwrap();
        assert_eq!(result.len(), 3);
    }
}
