package hero.com.heroandroid.ui.main.components

import hero.com.heroandroid.MainActivity

fun HideActionBar(activity: MainActivity) {
    activity.getSupportActionBar()!!.hide();
}

fun ShowActionBar(activity: MainActivity) {
    activity.getSupportActionBar()!!.show();
}