package hero.com.heroandroid.ui.main.pages

import android.content.Context
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import hero.com.heroandroid.MainActivity
import hero.com.heroandroid.R
import hero.com.heroandroid.ui.main.components.HideActionBar
import hero.com.heroandroid.ui.main.appearance.ApplyFullScreenStyle
import hero.com.heroandroid.ui.main.appearance.ApplySplashTitleStyle

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class SplashFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        var delayHandler: Handler = Handler()
        val SPLASH_DELAY: Long = 3000 //3 seconds
        val runnable: Runnable = Runnable {
            findNavController().navigate(R.id.action_Splash_to_Home)
        }
        delayHandler.postDelayed(runnable, SPLASH_DELAY)
        val context = requireContext()
        val activity: MainActivity = requireActivity() as MainActivity

        HideActionBar(activity)
        val splash = splash(activity, context)
        return splash
    }
}

fun splash(activity: MainActivity, context: Context): RelativeLayout {
    var relativeLayout = RelativeLayout(activity)
    var textView = TextView(context)
    var imageView = ImageView(context)
    imageView.setImageResource(R.drawable.splash);
    ApplyFullScreenStyle(imageView)
    relativeLayout.addView(imageView)
    textView.setText("HERO/Android")
    ApplySplashTitleStyle(textView)
    relativeLayout.addView(textView)
    return relativeLayout
}
