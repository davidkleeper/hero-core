package hero.com.heroandroid.ui.main.appearance

import android.graphics.Color
import android.graphics.Paint
import android.view.Gravity
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.widget.TextViewCompat
import hero.com.heroandroid.MainActivity
import hero.com.heroandroid.R

fun ApplySplashTitleStyle(textView: TextView) {
    var layoutParams: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
        ViewGroup.LayoutParams.MATCH_PARENT,
        ViewGroup.LayoutParams.WRAP_CONTENT
    )
    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
    textView.setLayoutParams(layoutParams)
    TextViewCompat.setTextAppearance(textView, R.style.fontSplashText)
    textView.setTextColor(Color.WHITE);
    textView.setPadding(0,0,0,480)
    textView.setGravity(Gravity.CENTER)
}

fun ApplyButtonTextStyle(textView: TextView) {
    TextViewCompat.setTextAppearance(textView, R.style.fontButtonText)
}

fun ApplyButtonPanelTitleTextStyle(textView: TextView) {
    TextViewCompat.setTextAppearance(textView, R.style.fontButtonPanelTitleText)
}

fun ApplyLinkTextStyle(textView: TextView) {
    TextViewCompat.setTextAppearance(textView, R.style.fontLinkText)
    textView.setPaintFlags(textView.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
}


