package hero.com.heroandroid.ui.main.appearance

import android.view.ViewGroup.LayoutParams
import android.widget.ImageView
import android.widget.RelativeLayout
import hero.com.heroandroid.MainActivity

fun ApplyFullScreenStyle(imageView: ImageView){
    var layoutParams: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
        LayoutParams.WRAP_CONTENT,
        LayoutParams.WRAP_CONTENT
    )
    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE)
    imageView.setLayoutParams(layoutParams)
    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
}
