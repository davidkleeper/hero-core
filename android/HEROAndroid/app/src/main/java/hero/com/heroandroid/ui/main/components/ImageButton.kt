package hero.com.heroandroid.ui.main.components

import android.content.Context
import android.view.Gravity
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import hero.com.heroandroid.MainActivity
import hero.com.heroandroid.ui.main.appearance.ApplyButtonTextStyle

class ButtonData
constructor(
    public var id: Int,
    public var name: String,
    public var archetype: String,
    public var build: String
)
{}

class ButtonMetrics
constructor(
    public var imageWidth: Float,
    public var imageHeight: Float,
    public var buttonWidth: Float,
    public var buttonHeight: Float,
    public var buttonPaddingLeft: Float,
    public var buttonPaddingTop: Float,
    public var buttonPaddingRight: Float,
    public var buttonPaddingBottom: Float,
    public var fontSize: Float
)
{}

public fun imageButton(activity: MainActivity, context: Context, imageId: Int, buttonLabel: String, buttonMetrics: ButtonMetrics): LinearLayout {
    val buttonImage = ImageView(context)
    buttonImage.setImageResource(imageId);
    var buttonParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
    )
    buttonParams.width = buttonMetrics.imageWidth.toInt()
    buttonParams.height = buttonMetrics.imageHeight.toInt()
    buttonParams.gravity = Gravity.CENTER_HORIZONTAL;
    buttonImage.setLayoutParams(buttonParams)

    val buttonText = TextView(context)
    buttonText.setText(buttonLabel)
    buttonText.setGravity(Gravity.CENTER);
    buttonText.setTextSize(buttonMetrics.fontSize);
    ApplyButtonTextStyle(buttonText)
    val textParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
    )
    textParams.gravity = Gravity.CENTER_HORIZONTAL;
    buttonText.setLayoutParams(textParams)

    val linearLayout = LinearLayout(context)
    linearLayout.setOrientation(LinearLayout.VERTICAL)
    val layoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
    )
    layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
    layoutParams.width = buttonMetrics.buttonWidth.toInt()
    layoutParams.height = buttonMetrics.buttonHeight.toInt()
    linearLayout.setLayoutParams(layoutParams)
    linearLayout.setPadding(buttonMetrics.buttonPaddingLeft.toInt(), buttonMetrics.buttonPaddingTop.toInt(), buttonMetrics.buttonPaddingRight.toInt(), buttonMetrics.buttonPaddingBottom.toInt())
    linearLayout.addView(buttonImage)
    linearLayout.addView(buttonText)
    return linearLayout
}
