package hero.com.heroandroid.ui.main.pages

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import hero.com.heroandroid.MainActivity
import hero.com.heroandroid.R
import hero.com.heroandroid.ui.main.components.ShowActionBar

/**
 * A simple [Fragment] subclass.
 * Use the [ToonFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ToonFragment : Fragment() {
    val args: ToonFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val context = requireContext()
        val activity: MainActivity = requireActivity() as MainActivity
        val navController = container?.findNavController()

        ShowActionBar(activity)
        Log.i(args.archetype, args.build)

        return inflater.inflate(R.layout.fragment_toon, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         * @return A new instance of fragment toonFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ToonFragment().apply {
            }
    }
}