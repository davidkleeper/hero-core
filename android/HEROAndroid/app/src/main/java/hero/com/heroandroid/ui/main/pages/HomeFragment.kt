package hero.com.heroandroid.ui.main.pages

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.findNavController
import hero.com.heroandroid.MainActivity
import hero.com.heroandroid.R
import hero.com.heroandroid.ui.main.appearance.ApplyButtonPanelTitleTextStyle
import hero.com.heroandroid.ui.main.appearance.ApplyLinkTextStyle
import hero.com.heroandroid.ui.main.components.ButtonData
import hero.com.heroandroid.ui.main.components.ButtonMetrics
import hero.com.heroandroid.ui.main.components.HideActionBar
import hero.com.heroandroid.ui.main.components.imageButton

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class HomeFragment : Fragment() {

    val REPOSITORIES: Array<ButtonData> get() = arrayOf(
        ButtonData(R.drawable.reborn, "Mid's Reborn", "None", "None"),
        ButtonData(R.drawable.dropbox, "Dropbox", "None", "None"),
        ButtonData(R.drawable.herobase, "HeroBase", "None", "None")
    )

    val ARCHETYPES: Array<ButtonData> get() = arrayOf(
        ButtonData(R.drawable.arachnos_soldier, "Arachnos Soldier", "Class_Arachnos_Soldier", "None"),
        ButtonData(R.drawable.arachnos_widow, "Arachnos Widow", "Class_Arachnos_Widow", "None"),
        ButtonData(R.drawable.blaster, "Blaster", "Class_Blaster", "None"),
        ButtonData(R.drawable.brute, "Brute", "Class_Brute", "None"),
        ButtonData(R.drawable.controller, "Controller", "Class_Controller", "None"),
        ButtonData(R.drawable.corruptor, "Corruptor", "Class_Corruptor", "None"),
        ButtonData(R.drawable.defender, "Defender", "Class_Defender", "None"),
        ButtonData(R.drawable.dominator, "Dominator", "Class_Dominator", "None"),
        ButtonData(R.drawable.mastermind, "Mastermind", "Class_Mastermind", "None"),
        ButtonData(R.drawable.peacebringer, "Peacebringer", "Class_Peacebringer", "None"),
        ButtonData(R.drawable.scrapper, "Scrapper", "Class_Scrapper", "None"),
        ButtonData(R.drawable.sentinel, "Sentinel", "Class_Sentinel", "None"),
        ButtonData(R.drawable.stalker, "Stalker", "Class_Stalker", "None"),
        ButtonData(R.drawable.tanker, "Tanker", "Class_Tanker", "None"),
        ButtonData(R.drawable.warshade, "Warshade", "Class_Warshade", "None")
    )

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val context = requireContext()
        val activity: MainActivity = requireActivity() as MainActivity
        val navController = container?.findNavController()

        HideActionBar(activity)
        val linearLayout = LinearLayout(context)
        linearLayout.setOrientation(LinearLayout.VERTICAL)
        val layoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        )
        linearLayout.setLayoutParams(layoutParams)

        val openButtonPanel = buttonPanel(activity, context, REPOSITORIES, "Open", navController)
        linearLayout.addView(openButtonPanel)
        val createButtonPanel = buttonPanel(activity, context, ARCHETYPES, "Create", navController)
        linearLayout.addView(createButtonPanel)

        val link = buttonPanelLink(context, "Tell me about the Archetypes")
        linearLayout.addView(link)

        return linearLayout
    }
}

fun buttonPanel(activity: MainActivity, context: Context, buttonInfo: Array<ButtonData>, title: String, navController: NavController?): View {
    val linearLayout = LinearLayout(context)
    linearLayout.setOrientation(LinearLayout.VERTICAL)
    val layoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
    )
    layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
    linearLayout.setLayoutParams(layoutParams)

    val title = buttonPanelTitle(context, title)
    linearLayout.addView(title)

    for (index in 0..buttonInfo.size step 3) {
        val panel = buttonPanelRow(activity, context, buttonInfo, index, navController)
        linearLayout.addView(panel)
    }
    return linearLayout
}

private fun buttonPanelTitle(context: Context, title: String): TextView {
    val titleText = TextView(context)
    val displayMetrics = context.getResources().getDisplayMetrics()
    val buttonWidth = displayMetrics.widthPixels / 3.2f
    val textFontSize = buttonWidth / 20f
    ApplyButtonPanelTitleTextStyle(titleText)
    titleText.setText(title)
    titleText.setGravity(Gravity.CENTER);
    val textParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
    )
    textParams.gravity = Gravity.CENTER_HORIZONTAL;
    textParams.width = ViewGroup.LayoutParams.FILL_PARENT;
    textParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
    titleText.setLayoutParams(textParams)
    titleText.setTextSize(textFontSize);
    titleText.setPadding(0, 10, 0, 0)
    return titleText
}

fun buttonPanelRow(activity: MainActivity, context: Context, buttonInfo: Array<ButtonData>, buttonInfoIndex: Int, navController: NavController?): View {
    val displayMetrics = context.getResources().getDisplayMetrics()
    val imageWidth = displayMetrics.widthPixels / 8.5f
    val imageHeight = imageWidth
    val buttonWidth = displayMetrics.widthPixels / 3.2f
    val buttonHeight = buttonWidth * 0.6f
    val textFontSize = buttonWidth / 35f
    val buttonMetrics = ButtonMetrics(
        imageWidth,
        imageHeight,
        buttonWidth,
        buttonHeight,
        5.0f,
        20.0f,
        5.0f,
        20.0f,
        textFontSize
    )

    val linearLayout = LinearLayout(context)
    linearLayout.setOrientation(LinearLayout.HORIZONTAL)
    val layoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
        ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
    )
    layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
    linearLayout.setLayoutParams(layoutParams)
    for (index in buttonInfoIndex..buttonInfoIndex + 2) {
        if (index >= buttonInfo.size)
            break
        val imageButton = imageButton(activity, context, buttonInfo[index].id, buttonInfo[index].name, buttonMetrics)
        imageButtonAddClick(imageButton, linearLayout, buttonInfo[index], navController)
        linearLayout.addView(imageButton)
    }

    return linearLayout
}

private fun buttonPanelLink(context: Context, title: String): TextView {
    val titleText = TextView(context)
    val displayMetrics = context.getResources().getDisplayMetrics()
    val buttonWidth = displayMetrics.widthPixels / 3.2f
    val textFontSize = buttonWidth / 25f
    ApplyLinkTextStyle(titleText)
    titleText.setText(title)
    titleText.setGravity(Gravity.CENTER);
    val textParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
    )
    textParams.gravity = Gravity.CENTER_HORIZONTAL;
    textParams.width = ViewGroup.LayoutParams.FILL_PARENT;
    textParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
    titleText.setLayoutParams(textParams)
    titleText.setTextSize(textFontSize);
    titleText.setPadding(0, 50, 0, 0)
    return titleText
}

public fun imageButtonAddClick(imageButton: LinearLayout, parentView: View, buttonData: ButtonData, navController: NavController?) {
    imageButton.setOnClickListener{
        val navAction = HomeFragmentDirections.actionHomeToToon(buttonData.archetype, buttonData.build)
        navController?.navigate(navAction)
        Log.i(buttonData.archetype, buttonData.build)
    }
}
