import SwiftUI

struct ImageButtonRow: View {
    var buttonWidth: CGFloat
    var buttonHeight: CGFloat
    var imageWidth: CGFloat
    var imageHeight: CGFloat
    var textFontSize: CGFloat
    var imageIds: [String]
    var titles: [String]
    var actionCodes: [ActionCode]

    var body: some View {
        VStack {
            HStack {
                ImageButton(
                    buttonWidth: buttonWidth, buttonHeight: buttonHeight,
                    imageWidth: imageWidth, imageHeight: imageHeight,
                    textFontSize: textFontSize, imageId: imageIds[0],
                    title: titles[0], actionCode: actionCodes[0]
                );
                ImageButton(
                    buttonWidth: buttonWidth, buttonHeight: buttonHeight,
                    imageWidth: imageWidth, imageHeight: imageHeight,
                    textFontSize: textFontSize, imageId: imageIds[1],
                    title: titles[1], actionCode: actionCodes[1]
                );
                ImageButton(
                    buttonWidth: buttonWidth, buttonHeight: buttonHeight,
                    imageWidth: imageWidth, imageHeight: imageHeight,
                    textFontSize: textFontSize, imageId: imageIds[2],
                    title: titles[2], actionCode: actionCodes[2]
                );
            }
            Spacer().frame(height: 30)
        }
    }
}
