import SwiftUI

struct ImageButton: View {
    var buttonWidth: CGFloat
    var buttonHeight: CGFloat
    var imageWidth: CGFloat
    var imageHeight: CGFloat
    var textFontSize: CGFloat
    var imageId: String
    var title: String
    var actionCode: ActionCode

    var body: some View {
        let view = getView(actionCode: actionCode)
        return Button(action: { print("button pressed"); }) {
            NavigationLink(destination: view) {
                VStack {
                    Image(imageId).renderingMode(.original).resizable().frame(width:           imageWidth, height: imageHeight)
                    Text(title).font(.system(size: textFontSize)).fontWeight(.bold)
                }
                .frame(width: buttonWidth, height: buttonHeight)
                .hoverEffect(.lift)
            }
        }
    }
}

func getView(actionCode: ActionCode) -> AnyView{
    return AnyView(ToonView(archetype: actionCode.archetype, build: actionCode.build))
}
