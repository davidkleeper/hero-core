//
//  Toon.swift
//  Shared
//
//  Created by David Leeper on 6/29/20.
//

import SwiftUI

struct ToonView: View {
    var archetype: String
    var build: String
    var body: some View {
        VStack {
            Text(archetype).font(.system(size: 12)).fontWeight(.bold)
            Text(build).font(.system(size: 12)).fontWeight(.bold)
            Text(runCoreCommand()).font(.system(size: 12)).fontWeight(.bold)
        }
    }
}

struct ToonView_Previews: PreviewProvider {
    static var previews: some View {
        ToonView(archetype: "Class_Blaster", build: "None")
    }
}

func getView() -> String {
    let result = core_command("PLAYER_ARCHETYPES")
    let sr = String(cString: result!)
    // IMPORTANT: once we get the result we have to release the pointer.
    core_command_release(UnsafeMutablePointer(mutating: result))
    return sr
}
