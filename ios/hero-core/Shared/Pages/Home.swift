//
//  Home.swift
//  Shared
//
//  Created by David Leeper on 6/29/20.
//

import SwiftUI

struct ActionCode {
    init(view: String, archetype: String, build: String) {
        self.view = view
        self.archetype = archetype
        self.build = build
    }
    var view = ""
    var archetype = ""
    var build = ""
}

struct HomeView: View {
    @State private var fullScreen = false
    let s = runCoreCommand()
    var body: some View {
        NavigationView { // NAV
            GeometryReader { geometry in // GEOMETRY
                let imageWidth: CGFloat = geometry.size.width / 8.5
                let imageHeight: CGFloat = imageWidth
                let buttonWidth: CGFloat = geometry.size.width / 3.2
                let buttonHeight: CGFloat = buttonWidth * 0.45
                let textFontSize: CGFloat = buttonWidth / 11.2
                let titleFontSize: CGFloat = textFontSize * 1.2
                let imageIds = getImageIds()
                let titles = getTitles()
                let actionCodes = getActionCodes()

                VStack{ // PANEL
                    VStack{
                        Text("Open").font(.system(size: titleFontSize)).fontWeight(.bold)
                        ImageButtonRow(
                            buttonWidth: buttonWidth, buttonHeight: buttonHeight,
                            imageWidth: imageWidth, imageHeight: imageHeight,
                            textFontSize: textFontSize, imageIds: imageIds[0],
                            titles: titles[0], actionCodes: actionCodes[0]
                        )
                    }
                    VStack{ // ARCHETYPE
                        Text("Create").font(.system(size: titleFontSize)).fontWeight(.bold)
                        VStack {
                            ImageButtonRow(
                                buttonWidth: buttonWidth, buttonHeight: buttonHeight,
                                imageWidth: imageWidth, imageHeight: imageHeight,
                                textFontSize: textFontSize, imageIds: imageIds[1],
                                titles: titles[1], actionCodes: actionCodes[1]
                            )
                            ImageButtonRow(
                                buttonWidth: buttonWidth, buttonHeight: buttonHeight,
                                imageWidth: imageWidth, imageHeight: imageHeight,
                                textFontSize: textFontSize, imageIds: imageIds[2],
                                titles: titles[2], actionCodes: actionCodes[2]
                            )
                            ImageButtonRow(
                                buttonWidth: buttonWidth, buttonHeight: buttonHeight,
                                imageWidth: imageWidth, imageHeight: imageHeight,
                                textFontSize: textFontSize, imageIds: imageIds[3],
                                titles: titles[3], actionCodes: actionCodes[3]
                            )
                            ImageButtonRow(
                                buttonWidth: buttonWidth, buttonHeight: buttonHeight,
                                imageWidth: imageWidth, imageHeight: imageHeight,
                                textFontSize: textFontSize, imageIds: imageIds[4],
                                titles: titles[4], actionCodes: actionCodes[4]
                            )
                            ImageButtonRow(
                                buttonWidth: buttonWidth, buttonHeight: buttonHeight,
                                imageWidth: imageWidth, imageHeight: imageHeight,
                                textFontSize: textFontSize, imageIds: imageIds[5],
                                titles: titles[5], actionCodes: actionCodes[5]
                            )
                        }
                    } // ARCHETYPE
                    VStack{
                        Button(action: { print("button pressed"); }) {
                            Text("Tell me about the Archetypes").font(.system(size: titleFontSize)).fontWeight(.bold)
                        }
                    }
                }// PANEL
            }// GEOMETRY
            .navigationBarTitle("Home")
            .navigationBarHidden(fullScreen)
        } // NAV
        .statusBar(hidden: fullScreen)
        .onAppear(perform: {
            self.fullScreen = true
        })
   } // BODY
}

func getImageIds() -> [[String]]{
    let openIds:[String] = ["Reborn", "dropbox", "HeroBase"]
    let archetypeIdsRow1:[String] = ["Arachnos Soldier", "Arachnos Widow", "Blaster"]
    let archetypeIdsRow2:[String] = ["Brute", "Controller", "Corruptor"]
    let archetypeIdsRow3:[String] = ["Defender", "Dominator", "Mastermind"]
    let archetypeIdsRow4:[String] = ["Peacebringer", "Scrapper", "Sentinel"]
    let archetypeIdsRow5:[String] = ["Stalker", "Tanker", "Warshade"]
    var ids = [[String]]()
    ids.append(openIds)
    ids.append(archetypeIdsRow1)
    ids.append(archetypeIdsRow2)
    ids.append(archetypeIdsRow3)
    ids.append(archetypeIdsRow4)
    ids.append(archetypeIdsRow5)
    return ids
}

func getTitles() -> [[String]]{
    var titles = getImageIds()
    titles[0] = ["Mid's Reborn", "Dropbox", "HeroBase"]
    return titles
}

func getActionCodes() -> [[ActionCode]]{
    var actionCodes = [[ActionCode]]()
    actionCodes.append([ActionCode]())
    actionCodes[0].append(ActionCode(view: "OpenRebornView", archetype: "None", build: "None"))
    actionCodes[0].append(ActionCode(view: "OpenDropboxView", archetype: "None", build: "None"))
    actionCodes[0].append(ActionCode(view: "OpenHeroBaseView", archetype: "None", build: "None"))
    actionCodes.append([ActionCode]())
    actionCodes[1].append(ActionCode(view: "ToonView", archetype: "Class_Arachnos_Soldier", build: "None"))
    actionCodes[1].append(ActionCode(view: "ToonView", archetype: "Class_Arachnos_Widow", build: "None"))
    actionCodes[1].append(ActionCode(view: "ToonView", archetype: "Class_Blaster", build: "None"))
    actionCodes.append([ActionCode]())
    actionCodes[2].append(ActionCode(view: "ToonView", archetype: "Class_Brute", build: "None"))
    actionCodes[2].append(ActionCode(view: "ToonView", archetype: "Class_Controller", build: "None"))
    actionCodes[2].append(ActionCode(view: "ToonView", archetype: "Class_Corruptor", build: "None"))
    actionCodes.append([ActionCode]())
    actionCodes[3].append(ActionCode(view: "ToonView", archetype: "Class_Defender", build: "None"))
    actionCodes[3].append(ActionCode(view: "ToonView", archetype: "Class_Dominator", build: "None"))
    actionCodes[3].append(ActionCode(view: "ToonView", archetype: "Class_Mastermind", build: "None"))
    actionCodes.append([ActionCode]())
    actionCodes[4].append(ActionCode(view: "ToonView", archetype: "Class_Peacebringer", build: "None"))
    actionCodes[4].append(ActionCode(view: "ToonView", archetype: "Class_Scrapper", build: "None"))
    actionCodes[4].append(ActionCode(view: "ToonView", archetype: "Class_Sentinel", build: "None"))
    actionCodes.append([ActionCode]())
    actionCodes[5].append(ActionCode(view: "ToonView", archetype: "Class_Stalker", build: "None"))
    actionCodes[5].append(ActionCode(view: "ToonView", archetype: "Class_Tanker", build: "None"))
    actionCodes[5].append(ActionCode(view: "ToonView", archetype: "Class_Warshade", build: "None"))
    return actionCodes
}

func runCoreCommand() -> String {
    let result = core_command("PLAYER_ARCHETYPES")
    let sr = String(cString: result!)
    // IMPORTANT: once we get the result we have to release the pointer.
    core_command_release(UnsafeMutablePointer(mutating: result))
    return sr
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

