//
//  hero_coreApp.swift
//  Shared
//
//  Created by David Leeper on 6/29/20.
//

import SwiftUI

@main
struct hero_coreApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}
