#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#define Vector_AOEAttack 4

#define Suppress_ActivateAttackClick 512

#define Entity_Any 32768

#define Entity_AnyLeaguemate 2097152

#define ModeFlags_Arena 1

#define Suppress_Attacked 64

#define CastFlags_CastableAfterDeath 4

#define Entity_Caster 1

#define Vector_ColdAttack 32

#define Suppress_Confused 4096

#define Suppress_Damaged 1024

#define Entity_DeadFoe 131072

#define Entity_DeadFriend 1024

#define Entity_DeadLeaguemate 1048576

#define Entity_DeadMyCreation 4194304

#define Entity_DeadMyPet 8388608

#define Entity_DeadOrAliveFoe 16777216

#define Entity_DeadOrAliveLeaguemate 33554432

#define Entity_DeadOrAliveTeammate 32

#define Entity_DeadPlayer 4

#define Entity_DeadPlayerFriend 67108864

#define Entity_DeadTeammate 16

#define Entity_DeadVillain 128

#define ModeFlags_DisableAll 2

#define ModeFlags_DisableEnhancements 4

#define ModeFlags_DisableEpic 8

#define ModeFlags_DisableInspirations 16

#define ModeFlags_DisableMarketTP 32

#define ModeFlags_DisablePool 64

#define ModeFlags_DisableRezInsp 128

#define ModeFlags_DisableTeleport 256

#define ModeFlags_DisableTemp 512

#define ModeFlags_DisableToggle 1024

#define ModeFlags_DisableTravel 2048

#define ModeFlags_Domination 4096

#define Vector_EnergyAttack 128

#define Vector_FireAttack 64

#define Entity_Foe 2048

#define Entity_FoeRezzingFoe 262144

#define Entity_Friend 512

#define Suppress_Held 1

#define Suppress_HitByFoe 128

#define Suppress_Immobilized 8

#define Suppress_Knocked 32

#define Entity_Leaguemate 524288

#define Vector_LethalAttack 16

#define Entity_Location 8192

#define Vector_MeleeAttack 1

#define Suppress_MissionObjectClick 256

#define Entity_MyOwner 134217728

#define Entity_MyPet 65536

#define Entity_NPC 256

#define CastFlags_NearGround 1

#define Vector_NegativeEnergyAttack 256

#define CastFlags_None 0

#define ModeFlags_PeacebringerBlasterMode 8192

#define ModeFlags_PeacebringerLightformMode 16384

#define ModeFlags_PeacebringerTankerMode 32768

#define Suppress_Phased1 2048

#define Entity_Player 2

#define Vector_PsionicAttack 512

#define ModeFlags_RaidAttackerMode 65536

#define Vector_RangedAttack 2

#define ModeFlags_ShivanMode 131072

#define Suppress_Sleep 2

#define Vector_SmashingAttack 8

#define Suppress_Stunned 4

#define CastFlags_TargetNearGround 2

#define Entity_Teammate 8

#define Entity_Teleport 16384

#define Suppress_Terrorized 16

#define ModeFlags_Unknown18 262144

#define Entity_Villain 64

#define ModeFlags_WarshadeBlasterMode 524288

#define ModeFlags_WarshadeTankerMode 1048576

char *core_command(const char *command);

void core_command_release(char *command);
