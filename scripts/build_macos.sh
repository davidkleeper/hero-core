#!/usr/bin/env bash
# building
cbindgen src/lib.rs -l c > hero_core.h
cargo lipo --release

# moving files to the ios project
inc=./macos/include
libs=./macos/libs

rm -rf ${inc} ${libs}

mkdir ${inc}
mkdir ${libs}

cp ./hero_core.h ${inc}
cp ./target/universal/release/libhero_core.a ${libs}/libhero_core.a
